function [IctrRC,IctrInd,fig] = daVinciZoneSample(imgNum,I,LorR,Opts)
%function [IctrRC,IctrInd] = daVinciZoneSample(imgNum,PszXY,LorR,ctrORedg,minMaxPixDVN,zoneTestType,zoneBufferRC,zoneMinDensity,rndSd,plotFlag,Lpht,Rpht,Lrng,Rrng,Ldvn,Rdvn,indLookup,rootDTBdir)
%
% Sample from DaVinci regions to find potential patch centers that satisfy the following:
%   Are the center or leftmost pixel in the davinci region (ctrORedg)
%   In a DaVinci region with a minimum width (minMaxPixDVN(1)) and optionally w/ max (minMaxPixDVN(2))
%       This width Regions width extends through vertical of specified patch size
%       [with some buffering (zoneBufferRC)
%           OR (zoneTestType)
%       DaVinci region within patch size meets a DaVinci density requirement
%   Potential Patches do not overlap
%
%   example call:
%       IctrRC=daVinciZoneSample(1,[130 52],'L',edg,18,'slice',[0 13],.2,1,1);
%       daVinciZoneSample([52 52],'B',8,18,'bgn','slice',13,0,.2,1,rootDTBdir,1,8,[])
%
%   INPUTS
%   imgNum   - range of images to sue from    [numImages x 1]
%               *Optional if including Lpht,Rpht,Lrng,Rrng,Ldvn,Rdvn
%   PszXY    - patch size [2 x 1]
%   LorR     - which anchor to use
%              'L' -> Left eye ancho
%              'R' -> Right eye anchor
%              'B' -> Both eyes as anchors (individually not togehter)
%   ctrORedg - whether the patch center is at the beginning or end of DaVinci region
%   minMaxPixDVN - [2 x 1]
%              (1) minimum width of DaVinci region for patch center
%              (2) maximum width of DaVinci region for patch center
%                 'edg' -> patch center is at center pixel of DaVicni region
%                 'bgn' -> patch center is at leftmost pixerl of DaVinci region
%   nDVNbuffer   - size of DVN region in corresponding patch to be considered significant (superSlice method only)
%   nDVN         - number of DVN regions allowed in each corresponding patch (superSlice method only)
%   minZoneSep   - required horizontal distance between central and additional significant DVN zones. (superSlice method only)
%   zoneTestType - whether to use 'slice' or 'density' vetting for patch center
%              'slice' - use slice method, where DaVinci region center width must be present
%               within every row of the patch
%              'superSlice' - slice method, but allows for non-significant DVN zones, and allows
%                             a specified number of sigificant DVN zones if they are seperated
%                             horizontally by a specified distance.
%                             See nDVNbuffer, nDVN, minZoneSep.
%              'density' - DaVinci region must have a specified density within patch region
%   zoneBufferRC(2) - buffer for 'slice' method: width can be minMaxPixDVN(1)-zoneBufferRC(2) in length
%              to count as an extending region in patch area
%   zoneBufferRC(1) - buffer for 'slice' method: if there are X number of rows in a patch, there
%              must be X-zoneBufferRC(1) rows with width of minMaxPixDVN(1)-zoneBufferRC(2) to be
%              classified as a valid patch
%   zoneMinDensity - density of DaVinci pixels to non-DaVinci pixels in patch region to be
%              classified as a valid patch
%   rndSd    - random seed to use for random sampling
%              0 -> non-random sampling that maximizes number of samples
%   plotFlag - whether to plot and/or which part of the image to plot
%               0 -> do not plog
%               1 -> plot for only L or R anchor only (whatever is being used)
%               2 -> plot for Left anchor
%               3 -> plot for RIght anchor
%               2 & 3 are meant to be used in a loop
%   Lpht     - left luminance image  [PszXY(2) x PszXY(1) ]
%   Rpht     - right luminance image [PszXY(2) x PszXY(1) ]
%   Lrng     - left range image      [PszXY(2) x PszXY(1) ]
%   Rrng     - right range image     [PszXY(2) x PszXY(1) ]
%   Ldvn     - left DaVinci image    [PszXY(2) x PszXY(1) ]
%   Rdvn     - right DaVinci image   [PszXY(2) x PszXY(1) ]
%   rootDTBdir - root directory of database, exlcuding the 'LRSI' directory
%               *optional for burge lab - TODO add this functionality
%   indLookup - index lookup table defined as:
%                indLookup=reshape(1:(IszRC(1)*IszRC(2)),IszRC)
%                defines this outside of this function and loops to make things speedy
%   overlapPix- number of pixels patches are allowed to overlap vertically
%
%   OUTPUTS
%   IctrRC   - veted patch center subscripts  [maxSmp x 2]
%   IctrInd  - veted patch center indeces     [maxSmp x 1]
% --------------------------------------------------------------------
%imgNum,PszXY,LorR,ctrORedg,minMaxPixDVN,bTwoWay,nDVNbuffer,nDVN,minZoneSep,zoneTestType,zoneBufferRC,zoneMinDensity,rndSd,plotFlag,indLookup,rootDTBdir,overlapPix
unpackOpts(I,1);
unpackOpts(Opts,1);

if ~isvar('ML')
    ML=[];
end
if ~isvar('MR')
    MR=[];
end

if ~exist('overlapPix','var')|| isempty(overlapPix); overlapPix=0; end
if ~exist('indLookup','var'); indLookup=[]; end
if ~exist('rootDTBdir','var'); rootDTBdir=[]; end
if ~exist('Lpht','var') || ~exist('Rpht','var') || ...
   ~exist('Lrng','var') || ~exist('Rrng','var') || ...
   ~exist('Ldvn','var') || ~exist('Rdvn','var')
    [Lpht,Rpht,Lrng,Rrng] = loadLRSIimage(imgNum,1,0,'PHT','img',rootDTBdir,'XYZ2');
    [Ldvn,Rdvn]           = loadLRSIimageDVN(imgNum,'local',0,rootDTBdir,'DVN3');
end

%% INIT
IszRC = [1080 1920]; %DATABASE IMAGE DIMENSIONS
fig=[];

% -------------------------------------------------------------------------------

%SPECIFY CORRECT IMAGE FOR ACHOR
if strcmp(LorR,'L')
    Idvn=Ldvn;
    Irng=Lrng;
    Odvn=Rdvn;
    figH=10;
elseif strcmp(LorR,'R')
    Idvn=Rdvn;
    Irng=Rrng;
    Odvn=Ldvn;
    figH=11;
end
figH=10;

%XXX MAKE
Idvn=ceil(Idvn);

%FIND CENTERS OF DaVinci REGIONS & THEIR WIDTHS - HORIZONTAL
[IdvnCtrRC,IdvnCtrInd,~] = daVinciZoneCenterOrEdge(Idvn,minMaxPixDVN,ctrORedg,LorR,0);

%Vet CENTERS
[PctrRC,PctrInd] = daVinciZoneSampleVet(IdvnCtrInd,PszXY,zoneTestType,zoneBufferRC,zoneMinDensity,minMaxPixDVN,bTwoWay,Irng,Idvn, Odvn,LorR,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,nDVNbuffer,nDVN,minZoneSep,ML,MR);

%% SAMPLING
[IctrInd,IctrRC] = samplePatchCentersMax(PctrIndPri,PctrInd,IszRC,PszXY,indLookup,rndSd,[],overlapPix);

% ====================================================================
%% PLOTING

%PLOT VALID POINTS (BLUE & WHITE)
if plotFlag==1
    fig=figure(figH);
    if strcmp(LorR,'L')==1
        imagesc(Lpht.^.5)
    elseif strcmp(LorR,'R')==1
        imagesc(Rpht.^.5)
    end
    set(gcf,'position',[5  801  1815  537])
    set(gca,'xtick',[]); set(gca,'ytick',[]);
    set(gca,'YDir','reverse');
    colormap gray
    axis image
    hold on

    [U,V]=find(Idvn==1);
    idx=ismember([IdvnCtrRC(:,1),IdvnCtrRC(:,2)],[U,V],'rows');
    scatter(V,U,'b.')
    scatter(IdvnCtrRC(idx,2),IdvnCtrRC(idx,1),'w.');

    title({'Blue - daVinci region',...
    ['White - pixels satisfying minMaxPixDVN=' num2str(minMaxPixDVN) ],...
    ['Red - pixels satisfying zoneTestType=' ctrORedg ' and not too close to edges'],...
    'Yellow - pixels at center of sampled non-overlapping patches'})
    set(gca,'xtick',[]); set(gca,'ytick',[]);
    set(gca,'YDir','reverse');set(gca,'YDir','reverse');
elseif plotFlag==2
    fig=figure(figH);
    imagesc([Lpht,Rpht].^.5)
    set(gcf,'position',[5  801  1815  537])
    set(gca,'xtick',[]); set(gca,'ytick',[]);
    set(gca,'YDir','reverse');
    colormap gray
    axis image
    hold on

    [U,V]=find(Idvn==1);
    idx=ismember([IdvnCtrRC(:,1),IdvnCtrRC(:,2)],[U,V],'rows');
    scatter(V,U,'b.')
    scatter(IdvnCtrRC(idx,2),IdvnCtrRC(idx,1),'w.');
elseif plotFlag==3
    fig=figure(figH);
    hold on

    [U,V]=find(Idvn==1);
    idx=ismember([IdvnCtrRC(:,1),IdvnCtrRC(:,2)],[U,V],'rows');
    scatter(V+size(Lpht,2),U,'b.')
    scatter(IdvnCtrRC(idx,2)+size(Lpht,2),IdvnCtrRC(idx,1),'w.');

    title({'Left image corresponds to Left anchor, Right image to Right anchor',...
        'Blue - daVinci region',...
    ['White - pixels satisfying minMaxPixDVN=' num2str(minMaxPixDVN) ],...
    ['Red - pixels satisfying zoneTestType=' ctrORedg ' and not too close to edges'],...
    'Yellow - pixels at center of sampled non-overlapping patches'})
end
hold off

%PLOT VALID POINTS (RED)
if ~isempty(PctrRC)
    ind=PctrRC(:,2)<=size(Lpht,2) & PctrRC(:,2)>0 & PctrRC(:,1)<=size(Lpht,1) & PctrRC(:,1)>0;
    if plotFlag==1
        fig=figure(figH);
        hold on
        scatter(PctrRC(ind,2),PctrRC(ind,1),'r.');
    elseif plotFlag==2
        fig=figure(figH);
        hold on
        scatter(PctrRC(ind,2),PctrRC(ind,1),'r.');
    elseif plotFlag==3
        fig=figure(figH);
        hold on
        scatter(PctrRC(ind,2)+size(Lpht,2),PctrRC(ind,1),'r.');
    end
    hold off

    %PLOT SAMPLED POINTS (YELLOW)
    if plotFlag==1 && any(~isempty(IctrRC))
        fig=figure(figH);
        hold on
        scatter(IctrRC(:,2),IctrRC(:,1),'y','filled');
    elseif plotFlag==2 && any(~isempty(IctrRC))
        fig=figure(figH);
        hold on
        scatter(IctrRC(:,2),IctrRC(:,1),'y','filled');
    elseif plotFlag==3 && any(~isempty(IctrRC))
        fig=figure(figH);
        hold on
        scatter(IctrRC(:,2)+size(Lpht,2),IctrRC(:,1),'y','filled');
    end
end
