function [IdvnCtrRC,IdvnCtrInd,widthAll] = daVinciZoneCenterOrEdge(Idvn,minMaxPixDVN,ctrORedg,LorR,bPlot)

%function [IdvnCtrRC,IdvnCtrInd,widthAll] = daVinciZoneCenterOrEdge(Idvn,minPixDVN,ctrORedg,LorR,bPlot)
%
%   example call:
%   [Ldvn]=loadLRSIimageDVN(8,'server',0);
%   daVinciZoneCenter(Ldvn,18,1)
%
% Identifies coordinates of daVinci (i.e. half-occluded) zone centers
% that are wider than a criterion width
%
% INPUT PARAMS
% Idvn:      left- or right-eye  daVinci image
% minPixDvn: minimum pixel width of daVinci zone
% bPlot:     whether to plot
% ctrORedg:  whether center is specified as horizontal center pixel of DaVinci region
%            or the leftmost pixel
%            'ctr' -> center pixel is defined as center of patch
%            'beg' -> leftmost pixel is defined as center of patch
%            'fgnd' -> foreground pixel is defined as center of patch
% LorR       specifies wheter left or right eye image being used
%            This determines whether the beginning of the DaVinci region is begins
%            from the left (left DVN image) or right (right DVN image)a
%            For ctrORedg=='ctr' this variable doesnot matter.
%            'L' - left image
%            'R' - right image
%
% OUTPUT PARAMS
% IdvnCtrRC:     left- or right-eye subscripts of zone centers
% IdvnCtrInd:    left- or right-eye indeces of zone centers
% widthAll:      widths of IdvnCtr*
% =================================================================
if exist('bPlot','var')~=1 || isempty(bPlot); bPlot=0; end
if exist('ctrORedg','var')~=1 || isempty(ctrORedg); ctrORedg='ctr'; end
if exist('LorR','var')~=1 || isempty(LorR); LorR='L'; end
if numel(minMaxPixDVN)==1
  minMaxPixDVN=[minMaxPixDVN inf]
end

%SIZE OF IMAGE
IszRC=size(Idvn);

IdvnCtrRC=[];
widthAll=[]; %
for j = 1:IszRC(1)
    %COUNT RUNS OF HALF OCCLUDED PIXELS
    [indChg,runVal,runCnt]=runLengthEncoder(Idvn(j,:));

    %INEX OF RUN STARTS
    begs=indChg(1:end-1);
    begs=begs(runVal==1); %first DVN pixel

    %WIDTHS OF RUNS
    width=runCnt(runVal==1);

    %INDEX OF RUN ENDS
    ends=begs+width-1;  %Last DVN pixel
                        %minus 1 because begs is index 1

    %CONDITION WIDTHS AND BEGINNINGS ON BEING LARGER THAN minPixDVN
    valInd = (width>=minMaxPixDVN(1) & width<=minMaxPixDVN(2));
    begs=begs(valInd);
    ends=ends(valInd);
    width=width(valInd);

    %MID POINT OF RUN
    %XXX
    ctr=floor(width./2+1);

    if strcmp(ctrORedg,'edg')

        %BEGININGS ARE VALID POINTS - GET THEIR SUBSCRIPTS FROM BEGS/ENDS
        if strcmp(LorR,'L')
            IdvnCtrRC=[IdvnCtrRC; repmat(j,length(begs),1), begs'];
        elseif strcmp(LorR,'R')
            IdvnCtrRC=[IdvnCtrRC; repmat(j,length(ends),1), ends'];
        end

    elseif strcmp(ctrORedg,'ctr')

        %CENTERS ARE VALID POINTS - GET THEIR SUBSCRIPTS FROM BEGS
        IdvnCtrRC=[IdvnCtrRC; repmat(j,length(begs),1), begs'+ctr'];
    elseif strcmp(ctrORedg,'fgnd')
        if strcmp(LorR,'L')
            IdvnCtrRC=[IdvnCtrRC; repmat(j,length(ends),1), ends'+1];
        elseif strcmp(LorR,'R')
            IdvnCtrRC=[IdvnCtrRC; repmat(j,length(begs),1), begs'-1];
        end
	else
		error('Unhandled ctrORedg value');
    end

    if isempty(ctr)
        continue
    else
        widthAll=[widthAll; width'];
    end
end
%MAKE SURE THESE REGIONS ARE WITHIN RANGE
IdvnCtrRC=IdvnCtrRC(IdvnCtrRC(:,2)<=IszRC(2) & IdvnCtrRC(:,1)<=IszRC(1),:);

%CONVERT TO INDECES
IdvnCtrInd=sub2ind(IszRC,IdvnCtrRC(:,1),IdvnCtrRC(:,2));

if bPlot==1
    figure(1)
    imagesc(Idvn.^.5); axis image; colormap gray; hold on
    set(gca,'xtick',[]); set(gca,'ytick',[]);
    scatter(IdvnCtrRC(:,2),IdvnCtrRC(:,1),'r.');
end
