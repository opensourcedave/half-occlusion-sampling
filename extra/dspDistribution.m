function [P,D] = dspDistribution(P,D)
%function [P,D] = dspDistribution(P,D)
%GET DISPLAY PARAMETERS
D = psyPTBdisplayParameters(D,D.hostName);             % requires D.wdwPtr... PHASE OUT CALL: D.bitsOut = Screen('PixelSize',D.wdwPtr); % DISPLAY PARAMETERS

%GET STIM RECT
%D=setupPsychToolbox(D,D.stmXYdeg,D.prbXYdeg)
%sca
%D.wdwXYpix=[0 0  1920 1080];
D.prbPlySqrPix=[958.0000  538.0000  962.9221  542.8922];


%GET STIM RECT
%D.plyXYpix          = bsxfun(@times,D.stmXYdeg,D.pixPerDegXY(1,:));
%D.plySqrPix         = CenterRect([0 0 D.plyXYpix(1) D.plyXYpix(2)], D.wdwXYpix);
%D.plySqrSizXYpix    = D.plySqrPix(3:4)-D.plySqrPix(1:2);   %Square in display
%
%%GET PRB RECT
%D.prbPlyXYpix=D.plyXYpix;
%D.prbPlySqrPix=D.plySqrPix;
%D.prbPlySqrSizXYpix=D.plySqrSizXYpix;

[X,Y]=meshgrid(1:P.PszXY(1),1:P.PszXY(2));
%GET PROJECTION PLANE OF DISPLAY
[pCppXm,pCppYm,pCppZm] = psyProjPlaneAnchorEye('C',1,1,P.PszXY,D.stmXYdeg,D.hostName);

%GET STIMULUS STRETCH/SHRINK SIZE
multFactorXY     = D.stmXYdeg.*D.pixPerDegXY./P.PszXY;

%RADIUS OF PROBE TO GET CORRECT LOCATION
D.prbRadius      = abs(D.prbPlySqrPix(3) - D.prbPlySqrPix(1))/2;


%GET HDIST
[hDist]=disp2hDist(P.dspArcMinAll,[],D);

P.dspArcMinTrue=zeros(length(P.LorRall),1);
P.dspArcMinTrueChk=zeros(length(P.LorRall),1);
CountBad=0;
for s = 1:length(P.LorRall)
    [prbCurXYpix,D] = depthProbeAdjustPrbCurXYpix(D,P,s,multFactorXY,1);

    %COMPUTE DISPARITY MAP
    [~,~,~,tmp,exitflag]=disparityMapDisplay( P.AitpRC(:,:,s),P.ABitpRC(:,:,s),P.BitpRC(:,:,s),P.LorRall(s), D.stmXYdeg,P.PszXY,pCppXm,pCppYm,pCppZm);
    if exitflag==1;
        CountBad=CountBad+1;
        P.dspArcMinTrue(s)=nan;
        continue
    else
      P.dspArcMinImgDisp(:,:,s)=tmp;
    end
    %INTERPOLATE FOR DESIRED POINT
    P.dspArcMinTrue(s)  = interp2(X,Y,fliplr(P.dspArcMinImgDisp(:,:,s)),prbCurXYpix(1),prbCurXYpix(2));
    if P.LorRall(s)=='L'
        P.dspArcMinTrueChk(s) = interp2(X,Y,(P.dspArcMinImgDisp(:,:,s)),P.AitpRCbgnd(s,2)+(P.width(s)/2*multFactorXY(1)),P.BitpRCfbgnd(s,1));
    elseif P.LorRall(s)=='R'
        P.dspArcMinTrueChk(s) = interp2(X,Y,(P.dspArcMinImgDisp(:,:,s)),P.AitpRCbgnd(s,2)-(P.width(s)/2*multFactorXY(1)),P.BitpRCfbgnd(s,1));
    end
end

figure(646)
subplot(2,2,1)
[Count,Ctrs]=hist(P.dspArcMinTrue(~isnan(P.dspArcMinTrue)),20);
bar(Ctrs,Count,'FaceColor',[1 1 1],'LineWidth',2);
%disp(['Count:' num2str(CountBad)])
ylabel('Count')
xlabel('Disparity')
title([ 'Widths ' num2str(P.minMaxPixDVN(1)) '-' num2str(P.minMaxPixDVN(2)) newline 'PszXY ' num2str(P.PszXY(2)) 'x' num2str(P.PszXY(1)) newline 'Total ' num2str(length(P.LorRall)) ])
axis square

subplot(2,2,2)
[Count,Ctrs]=hist(P.dspArcMinTrueChk(~isnan(P.dspArcMinTrue)),20);
bar(Ctrs,Count,'FaceColor',[1 1 1],'LineWidth',2);
ylabel('Count')
xlabel('Disparity')
title('check from AitpRCbgnd')
axis square

subplot(2,2,3)
%scatter(P.dspArcMinTrue(~isnan(P.dspArcMinTrue))-P.dspArcMinTrueChk(~isnan(P.dspArcMinTrue)))
scatter(P.dspArcMinTrue(~isnan(P.dspArcMinTrue)),P.dspArcMinTrue(~isnan(P.dspArcMinTrue))-P.dspArcMinTrueChk(~isnan(P.dspArcMinTrue)))
axis square


subplot(2,2,4)
[Count,Ctrs]=hist(P.kRMSorig(~isnan(P.dspArcMinTrue)),20);
bar(Ctrs,Count,'FaceColor',[1 1 1],'LineWidth',2);
ylabel('Count')
xlabel('ORIGINAL RMS CONTRAST')
axis square

format long g
ind=~isnan(P.dspArcMinTrue);
errors=[P.dspArcMinTrue(ind) - P.dspArcMinTrueChk(ind)];
disp(['max diff: ' num2str(max(errors)) ]);
errors=(errors).^2;
sum(ind)
disp(['due to L: ' num2str(sum(errors(P.LorRall(ind)=='L')))])
disp(['due to L: ' num2str(sum(errors(P.LorRall(ind)=='R')))])
sum(P.LorRall(ind)=='R');
disp(['greater than .1: ' num2str(sum(errors>.1))])