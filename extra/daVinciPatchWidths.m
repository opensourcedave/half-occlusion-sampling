function P = daVinciPatchWidths(P)
%1. get widths of central half occlusion row for a given image

P.width      = zeros(size(P.LorRall));
P.endWidth   = zeros(size(P.LorRall));
P.beginWidth = zeros(size(P.LorRall));

%CENTRAL ROW
VERT=ceil(P.PszXY(2)/2);

for i = 1:length(P.LorRall)
    if P.bad(i)==1
        continue
    end

    %GET RUN LENGTHS AND VALUES OF CURRENT ROW
    if strcmp(P.LorRall(i),'L')
        dvnImg=P.LmonoBGmain(:,:,i);
    elseif strcmp(P.LorRall(i),'R')
        dvnImg=P.RmonoBGmain(:,:,i);
    end
    [oneCnt,~,indOneChg,~]=runLengthEncoderVec(dvnImg,2);
    cnts=oneCnt{VERT};
    mx=max(cnts);
    ind=find(cnts==mx);
    if isempty(mx) || numel(ind)>1
        P.bad(i)=1;
        continue
    end

    P.width(i)=mx;
    P.beginWidth(i)=indOneChg{VERT}(ind);
    P.endWidth(i)=P.beginWidth(i)+mx-1;

end