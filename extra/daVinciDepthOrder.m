function P = daVinciDepthOrder(P,bPlot)
%TODO: load images accuratley
%WANT MAIN FOREGROUND @zero

PszRC=fliplr(P.PszXY);
Pctr=ceil(PszRC./2);
blank=zeros(fliplr(P.PszXY));
colMat=repmat(1:PszRC(2),PszRC(1),1);
rowInd=1:PszRC(1);

P.LedgeMap=zeros(size(P.LdvnCrpAll));
P.RedgeMap=zeros(size(P.LdvnCrpAll));
P.MedgeMap=zeros(size(P.LdvnCrpAll));
P.SedgeMap=zeros(size(P.LdvnCrpAll));
if ~isfield(P,'bad')
    P.bad=zeros(size(P.LorRall));
end

calibType = 'PHT';
dnK       = 1;
imgType   = 'img';

loaded=[];
unqImg=unique(P.imgNumAll);
for I = 1:length(unqImg) %LOOP IN SUCH A WAY THAT UNIQUE IMAGES ARE ONLY LOADED ONCE
    if ~ismember(unqImg(I),loaded) %LOAD IMAGE IF IT HASNT BEEN LOADED YET
        loaded(end+1,1)=unqImg(I);
        try
            [~,~,~,~,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(unqImg(I),dnK,0,calibType,imgType,[],'XYZ2');
        catch
            warning('daVinciDepthOrder: Reverting to old DVN data files');
            [~,~,~,~,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(unqImg(I),dnK,0,calibType,imgType,[]);
        end
    end
    ImInd=find(P.imgNumAll==unqImg(I));
    for iI = 1:length(ImInd) %LOOP OVER IMAGES ACCORDING RELEVANT TO CURRENTLY LOADED IMAGE
        i=ImInd(iI);

        LorR=P.LorRall(i);
        LmonoBG=P.LdvnCrpAll(:,:,i);
        RmonoBG=P.RdvnCrpAll(:,:,i);
        LitpRC=P.LitpRCall(i,:);
        RitpRC=P.RitpRCall(i,:);
        LctrCrp=P.LctrCrp(i,:);
        RctrCrp=P.RctrCrp(i,:);
        Ldvn=P.LdvnCrpAll(:,:,i);
        Rdvn=P.RdvnCrpAll(:,:,i);
        Lrng=fillmissing(P.LrngCrpAll(:,:,i),'linear');
        Rrng=fillmissing(P.RrngCrpAll(:,:,i),'linear');

        [exitflag,LedgeMap,RedgeMap,LctrEdg,RctrEdg,LctrRows,RctrRows,Lsign,Rsign] = edgeFromRange(LorR,LmonoBG,RmonoBG,LitpRC,RitpRC,LctrCrp,RctrCrp,Ldvn,Rdvn,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,bPlot);
        if exitflag==1
            [10 i unqImg(I)]
            P.bad(i)=1;
            continue
        end

        %depth ordering
        if strcmp(P.LorRall(i),'L')
            try
                MedgeMap=bsxfun(@lt,colMat,LctrRows(:,2)-1); %NUMBER OF EDGES BEFORE CENTRAL REGION, LEFT SIDE, THEN RIGHT SIDE
            catch
                [20 i unqImg(I)]
            end


            %P.LorderMap(:,:,i)=(cumsum(LedgeMap.*~MedgeMap,2)) + (fliplr(cumsum(fliplr(LedgeMap.* MedgeMap),2))+ 1);
            %P.RorderMap(:,:,i)=(cumsum(RedgeMap.*~MedgeMap,2)) + (fliplr(cumsum(fliplr(RedgeMap.* MedgeMap),2))+ 1);
        elseif strcmp(P.LorRall(i),'R')
            try
                MedgeMap=bsxfun(@gt,colMat,RctrRows(:,2)+1);
            catch ME
                disp(ME.message)
                [20 i unqImg(I)]
            end

            %P.LorderMap(:,:,i)=(cumsum(LedgeMap.*~MedgeMap,2))+(fliplr(cumsum(fliplr(LedgeMap.*MedgeMap),2)));
            %P.RorderMap(:,:,i)=(cumsum(RedgeMap.*~MedgeMap,2))+(fliplr(cumsum(fliplr(RedgeMap.*MedgeMap),2)));
        end

        %SEGMENTATION
        %XXX
        L=multithresh(Lrng,2);
        L=logical(imquantize(Lrng,L)-1);
        %XXX
        R=multithresh(Rrng,2);
        R=logical(imquantize(Rrng,R)-1);

        %FURTHER OBJECTS -> HIGHER VALUES
        if mean(mean(Lrng( L)))<mean(mean(Lrng(~L)))
            L=~L;
        end

        if mean(mean(Rrng( R)))<mean(mean(Lrng(~R)))
            R=~R;
        end

        P.LorderMap(:,:,i)=L;
        P.RorderMap(:,:,i)=R;

        try
            P.LedgeMap(:,:,i)=LedgeMap;
            P.RedgeMap(:,:,i)=RedgeMap;
            P.MedgeMap(:,:,i)=MedgeMap;
        end

        if bPlot==1

            subplot(2,1,1)
            imagesc(P.LorderMap(:,:,i))
            title([P.LorRall(i) ' ' num2str(i)])
            axis image
            colorbar

            subplot(2,1,2)
            imagesc(P.RorderMap(:,:,i))
            axis image
            colorbar

            drawnow
            waitforbuttonpress
        end
    end
end

%for A, between mono & edge in non anchor direction is BFG
%for B, between edge & mono regions is BFG
%keep track of mono edges in between - value must align
