function P=getBinoMain(P,bPlot)
P.LbinoFGmain=zeros(size(P.LmonoBGmain));
P.RbinoFGmain=zeros(size(P.RmonoBGmain));
P.LbinoBGmain=zeros(size(P.LmonoBGmain));
P.RbinoBGmain=zeros(size(P.RmonoBGmain));
PszRC=fliplr(P.PszXY);
zer=zeros(size(P.LmonoBGmain(:,:,1)));
for i = 1:length(P.LorRall)
    LorR=P.LorRall(i);
    if LorR=='L'
        monoA=P.LmonoBGmain(:,:,i);
        monoB=P.RmonoBGmain(:,:,i);
        fgA=P.LbinoFG(:,:,i);
        fgB=P.RbinoFG(:,:,i);

    elseif LorR=='R'
        monoA=P.RmonoBGmain(:,:,i);
        monoB=P.LmonoBGmain(:,:,i);
        fgA=P.RbinoFG(:,:,i);
        fgB=P.LbinoFG(:,:,i);
    end
    %----------------------------------------------------------------
    %% GET MAIN BINO FG BASED ON OBJECT PIXEL COUNT

    bIndFGA=largest_obj_2_bInd(fgA,PszRC,zer);
    bIndFGB=largest_obj_2_bInd(fgB,PszRC,zer);

    bIndBGA=~bIndFGA & ~monoA;
    bIndBGB=~bIndFGB & ~monoB;

    %----------------------------------------------------------------
    %% SAVE TO STRUCT
    if LorR=='L'
        P.LbinoFGmain(:,:,i)=bIndFGA;
        P.RbinoFGmain(:,:,i)=bIndFGB;

        P.LbinoBGmain(:,:,i)=bIndBGA;
        P.RbinoBGmain(:,:,i)=bIndBGB;
    elseif LorR=='R'
        P.LbinoFGmain(:,:,i)=bIndFGB;
        P.RbinoFGmain(:,:,i)=bIndFGA;

        P.LbinoBGmain(:,:,i)=bIndBGB;
        P.RbinoBGmain(:,:,i)=bIndBGA;
    end
    %----------------------------------------------------------------
    %% PLOT
    if bPlot
        figure(2)
        n=[4,2];

        %M
        subPlot(n,1,1)
        imagesc(P.LmonoBGmain(:,:,i))
        title([num2str(i) ' ' LorR newline ' Mono'])
        formatImage;
        if LorR=='L'
            c=caxis;
        end

        subPlot(n,1,2)
        imagesc(P.RmonoBGmain(:,:,i))
        title('Mono')
        formatImage;
        if LorR=='R'
            c=caxis;
            subPlot(n,1,1)
            caxis(c);
        else
            caxis(c);
        end

        %FG All
        subPlot(n,2,1)
        imagesc(P.LbinoFG(:,:,i))
        title('FG All')
        formatImage;

        subPlot(n,2,2)
        imagesc(P.RbinoFG(:,:,i))
        title('FG All')
        formatImage;


        %FG
        subPlot(n,3,1)
        imagesc(P.LbinoFGmain(:,:,i))
        title('FG main')
        formatImage;

        subPlot(n,3,2)
        imagesc(P.RbinoFGmain(:,:,i))
        title('FG main')
        formatImage;

        %BG
        subPlot(n,4,1)
        imagesc(P.LbinoBGmain(:,:,i))
        title('BG')
        formatImage;

        subPlot(n,4,2)
        imagesc(P.RbinoBGmain(:,:,i))
        title('BG main')
        formatImage;

        drawnow
        waitforbuttonpress
    end
end

%----------------------------------------------------------------
%% FUNCTIONS
end
function bInd=largest_obj_2_bInd(BW,PszRC,zer)
    CC = bwconncomp(BW);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    [biggest,idx] = max(numPixels);
    zer(CC.PixelIdxList{idx}) = 1;
    bInd=zer;
end
