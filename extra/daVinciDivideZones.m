function P = daVinciDivideZones(P,bPlot)
%DOESNT HANDLE MULTPIPLE DAVINCI ZONES
P.monoBG =zeros(size(P.LdvnCrpAll));;
P.LbinoBG=zeros(size(P.LdvnCrpAll));;
P.LbinoFG=zeros(size(P.LdvnCrpAll));
P.RbinoBG=zeros(size(P.LdvnCrpAll));
P.RbinoFG=zeros(size(P.LdvnCrpAll));
P.bad=zeros(size(P.LorRall));

for i = 1:length(P.LorRall)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % SEPERATE IMAGE INTO monoBG, binoBG, binoFG
    LmonoBG=round(P.LdvnCrpAll(:,:,i))==1;
    RmonoBG=round(P.RdvnCrpAll(:,:,i))==1;

    if strcmp(P.LorRall(i),'L')
        width=sum(LmonoBG,2);
        ctr=round(width./2);
        c=zeros(size(LmonoBG,1),1);
        d=zeros(size(LmonoBG,1),1);
        ind=zeros(size(LmonoBG,1),1);
        Limg_L=zeros(size(LmonoBG));
        Rimg_L=zeros(size(LmonoBG));
        for j = 1:size(LmonoBG,1)
            %GET L DVN EDGE FOR A GIVEN ROW
            [~,tmp,~]=unique(LmonoBG(j,:),'first');
            if numel(tmp)==2
                c(j)=max(tmp);
            elseif numel(tmp)==1
                c(j)=0;
            end
            LctrBound=ctr(j)+c(j)-2;
            try
                Limg_L(j,:)=[ ones(LctrBound,1); zeros(P.PszXY(1)-LctrBound,1 ) ];
            catch ME
                P.bad(i)=1;
            end

            %GET L DVN EDGE FOR A GIVEN ROW
            [~,tmp,~]=unique(fliplr(LmonoBG(j,:)),'first');
            if numel(tmp)==2
                d(j)=P.PszXY(1)-max(tmp)+2;
            elseif numel(tmp)==1
                d(j)=0;
            end
            DVedge(j,:)=[j d(j)];

            %FIND CORRESPONDING POINT INDECES BY FINDING CLOSEST POINTS
            distances = sum(bsxfun(@minus, P.AitpRC(:,:,i), DVedge(j,:)).^2,2);
            tmp2      = find(distances==min(distances));
            RctrBound=mean(P.BitpRC(tmp2,:,i),1);
            RctrBound=round(RctrBound(2));
            try
                Rimg_L(j,:)=[ ones(RctrBound,1); zeros(P.PszXY(1)-RctrBound,1 ) ];
            catch ME
                P.bad(i)=1;
            end

            %ind(j)    = tmp2(1)
        end
        %GET L_img ZONES
        Limg_R=~Limg_L;
        Limg_L=Limg_L & ~LmonoBG;
        Limg_R=Limg_R & ~LmonoBG;
        Rimg_R=~Rimg_L;

        P.monoBG(:,:,i)=LmonoBG;
        P.LbinoBG(:,:,i)=Limg_L;
        P.LbinoFG(:,:,i)=Limg_R;
        P.RbinoBG(:,:,i)=Rimg_L;
        P.RbinoFG(:,:,i)=Rimg_R;
    elseif strcmp(P.LorRall(i),'R')
        width=sum(RmonoBG,2);
        ctr=round(width./2);
        c=zeros(size(RmonoBG,1),1);
        d=zeros(size(RmonoBG,1),1);
        ind=zeros(size(RmonoBG,1),1);
        Limg_L=zeros(size(RmonoBG));
        Rimg_L=zeros(size(RmonoBG));
        for j = 1:size(RmonoBG,1)
            %GET R DVN EDGE FOR A GIVEN ROW
            [~,tmp,~]=unique(fliplr(RmonoBG(j,:)),'first');
            if numel(tmp)==2
                c(j)=P.PszXY(1)-max(tmp);
            elseif numel(tmp)==1
                c(j)=0;
            end
            LctrBound=c(j)-ctr(j)+2;
            Rimg_R(j,:)=[ zeros(LctrBound,1); ones(P.PszXY(1)-LctrBound,1 ) ];

            %GET R DVN EDGE FOR A GIVEN ROW
            [~,tmp,~]=unique((RmonoBG(j,:)),'first');
            if numel(tmp)==2
                d(j)=max(tmp)-2;
            elseif numel(tmp)==1
                d(j)=0;
            end
            DVedge(j,:)=[j d(j)];

            %FIND CORRESPONDING POINT INDECES BY FINDING CLOSEST POINTS
            distances = sum(bsxfun(@minus, P.AitpRC(:,:,i), DVedge(j,:)).^2,2);
            tmp2      = find(distances==min(distances));
            RctrBound=mean(P.BitpRC(tmp2,:,i),1);
            RctrBound=round(RctrBound(2));
            try
                Limg_L(j,:)=[ ones(RctrBound,1); zeros(P.PszXY(1)-RctrBound,1 ) ];
            catch ME
                P.bad(i)=1;
            end

            %ind(j)    = tmp2(1)
        end
        %GET L_img ZONES
        Rimg_L=~Rimg_R;
        Rimg_L=Rimg_L & ~RmonoBG;
        Rimg_R=Rimg_R & ~RmonoBG;
        Limg_R=~Limg_L;

        P.monoBG(:,:,i)=RmonoBG;
        P.LbinoBG(:,:,i)=Limg_R;
        P.LbinoFG(:,:,i)=Limg_L;
        P.RbinoBG(:,:,i)=Rimg_R;
        P.RbinoFG(:,:,i)=Rimg_L;
    end
    if bPlot == 1
        figure(1)
        if strcmp(P.LorRall(i),'L')
            imagesc(LmonoBG)
        elseif strcmp(P.LorRall(i),'R')
            imagesc(RmonoBG)
        end
        axis image
        drawnow

        figure(2)
        subplot(3,2,1)
        imagesc(Limg_L)
        if strcmp(P.LorRall(i),'L')
            hold on
            scatter(c+ctr+2,1:size(LmonoBG),'g')
            hold off
        end
        title('Leye')
        axis image

        subplot(3,2,3)
        imagesc(Limg_R)
        if strcmp(P.LorRall(i),'L')
            hold on
            scatter(d,1:size(LmonoBG),'r')
            hold off
        end
        axis image

        subplot(3,2,5)
        imagesc(P.LphtCrpAll(:,:,i))
        axis image

        %%

        subplot(3,2,2)
        imagesc(Rimg_L)
        if strcmp(P.LorRall(i),'R')
            hold on
            scatter(d,1:size(RmonoBG),'r')
            hold off
        end
        title('Reye')
        axis image

        subplot(3,2,4)
        imagesc(Rimg_R)
        if strcmp(P.LorRall(i),'R')
            hold on
            scatter(c-ctr+2,1:size(LmonoBG),'g')
            hold off
        end
        axis image

        subplot(3,2,6)
        imagesc(P.RphtCrpAll(:,:,i))
        axis image

        drawnow
        waitforbuttonpress
        end
end

%P.monoBG =logical(P.monoBG);
%P.LbinoBG=logical(P.LbinoFG);
%P.RbinoBG=logical(P.RbinoFG);
%P.LbinoBG=logical(P.LbinoFG);
%P.RbinoBG=logical(P.RbinoFG);
