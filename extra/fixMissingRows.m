function [IctrRows,IctrEdg]=fixMissingRows(IctrRows,IctrEdg,Isign,PszRC,allRows,blank)

%FILL BORDER INTERPOLATE
missing=allRows(~ismember(allRows,IctrRows(:,1)));
if ~isempty(missing)
    s=0;
    try
        missing2=interp1Clip(IctrRows(:,1),IctrRows(:,2),missing);
        s=1;
    catch ME
        disp(ME.message)
    end
    if s==1
        IctrRows=sortrows([IctrRows; missing', missing2']);
    end
else
    s=2;
end

%INTERPOLATE NANs
lind=isnan(IctrRows(:,2));
if sum(lind)>1
    s=0;
    try
        tmp=interp1(IctrRows(~lind,1),IctrRows(~lind,2),IctrRows(lind));
        s==1;
    catch ME
        disp(ME.message)
    end
    if s==1
        IctrRows(lind,2)=tmp;
    end
end

ind=sub2ind(PszRC,IctrRows(:,1),round(IctrRows(:,2)));
if ~isempty(ind)
    IctrEdg(ind)=Isign;
else
    IctrEdg=[];
end