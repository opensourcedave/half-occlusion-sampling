function P = daVinciDivideZones2(P,bPlot)

%if ~isfield(P,'LedgeMap') || ~isfield(P,'RedgeMap') || ~isfield(P,'MedgeMap') || ~isfield(P,'SedgeMap')
    P=daVinciDepthOrder(P,0);
%end
P.LmonoBGmain=zeros(size(P.LdvnCrpAll));
P.RmonoBGmain=zeros(size(P.LdvnCrpAll));

for i = 1:length(P.LorRall)
   if isfield(P,'bad') && P.bad(i)==1
       continue
   end

   if P.LorRall(i)=='L'
       ctr=P.LitpRCchkAll(i,:)-P.LitpRCchkDspAll(i,:)+fliplr(P.PszXY)/2;
       P.LmonoBGmain(:,:,i)=objByLoc(P.LdvnCrpAll(:,:,i),ctr,300);
       P.RmonoBGmain(:,:,i)=zeros(size(P.LdvnCrpAll,1),size(P.LdvnCrpAll,2));
   elseif P.LorRall(i)=='R'
       ctr=P.RitpRCchkAll(i,:)-P.RitpRCchkDspAll(i,:)+fliplr(P.PszXY)/2;
       P.RmonoBGmain(:,:,i)=objByLoc(P.RdvnCrpAll(:,:,i),ctr,300);
       P.LmonoBGmain(:,:,i)=zeros(size(P.LdvnCrpAll,1),size(P.LdvnCrpAll,2));
   end
   P.LmonoBG(:,:,i)    =objByThreshold(P.LdvnCrpAll(:,:,i),300);
   P.RmonoBG(:,:,i)    =objByThreshold(P.RdvnCrpAll(:,:,i),300);

   %P.LmonoBG(:,:,i)=round(P.LdvnCrpAll(:,:,i))==1;
   %P.RmonoBG(:,:,i)=round(P.RdvnCrpAll(:,:,i))==1;
   if P.LorRall(i)=='L'
       %P.LbinoBG(:,:,i)=(P.MedgeMap(:,:,i) & P.LorderMap(:,:,i)>0 & ~P.LmonoBGmain(:,:,i));
       P.LbinoBG(:,:,i)=(P.LorderMap(:,:,i) & ~P.LmonoBGmain(:,:,i));
       P.RbinoBG(:,:,i)=(P.RorderMap(:,:,i) & ~P.SedgeMap(:,:,i));

       P.LbinoFG(:,:,i)=(~P.LbinoBG(:,:,i) & ~P.LmonoBGmain(:,:,i));
       P.RbinoFG(:,:,i)=(P.RorderMap(:,:,i)<=0 & ~P.SedgeMap(:,:,i));
       %P.RbinoFG(:,:,i)=(P.RorderMap(:,:,i)<=0 );
   elseif P.LorRall(i)=='R'
       P.RbinoBG(:,:,i)=(P.RorderMap(:,:,i) & ~P.RmonoBGmain(:,:,i));
       P.LbinoBG(:,:,i)=(P.LorderMap(:,:,i) & ~P.SedgeMap(:,:,i));

       P.LbinoFG(:,:,i)=(~P.SedgeMap(:,:,i) & P.LorderMap(:,:,i)<=0);
       %P.RbinoFG(:,:,i)=(~P.MedgeMap(:,:,i) & P.RorderMap(:,:,i)<=0) & ~P.RmonoBGmain(:,:,i);
       %P.RbinoBG(:,:,i)=(P.MedgeMap(:,:,i) & P.RorderMap(:,:,i)>0 & ~P.RmonoBGmain(:,:,i));
       P.RbinoFG(:,:,i)=(~P.RbinoBG(:,:,i) & ~P.RmonoBGmain(:,:,i));
   end

   if bPlot==1
       %PHT
       subplot(4,2,1)
       imagesc(P.LphtCrpAll(:,:,i))
       title([P.LorRall(i) ' ' num2str(i)])
       axis image

       subplot(4,2,2)
       imagesc(P.RphtCrpAll(:,:,i))
       axis image

       %FG
       subplot(4,2,3)
       imagesc(P.LbinoFG(:,:,i))
       title('Bino FG')
       axis image

       subplot(4,2,4)
       imagesc(P.RbinoFG(:,:,i))
       axis image

       %MONO
       subplot(4,2,5)
       imagesc(P.LmonoBG(:,:,i))
       title('Mono BG')
       axis image

       subplot(4,2,6)
       imagesc(P.RmonoBG(:,:,i))
       axis image

       %BG
       subplot(4,2,7)
       imagesc(P.LbinoBG(:,:,i))
       title('Bino BG')
       axis image

       subplot(4,2,8)
       imagesc(P.RbinoBG(:,:,i))
       axis image


       drawnow
       waitforbuttonpress
   end
end