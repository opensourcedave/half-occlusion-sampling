function P = daVinciPostVet(P)
disp([num2str(sum(~P.bad)) ' valid patches out of ' num2str(length(P.bad)) ]);
%P.bad=P.bad | vertcat(squeeze(max(max(P.depthImgDisp))))<.996;
%P.bad=P.bad | vertcat(squeeze(prctile(prctile(P.depthImgDisp,.9),.9)))<1;
disp([num2str(sum(~P.bad)) ' valid patches out of ' num2str(length(P.bad)) ]);

figure(nFn)

subPlot([1,2],1,1)
[counts,center]=hist(P.imgNumAll(P.bad));
bar(center,counts);
formatFigure('imgNum','','Bad')

subPlot([1,2],1,2)
[counts,center]=hist(P.imgNumAll(~P.bad));
bar(center,counts);
formatFigure('imgNum','','Good')

%BAD=evalin('base','BAD');
%BAD(end+1)=sum(~P.bad);
%assignin('base','BAD',BAD)
