function fname = daVinciPatchFname(imgNum,saveDTB,dspArcMin,minMaxPixDVN,fgndORbgnd,ctrORedg,PszXY,rndSd)
% function fname = daVinciPatchFname(saveDTB,dspArcMin,minMaxPixDVN,fgndORbgnd,ctrORedg,PszXY,rndSd)
%   Create file name for daVinciPatch saving

rng = num2rangeStr(imgNum);

fname='daVinciZonePatches';
fname=[saveDTB filesep fname '_dspArcmin_' num2str(dspArcMin) '_minMaxPixDVN_' ...
       num2str(minMaxPixDVN(1)) '-' num2str(minMaxPixDVN(2)) '-' ctrORedg '-' ...
       fgndORbgnd '_Psz_' num2str(PszXY(1)) 'x' num2str(PszXY(2)) '_rndSd_' num2str(rndSd) ...
       'imgNum' rng];
