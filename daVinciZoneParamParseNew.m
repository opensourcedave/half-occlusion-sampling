function H=daVinciZoneParamParseNew(H,bBatch)
%{
function H=daVinciZoneParamParse(H,bBatch)
 example calls:
       H=daVinciZoneParamParse();  %Create options struct consisting entirely of defaults

       H=daVinciZoneParamParse(H); %Add missing params and check existing params to options struct

 LIST OF FIELDS
 -------------------------------------------------------------------------------------------------
 minMaxPixDVNall  %MINIMUM AND MAX WIDTH IN PIXELS OF HALF OCCLUDED REGION TO SAMPLE FROM

 SEE "REMOVE INVALID SAMPLING POINTS" SECTION OF sampleHalfOccludedCtrs FOR DETAIL ON BELOW
 zoneBufferRCall - zoneBurfferRCs for all  minMaxPixDVN (minMaxPixDVN)
                   zoneBufferRC - WHETHER DVN REGION, HAVING A MINIMUM OF WIDTH OF minMaxPixDVN(1)-zoneBufferRC(2),
                                  IS CONTINOUS VERTICALLY FOR PszXY(2)-zoneBufferRC(1) pixels.
                                  IF zoneBufferRC(2)==0, THEN DVN REGION MUST HAVE A WIDTH == minPixDVN
                                      THROUGHOUT THE VERTICAL CONDITION IN ORDER TO BE COUNTED AS VALID
                                  IF zoneBufferRC(1)==0,
                                      THEN DVN REGION MUST EXTEND VERTICALLY THROUGH ENTIRE PATCH
%%

 imgNum       - range of images to sue from    [nImg x 1]
 LorRorB      - what to use as an anchor
               'L' -> only use left eyes as anchor
               'R' -> only use right eye as anchor
               'B' -> alternate betewee left and right anchors
 bPlot        - plot or not
 plotImgNum   - image number to use for example plotting: must be in range of imgNum to
 SET ONE OF THESE VARIABLES - nSmpPerImg OVERRIDES nSmp
    nSmp         - number of total samples desired across all images and anchors and produce a plot
    nSmpPerImg   - number of samples per image desired. Determined from nSmp if unspecified.
 PszXY        - desired horizontal by vertical patch size [1 x 2]
 ctrORedg     - whether the patch center is at the beginning or end of daVinci region
                'ctr' -> patch center is at center  pixel of daVinci region
                'edg' -> patch center is at leading pixel of daVinci region
 fgndORbgnd   - fixation on foreground or background surface
   	 		 'fgnd' foreground
			 	 'bgnd' background
 minMaxPixDVN    - minimum and max width of daVinci region for patch center
 nDVNbuffer  - size of DVN region in corresponding patch to be considered significant (superSlice method only)
 nDVN         - number of DVN regions allowed in each corresponding patch (superSlice method only)
 minZoneSep   - required horizontal distance between central and additional significant DVN zones. (superSlice method only)
 zoneTestType - whether to use 'slice' or 'density' vetting for patch center
                'slice'      -> daVinci region width must have min width at each row of the patch
                'superSlice' -> slice method, but allows for non-significant DVN zones, and allows
                                a specified number of sigificant DVN zones if they are seperated
                                horizontally by a specified distance.
                                See nDVNbuffer, nDVN, minZoneSep.
                'density'    -> daVinci region must have a specified density within patch region
 zoneBufferRC   row-column buffer for 'slice' method... if there are R rows in a patch
                there must be (R-zoneBufferRC(1)) rows with width of (minMaxPixDVN(1)-zoneBufferRC(2))
 zoneBufferRC - buffer for 'slice' method: width can be minMaxPixDVN(:,1)-zoneBufferRC(2) in length
                to count as an extended region in patch area classified as a valid patch
 zoneMinDensity- density of daVinci pixels to non-daVinci pixels in patch region to be
                 classified as a valid patch
 dspArcMin    - how much disparity in arcMinutes to add to stereo-patches
 rndSd        - random seed to use for random sampling
                0 -> non-random sampling that maximizes number of samples
 W            - crop window
                [] -> no crop window (default)
                *optional for burge lab - TODO add this functionality
 overlapPix   - number of pixels patches are allowed to overlap vertically

 bDisplayParams - whether to compute additional parameters.
 DC             -
 RMS            - fixed RMS contrast value
 WK             - window to use
 b2ctr
 hostname       - hostName of computer to compute display parameters for (considers monitor parameters)
%}

if ~isvar('H')
    H=struct();
end

p = inputParser();

if ~isvar('bBatch')
    bBatch=0;
end
if bBatch
    p.addParameter('zoneBufferRCall',-1,@isnumeric);
    p.addParameter('dspArcMinAll',0,@isnumeric);
    p.addParameter('minMaxPixDVNall',[18 inf],@isnumeric);
else
    p.addParameter('zoneBufferRC',-1,@isnumeric);
    p.addParameter('dspArcMin',0,@isnumeric);
    p.addParameter('minMaxPixDVN',[18 inf],@isnumeric);
    % REMOVE IRRELEVANT FIELDS IF CREATED FROM BATCH
    if isvar('H') && isfield(H,'zoneBufferRCall')
        H=rmfield(H,'zoneBufferRCall');
    end
    if isvar('H') && isfield(H,'dspArcMinAll')
        H=rmfield(H,'dspArcMinAll');
    end
    if isvar('H') && isfield(H,'minMaxPixDVNall')
        H=rmfield(H,'minMaxPixDVNall');
    end
    if isvar('H') && isfield(H,'Wk')
        H=rmfield(H,'Wk');
    end
    if isvar('H') && isfield(H,'bCreate')
        H=rmfield(H,'bCreate');
    end
end

p.addParameter('rootDTBdir','',@ischar);
p.addParameter('saveDTB','',@ischar);
p.addParameter('bSave',1,@isnumeric);
p.addParameter('imgNums',8,@isnumeric);
p.addParameter('bPlot',1,@isnumeric);
p.addParameter('plotImgNum',8,@isnumeric);
p.addParameter('bSaveFig',0,@isnumeric);
p.addParameter('LorRorB','B',@ischar);
p.addParameter('fgndORbgnd','fgnd',@ischar);
p.addParameter('PszXY',[130 50],@isnumeric);
p.addParameter('rndSd',1,@isnumeric);
p.addParameter('nSmpPerImg',20,@isnumeric);
p.addParameter('nSmp',100,@isnumeric);
p.addParameter('ctrORedg','edg',@ischar);
p.addParameter('zoneTestType','slice',@ischar);
p.addParameter('zoneMinDensity',.2,@isnumeric);
p.addParameter('DC',[],@isnumeric);
p.addParameter('RMS',[],@isnumeric);
p.addParameter('hostName',[],@ischar);
p.addParameter('bDisplayParams',0,@isnumeric);
p.addParameter('bPreDispDsp',0,@isnumeric);
p.addParameter('stmXYdeg',[3 1],@isnumeric);
p.addParameter('b2ctr',0,@isnumeric);
p.addParameter('overlapPix',0,@isnumeric);
p.addParameter('nDVN',                    1,          @isnumeric);
p.addParameter('nDVNbuffer',              30,          @isnumeric);
p.addParameter('minZoneSep',              10,          @isnumeric);
p.addParameter('bTwoWay',              0,               @isnumeric);
p.addParameter('monoORbino', 'bino', @ischar);
p.addParameter('bByImage', 0, @isnumeric);
p.addParameter('IszRC',[1080 1920], @isnumeric);
p.addParameter('bDebug',0, @isnumeric);
p.addParameter('PctrIndPri',[],@isnumeric);
p.addParameter('PctrIndPriAll',{},@iscell);

p.addParameter('imgNumOrig',[], @isnumeric);
p.addParameter('Wk',0,@isnumeric);
p.addParameter('W',[],@isnumeric);

if isvar('H') && isfield(H,'bDebug')
    bDebug=H.bDebug;
else
    bDebug=0;
end

p=parseStruct(H,p,~bDebug,~bDebug);
H=p.Results;

% XXX is this a good idea?
if any(mod(H.PszXY,2)==0)
    warning('PszXY must be odd!')
end

%MODIFY SOME PARAMETERS
if isfield(H,'zoneBufferRCall') && isequal(H.zoneBufferRCall,-1)
    H.zoneBufferRCall(:,2) = H.minMaxPixDVNall(:,1)-5;
    H.zoneBufferRCall(:,1) = zeros(size(H.minMaxPixDVNall,1),1);
end

%imgExc = [8,36,43,60,65,86,94]; %Images that have bad registration, don't change!
% XXX
imgExc = [36,43,60,65,86,94]; %Images that have bad registration, don't change!
H.imgNumOrig=H.imgNum;
ind=ismember(H.imgNum,imgExc);
H.imgNum=H.imgNum(~ind);

if sum(ind)~=0
    warning(['daVinciZoneSampleBatch: excluding image(s) ' num2rangeStr(H.imgNumOrig(ind)) '.'])
end

if isempty(H.W)
    H=rmfield(H,'W');
end
if isfield(H,'Wk') && ~isequal(H.Wk , 0) && ~isfield(H,'Wk')
    % XXX
    H.W=[];
    %H.W = cosWindowRect([H.PszXY(2),H.PszXY(1)],H.Wk);
else
    H.W=[];
end
