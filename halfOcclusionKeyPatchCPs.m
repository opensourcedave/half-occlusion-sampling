function P = halfOcclusionKeyPatchCPs(P,b2ctr,fgndORbgnd,PszXY)
% function P = halfOcclusionKeyPatchCPs(P,b2ctr,fgndORbgnd)
%   Returns key CPs in patch dimension
%
% b2ctr      - whether bring fixation to the projection plane by shifting CPs by a fixed amount
%              0 -> dont shift
%              1 -> shift
% fgndORbgnd - whether patch center is binocular foreground or monocular background
% PszXY      - patch size in pixels [1x2]
%
% AitpRCfgnd    - foreground fixation point for anchor eye
% AitpRCbgnd    - background fixation point for anchor eye (half occlusion region)
% AitpRCfbgnd   - background/foreground fixation point for non-anchor eye
if (~isfield(P,'PszXY') && exist('PszXY','var')) || exist('PszXY','var')
    P.PszXY=PszXY;
end
if (~isfield(P,'fgndORbgnd') && exist('fgndORbgnd','var')) || exist('fgndORbgnd','var')
    P.fgndORbgnd=fgndORbgnd;
end
if ~exist('b2ctr','var') || isempty(b2ctr)
    b2ctr=0;
end

%GET CENTER PATCH CPs (MONO-BGND AND FIXATION-FGND)
ctrPix=[P.PszXY(2), P.PszXY(1)]/2;
%P.AitpRCfgnd  = zeros(length(P.LorRall),2);
P.AitpRCbgnd  = zeros(length(P.LorRall),2);
P.BitpRCfbgnd = zeros(length(P.LorRall),2);
%
Lind = P.LorRall=='L';
Rind = P.LorRall=='R';
%
if b2ctr==1
    L_LRSIctr =P.LitpRCchkDspAll(Lind,:);
    BR_LRSIctr=P.RitpRCdspAll(Lind,:);

    R_LRSIctr =P.RitpRCchkDspAll(Rind,:);
    BL_LRSIctr=P.LitpRCdspAll(Rind,:);
elseif b2ctr==0
    L_LRSIctr =P.LitpRCchkDspAll(Lind,:);
    BR_LRSIctr=P.LitpRCdspAll(Lind,:);

    R_LRSIctr =P.RitpRCchkDspAll(Rind,:);
    BL_LRSIctr=P.RitpRCdspAll(Rind,:);
end
%
if     strcmp(P.fgndORbgnd,'fgnd')
    %% L
    P.AitpRCfgnd( Lind,:) = bsxfun(@plus, P.LitpRCchkAll(Lind,:) -  L_LRSIctr,ctrPix);
    P.AitpRCbgnd( Lind,:) = bsxfun(@plus, P.LitpRCall(Lind,:)    -  L_LRSIctr,ctrPix);

    P.BitpRCfbgnd(Lind,:) = bsxfun(@plus, P.RitpRCall(Lind,:)    - BR_LRSIctr,ctrPix);

    %% R
    P.AitpRCfgnd( Rind,:) = bsxfun(@plus, P.RitpRCchkAll(Rind,:) -  R_LRSIctr,ctrPix);
    P.AitpRCbgnd( Rind,:) = bsxfun(@plus, P.RitpRCall(Rind,:)    -  R_LRSIctr,ctrPix);

    P.BitpRCfbgnd(Rind,:) = bsxfun(@plus, P.LitpRCall(Rind,:)    - BL_LRSIctr,ctrPix);
elseif strcmp(P.fgndORbgnd,'bgnd')
    %% L
    P.AitpRCfgnd( Lind,:) = bsxfun(@plus, P.LitpRCall(Lind,:)    -  L_LRSIctr,ctrPix);
    P.AitpRCbgnd( Lind,:) = bsxfun(@plus, P.LitpRCchkall(Lind,:) -  L_LRSIctr,ctrPix);

    P.BitpRCfbgnd(Lind,:) = bsxfun(@plus, P.RitpRCall(Lind,:)    - BR_LRSIctr,ctrPix);

    %% R
    P.AitpRCfgnd( Rind,:) = bsxfun(@plus, P.RitpRCall(Rind,:)    -  R_LRSIctr,ctrPix);
    P.AitpRCbgnd( Rind,:) = bsxfun(@plus, P.RitpRCchkAll(Rind,:) -  R_LRSIctr,ctrPix);

    P.BitpRCfbgnd(Rind,:) = bsxfun(@plus, P.LitpRCall(Rind,:)    - BL_LRSIctr,ctrPix);
end