edgesFromCPs(LorR,Ldvn,Rdvn,LitpRCall,IdvnCtrRC,RitpRCall,PixelIdxList,nDVNbuffer)
PszRC=size(Ldvn);
Lmap=zeros(PszRC);
Rmap=zeros(PszRC);

%%%%%%%%%%%%%%%%%%%%%%
% L
CC=bwconncomp(Ldvn);
numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of DVN regions
N=numels(numels>nDVNbuffer); %number of significant DVN regions

%FIND L DVN EDGES
Ledges=zeros(0,2)
for i = 1:length(N)
    PIL=sortrows(CC.PixelIdxList(N(i)));
    ind=find(diff(PIL))-1;
    ind=ind(2:end);
    LedgesRC=[Ledges; PIL(ind)]
end
if LorR=='L'
    [N,co]=findCenter(CC,N,PszRC,IdvnCtrRC);
    ind=ismember(Ledges,P.PixelIdxList(N(co)),'rows');
    CLedges=Ledges(ind,:);
    ind=~ismember(1:PszRC(1),CLedges(:,1)); %missing elements
    tmp=[ind, ones(length(ind))];
    CLedges=[CLedges tmp];
end
%L -> R
RnDvnEdgesRC=nearestCPs(LedgesRC,LitpRCall,RitpRCall); %XXX shit solution
%XXX get corresponding ctr edges

%%%%%%%%%%%%%%%%%%%%%%
% R
CC=bwconncomp(Rdvn);
numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of DVN regions
N=numels(numels>nDVNbuffer); %number of significant DVN regions

%FIND R DVN EDGES
Redges=zeros(0,2)
for i = 1:length(N)
    PIL=sortrows(CC.PixelIdxList(N(i)));
    ind=find(diff(PIL));
    RedgesRC=[Redges; PIL(ind)];
end
if LorR=='R'
    [N,co]=findCenter(CC,N,PszRC,IdvnCtrRC);
    ind=ismember(RedgesRC,P.PixelIdxList(N(co)),'rows');
    CRedges=Redges(ind,:); %MAIN DVN EDGE
    ind=~ismember(1:PszRC(1),CRedges(:,1)); %missing elements
    tmp=[ind, ones(length(ind))];
    CRedges=[CRedges tmp];
end

%R -> L
LnDVNEdgesRC=nearestCPs(RedgesRC,RitpRCall,LitpRCall); %XXX shit solution
%XXX get corresponding ctr edges

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

Lmap(LnDVNEdgesRC(:,1),LnDVNEdgesRC(:,2))= 1;
Rmap(RnDVNEdgesRC(:,1),RnDVNEdgesRC(:,2))= 1;
Lmap( LDVNEdgesRC(:,1), LDVNEdgesRC(:,2))=-1;
Rmap( RDVNEdgesRC(:,1), RDVNEdgesRC(:,2))=-1;
Ltmp=Lmap*-1;
Rtmp=Lmap*-1;

for i = 1:PszRC(1)
    cumsum(Lmap(CLedges(i,2)+1:end,1),2,'reverse') %moving left
    cumsum(Ltmp(CLedges(i,2)-1:end,1),2)           %moving right
    cumsum(Rmap(CRedges(i,2)+1:end,1),2,'reverse') %moving left
    cumsum(Rtmp(CRedges(i,2)-1:end,1),2)           %moving right
end

end
function [N,co]=findCenter(CC,N,PszRC,IdvnCtrRC)
ctrind=subs2ind(PszRC,IdvnCtrRC(j,1),IdvnCtrRC(j,2))
co=[];
for i = 1:length(N)
    if ismember(ctrind,CC.PixelIdxList(N(i)))
        co=i;
        break
    end
end
