function P = edgeFromRangeBatch(P,bPlot)
if ~exist('bPlot','var') || isempty(bPlot)
    bPlot=0;
end

%Load range data
calibType = 'PHT';
dnK       = 1;
imgType   = 'img';

blank=zeros(fliplr(P.PszXY));
W  = ones(size(P.LphtCrpAll(:,:,1)));
PszRC=fliplr(P.PszXY);
Pctr=ceil(PszRC./2);
P.LedgeMap=zeros(size(P.LphtCrpAll));
P.RedgeMap=zeros(size(P.LphtCrpAll));
for i = 1:length(P.LorRall)
    [~,~,~,~,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(P.imgNumAll(i),dnK,0,calibType,imgType,'','XYZ2');

    LorR=P.LorRall(i);
    LmonoBG=P.LdvnCrpAll(:,:,i);
    RmonoBG=P.RdvnCrpAll(:,:,i);
    LitpRC=P.LitpRCall(i,:);
    RitpRC=P.RitpRCall(i,:);
    LctrCrp=P.LctrCrp(i,:);
    RctrCrp=P.RctrCrp(i,:);
    Ldvn=P.LdvnCrpAll(:,:,i);
    Rdvn=P.RdvnCrpAll(:,:,i);
    Lrng=fillmissing(P.LrngCrpAll(:,:,i),'linear');
    Rrng=fillmissing(P.RrngCrpAll(:,:,i),'linear');

    [P.Ledg(:,:,i),P.Redg(:,:,i),P.LctrEdg(:,:,i),P.RctrEdg(:,:,i)] = edgeFromRange(LorR,LmonoBG,LitpRC,RitpRC,LctrCrp,RctrCrp,Ldvn,Rdvn,Lrng,Rrng,bPlot);

    if bPlot==1
        drawnow
        waitforbuttonpress
    end

end
