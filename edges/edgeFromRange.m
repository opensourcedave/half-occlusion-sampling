function [exitflag,Ledg,Redg,LctrEdg,RctrEdg,LctrRows,RctrRows,Lsign,Rsign] = edgeFromRange(LorR,LmonoBG,RmonoBG,LitpRC,RitpRC,LctrCrp,RctrCrp,Ldvn,Rdvn,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,bPlot)
exitflag=0;

PszRC=size(LmonoBG);
allRows=1:PszRC(1);
blank=zeros(PszRC);

%Blur range image
sigma=.6;
sz=2*ceil(.2*sigma)+1;
Lrng=imgaussfilt(Lrng,sigma,'FilterDomain','spatial','FilterSize',sz);
Rrng=imgaussfilt(Rrng,sigma,'FilterDomain','spatial','FilterSize',sz);

%Depth edges
%thresh=[.15 .25];
%XXX
thresh=[.10 .20];
sigma =sqrt(.001);
[Ledge,~]=edge(Lrng,'Canny',thresh,sigma);
[Redge,~]=edge(Rrng,'Canny',thresh,sigma);

%FIND DVN DEPTH EDGES NOT ACCOUNTED FOR

%Direction of gradient
[LdirX,~]=imgradientxy(Lrng);
[RdirX,~]=imgradientxy(Rrng);

%Direction specific to each edge
Ledg=blank;
Redg=blank;
Ledg(LdirX>0 & Ledge)= 1;
Ledg(LdirX<0 & Ledge)=-1;
Redg(RdirX>0 & Redge)= 1;
Redg(RdirX<0 & Redge)=-1;

tmp1=[Ledge== 1 zeros(size(Ledg,1),1)];
tmp2=[Ledge==-1 zeros(size(Ledg,1),1)];
Ledg(~(diff(tmp1,[],2)==-1))= 0; %fix last
Ledg( (diff(tmp2,[],2)==-1))=-1; %fix last

tmp1=[zeros(size(Ledg,1),1) Redge== 1];
tmp2=[zeros(size(Ledg,1),1) Redge==-1];
Redg(~(diff(tmp1== 1,[],2)== 1))= 0;  %add first
Redg( (diff(tmp2==-1,[],2)== 1))=-1; %add first

%%%%%%%%%%%%%%%%%%%%%

%GET CENTRAL EDGE
[center,centerSec] = centralEdge(LorR,LmonoBG,RmonoBG,LctrCrp,RctrCrp,LitpRC,RitpRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm);
if isempty(center) || isempty(centerSec)
    LorR%
    LctrEdg=[];
    RctrEdg=[];
    LctrRows=[];
    RctrRows=[];
    Lsign=[];
    Rsign=[];
    exitflag=1;
    return
end
center(center(:,1)>PszRC(1),1)=PszRC(1);
center(center(:,2)>PszRC(2),2)=PszRC(2);
center(center(:,1)<=0,1)=1;
center(center(:,2)<=0,2)=1;
try
    center   =sub2ind(PszRC,center(:,1),center(:,2));
catch
    PszRC
    center
    LctrEdg=[];
    RctrEdg=[];
    LctrRows=[];
    RctrRows=[];
    Lsign=[];
    Rsign=[];
    exitflag=1;
    return
end

%CLEAN UP CENTRAL EDGE
lind=isnan(centerSec(:,1));
lind2=isnan(centerSec(:,2));
if sum(lind) > 0
    sum(lind)
end
if sum(lind2) > 0
    sum(lind2)
end
centerSec=round(centerSec);
centerSec( lind | lind2,:)=[];
centerSec(centerSec(:,1)>PszRC(1),1)=PszRC(1);
centerSec(centerSec(:,2)>PszRC(2),2)=PszRC(2);
centerSec(centerSec(:,1)<=0,1)=1;
centerSec(centerSec(:,2)<=0,2)=1;
try
    centerSec=sub2ind(PszRC,centerSec(:,1),centerSec(:,2));
catch ME
    PszRC
    centerSec
    exitflag=1;
    return
end

lCenter   =blank;
lCenterSec=blank;
lCenter(center)      =1;
lCenterSec(centerSec)=1;

%Label central edges
if LorR=='L'
    Lcenter=lCenter;
    Rcenter=lCenterSec;
elseif LorR=='R'
    Lcenter=lCenterSec;
    Rcenter=lCenter;
end

%Add buffer to central edge
LcenterBuff=logicalSurround(Lcenter,3,2);
RcenterBuff=logicalSurround(Rcenter,3,2);

%Find central edge in range data
LctrEdg=blank;
RctrEdg=blank;
LctrEdg(LdirX>0 & Ledge & LcenterBuff)= 1;
LctrEdg(LdirX<0 & Ledge & LcenterBuff)=-1;
RctrEdg(RdirX>0 & Redge & RcenterBuff)= 1;
RctrEdg(RdirX<0 & Redge & RcenterBuff)=-1;

%REMOVE PARTS NOT PART OF EDGE L
if sum(sum(LctrEdg==1)) < sum(sum(LctrEdg==-1))
    LctrEdg(LctrEdg== 1)=0;
    [n,m]=find(LctrEdg==-1);
    Lsign=1;
else
    LctrEdg(LctrEdg==-1)=0;
    [n,m]=find(LctrEdg==1);
    Lsign=-1;
end

%GET FURTHEST EDGE L
if ~isempty(n)
    rows=sortrows([n,m]);
    [~,ind]=unique(rows(:,1),'last');
    LctrRows=rows(ind,:);
else
    LctrRows=[];
end

%INTERPOLATE MISSING BORDER
if ~isempty(n)
    [LctrRows,LctrEdg]=fixMissingRows(LctrRows,LctrEdg,Lsign,PszRC,allRows,blank);
end

%REMOVE PARTS NOT PART OF EDGE R
if sum(sum(RctrEdg==1)) < sum(sum(RctrEdg==-1))
    RctrEdg(RctrEdg== 1)=0;
    [n,m]=find(RctrEdg==-1);
    Rsign=1;
else
    RctrEdg(RctrEdg==-1)=0;
    [n,m]=find(RctrEdg==1);
    Rsign=-1;
end

%GET FUTHEREST EDGE R
if ~isempty(n)
    rows=sortrows([n,m],'descend');
    [~,ind]=unique(rows(:,1),'last');
    RctrRows=rows(ind,:);
else
    RctrRows=[];
end

%INTERPOLATE MISSING BORDER
if ~isempty(n)
    [RctrRows,RctrEdg]=fixMissingRows(RctrRows,RctrEdg,Rsign,PszRC,allRows,blank);
end


%% PLOT
if bPlot==1
    subplot(2,3,1)
    if LorR=='L'
        imagesc(Ledg)
    elseif LorR=='R'
        imagesc(Redg);
    end
    axis image
    title('Depth Edges')
    colorbar

    subplot(2,3,2)
    if LorR=='L'
        imagesc(Lcenter)
    elseif LorR=='R'
        imagesc(Rcenter)
    end
    axis image
    title('Raw central Edge')
    colorbar

    subplot(2,3,3)
    if P.LorRall(1)=='L'
        imagesc(LctrEdg)
    elseif P.LorRall(1)=='R'
        imagesc(RctrEdg)
    end
    axis image
    title('Processed Central Edge')
    colorbar

end
