function [LedgeMap,RedgeMap,start,startSec] = edgeFromDVN(LorR,LmonoBG,RmonoBG,LctrCrp,RctrCrp,LitpRC,RitpRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm)
%LctrCrp(i,:)
%LitpRCall(i,:)
PszRC=size(LmonoBG);
blank=zeros(PszRC);
Pctr=ceil(PszRC./2);
rowInd=1:PszRC(1);

%GET NUMBER OF SIGNIFICANT DVN REGIONS AND THEIR PIXEL INDECES
CC=bwconncomp(LmonoBG);
if ~isempty(CC.PixelIdxList)
    LpixelIdxList=cellInd2sub(PszRC,CC.PixelIdxList);
    %numels=cellfun(@(x) numel(x), LpixelIdxList); %total number of DVN regions

    %GET EDGES
    Lsorted=cellfun(@(x) sortrows(x), LpixelIdxList,'UniformOutput',false);
    EdgesLsorted=cellfun(@(x) x(find(diff(x(:,1))),:)+[0,1],   Lsorted,'UniformOutput',false);
    EdgesL=vertcat(EdgesLsorted{:});

        %XXX round?
    if ~isempty(EdgesL)
        IctrRC=round(EdgesL-Pctr+LctrCrp);
        ind=1;
        while sum(ind)
            [~,EdgesRsec,~,~,RitpRCchk] = LRSIcorrespondingPointVec([],'L',IctrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm);
            ind=(abs(EdgesRsec(:,2)-RitpRCchk(:,2)).^2>.01);
            IctrRC(ind,2)=IctrRC(ind,2)+1;
        end
        EdgesRsec=EdgesRsec+Pctr-RctrCrp;
    else
        EdgesRsec=zeros(0,2);
    end
else
    EdgesL=zeros(0,2);
    EdgesRsec=zeros(0,2);
end

%% R
%GET NUMBER OF SIGNIFICANT DVN REGIONS AND THEIR PIXEL INDECES
CC=bwconncomp(RmonoBG);
if ~isempty(CC.PixelIdxList)
    RpixelIdxList=cellInd2sub(PszRC,CC.PixelIdxList);
    %numels=cellfun(@(x) numel(x), RpixelIdxList); %total number of DVN regions
                                                    %NR=find(numels>P.nDVNbuffer); %number of significant DVN regions
    %GET EDGES
    Rsorted=cellfun(@(x) sortrows(x),RpixelIdxList,'UniformOutput',false);
    EdgesRsorted=cellfun(@(x) x(find(diff(x(:,1))),:)-[0,1], Rsorted,'UniformOutput',false);
    EdgesR=vertcat(EdgesRsorted{:});

    %XXX round?
    if ~isempty(EdgesR)
        IctrRC=round(EdgesR-Pctr+RctrCrp);
        [EdgesLsec,~,~,~] = LRSIcorrespondingPointVec([],'R',IctrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm);
        EdgesLsec=EdgesLsec+Pctr-LctrCrp;

    else
        EdgesLsec=zeros(0,2);
    end
else
    EdgesR=zeros(0,2);
    EdgesLsec=zeros(0,2);
end

%% GET CENTRAL REGION
%XXX any & multiple ctr
if strcmp(LorR,'L') && ~isempty(EdgesL)

    %sort(LpixelIdxList{NL})-Pctr+LctrCrp;

    ctr=ceil(LitpRC);
    ctr=[ctr; ctr+[0 1]; ctr-[0 1]];
    ind=find(cellfun(@(x) sum(any(ismember(ceil(x-Pctr+LctrCrp), ctr,'rows'))),Lsorted));
    start=EdgesLsorted{ind};

    %GET CENTRAL EDGE IN NON-ANCHOR
    ind=ismember(EdgesL,start,'rows');
    startSec=EdgesRsec(ind,:);
elseif strcmp(LorR,'R') && ~isempty(EdgesR)
    ctr=floor(RitpRC);
    ctr=[ctr; ctr-[0 1]];
    ind=find(cellfun(@(x) sum(any(ismember(floor(x-Pctr+RctrCrp),ctr,'rows'))),Rsorted));
    start=EdgesRsorted{ind};

    %GET CENTRAL EDGE IN NON-ANCHOR
    ind=ismember(EdgesL,start,'rows');
    startSec=EdgesLsec(ind,:);
end


%% REMOVE VALUES BEYOND RANGE
% ???

%% LABEL EDGES
%XXX round?
EdgesL   =round(EdgesL);
EdgesR   =round(EdgesR);
EdgesLsec=round(EdgesLsec);
EdgesRsec=round(EdgesRsec);

ind=EdgesL(:,1)>PszRC(1);
EdgesL(ind,1)=EdgesL(ind,1)-1;
ind=EdgesL(:,2)>PszRC(2);
EdgesL(ind,2)=EdgesL(ind,2)-1;
%
ind=EdgesL(:,1)<=0;
EdgesL(ind,1)=EdgesL(ind,1)+1;
ind=EdgesL(:,2)<=0;
EdgesL(ind,2)=EdgesL(ind,2)+1;
%
ind=EdgesR(:,1)>PszRC(1);
EdgesR(ind,1)=EdgesR(ind,1)-1;
ind=EdgesR(:,2)>PszRC(2);
EdgesR(ind,2)=EdgesR(ind,2)-1;
%
ind=EdgesR(:,1)<=0;
EdgesR(ind,1)=EdgesR(ind,1)+1;
ind=EdgesR(:,2)<=0;
EdgesR(ind,2)=EdgesR(ind,2)+1;

%SECONDARY
%XXX MAYBE I SHOULD REMOVE THESE ELEMENTS (2)
%L
ind=EdgesLsec(:,1)>PszRC(1) | isnan(EdgesLsec(:,1)) | isnan(EdgesLsec(:,2));
EdgesLsec(ind,:)=[];
ind=EdgesLsec(:,2)>PszRC(2);
EdgesLsec(ind,2)=PszRC(2);
%
ind=EdgesLsec(:,1)<=0;
EdgesLsec(ind,:)=[];
ind=EdgesLsec(:,2)<=0;
EdgesLsec(ind,2)=1;
%R
ind=EdgesRsec(:,1)>PszRC(1) | isnan(EdgesRsec(:,1)) | isnan(EdgesRsec(:,2));
EdgesRsec(ind,:)=[];
ind=EdgesRsec(:,2)>PszRC(2);
EdgesRsec(ind,2)=PszRC(2);
%
ind=EdgesRsec(:,1)<=0;
EdgesRsec(ind,:)=[];
ind=EdgesRsec(:,2)<=0;
EdgesRsec(ind,2)=1;


LedgeMap=blank;
RedgeMap=blank;
if ~isempty(EdgesL)
    EdgesLind=sub2ind(PszRC,EdgesL(:,1),EdgesL(:,2));
    LedgeMap(EdgesLind)   =-1;
end
if ~isempty(EdgesLsec)
    EdgesLsecInd=sub2ind(PszRC,EdgesLsec(:,1),EdgesLsec(:,2));
    LedgeMap(EdgesLsecInd)= 1;
end
if ~isempty(EdgesR)
    EdgesRind=sub2ind(PszRC,EdgesR(:,1),EdgesR(:,2));
    RedgeMap(EdgesRind)   = 1;
end
if ~isempty(EdgesRsec)
    EdgesRsecInd=sub2ind(PszRC,EdgesRsec(:,1),EdgesRsec(:,2));
    RedgeMap(EdgesRsecInd)=-1;
end

%COMPLETE CENTRAL EDGE INDICES FOR ANCHOR
ind=rowInd(~ismember(rowInd,start(:,1)))';
if isempty(start)
    start=[ind nan(length(ind),1)];
else
    start=[start; ind nan(length(ind),1)];
end

%COMPLETE CENTRAL EDGE INDICES FOR NON ANCHOR
ind=rowInd(~ismember(rowInd,startSec(:,1)))';
if isempty(startSec) && ~isempty(ind)
    startSec=[ind nan(length(ind),1)];
elseif ~isempty(ind)
    startSec=[startSec; ind nan(length(ind),1)];
end
