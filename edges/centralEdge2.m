function [center,centerSec] = centralEdge(LorR,LmonoBG,RmonoBG,LctrCrp,RctrCrp,LitpRC,RitpRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm)
PszRC=size(LmonoBG);
Pctr=ceil(PszRC./2);

%%L
CC=bwconncomp(LmonoBG);
if ~isempty(CC.PixelIdxList)
    LpixelIdxList=cellInd2sub(PszRC,CC.PixelIdxList);

    %GET EDGES
    Lsorted=cellfun(@(x) sortrows(x), LpixelIdxList,'UniformOutput',false);
    EdgesLsorted=cellfun(@(x) x(find(diff(x(:,1))),:)+[0,1],   Lsorted,'UniformOutput',false);
    EdgesL=vertcat(EdgesLsorted{:});

    if ~isempty(EdgesL)
        IctrRC=round(EdgesL-Pctr+LctrCrp);
        ind=1;
        while sum(ind)
            [~,EdgesRsec,~,~,RitpRCchk] = LRSIcorrespondingPointVec([],'L',IctrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm);
            ind=(abs(EdgesRsec(:,2)-RitpRCchk(:,2)).^2>.01);
            IctrRC(ind,2)=IctrRC(ind,2)+1;
        end
        EdgesRsec=EdgesRsec+Pctr-RctrCrp;
    else
        EdgesRsec=zeros(0,2);
    end
else
    EdgesL=zeros(0,2);
    EdgesRsec=zeros(0,2);
end

%% R
CC=bwconncomp(RmonoBG);
if ~isempty(CC.PixelIdxList)
    RpixelIdxList=cellInd2sub(PszRC,CC.PixelIdxList);

    %GET EDGES
    Rsorted=cellfun(@(x) sortrows(x,'descend'),RpixelIdxList,'UniformOutput',false);
    EdgesRsorted=cellfun(@(x) x(find(diff(x(:,1))),:)-[0,1], Rsorted,'UniformOutput',false);
    EdgesR=vertcat(EdgesRsorted{:});

    if ~isempty(EdgesR)
        IctrRC=round(EdgesR-Pctr+RctrCrp);
        [EdgesLsec,~,~,~] = LRSIcorrespondingPointVec([],'R',IctrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm);
        EdgesLsec=EdgesLsec+Pctr-LctrCrp;

    else
        EdgesLsec=zeros(0,2);
    end
else
    EdgesR=zeros(0,2);
    EdgesLsec=zeros(0,2);
end

%% GET CENTRAL REGION
if strcmp(LorR,'L') && ~isempty(EdgesL)

    ctr=ceil(LitpRC);
    ctr=[ctr; ctr+[0 1]; ctr-[0 1]];
    ind=find(cellfun(@(x) sum(any(ismember(ceil(x-Pctr+LctrCrp), ctr,'rows'))),Lsorted));

    %GET CENTRAL EDGE IN NON-ANCHOR
    try
        center=EdgesLsorted{ind};
        ind=ismember(EdgesR,center,'rows');
        centerSec=EdgesRsec(ind,:);
    catch ME
        centerSec=[];
    end
elseif strcmp(LorR,'R') && ~isempty(EdgesR)

    ctr=floor(RitpRC);
    ctr=[ctr; ctr+[0 1]; ctr-[0 1]];
    ind=find(cellfun(@(x) sum(any(ismember(floor(x-Pctr+RctrCrp),ctr,'rows'))),Rsorted));

    %GET CENTRAL EDGE IN NON-ANCHOR
    try
        center=EdgesRsorted{ind};
        ind=ismember(EdgesL,center,'rows');
        centerSec=EdgesLsec(ind,:);
    catch ME
        centerSec=[];
    end
end