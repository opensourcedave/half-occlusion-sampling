stmXYdeg       = [3*.75 1*.75];
[pCppXm,pCppYm,pCppZm] = psyProjPlaneAnchorEye('C',1,1,P.PszXY,stmXYdeg,'jburge-wheatstone');

for s = 1:length(P.LorRall)
    [depth,dsp,depthB,dspB]=...
    disparityMapDisplay( P.AitpRC(:,:,s),P.ABitpRC(:,:,s),P.BitpRC(:,:,s),P.LorRall(s), D.stmXYdeg,P.PszXY,pCppXm,pCppYm,pCppZm);

    figure(1)

    subplot(5,2,1)
    imagesc(P.LphtCrpAll(:,:,s))
    axis image
    colormap gray

    subplot(5,2,2)
    imagesc(P.RphtCrpAll(:,:,s))
    axis image
    colormap gray

    subplot(5,2,3)
    if strcmp(P.LorRall(s),'L')
        scatter(P.AitpRC(:,2,s),P.AitpRC(:,1,s),'k.')
        title('L')
    elseif strcmp(P.LorRall(s),'R')
        scatter(P.BitpRC(:,2,s),P.BitpRC(:,1,s),'k.')
        title('R')
    end
    axis image
    %xlim([1,PszXY(1)])
    ylim([1,PszXY(2)])
    colormap gray
    set(gca,'YDir','reverse')

    subplot(5,2,4)
    if strcmp(P.LorRall(s),'R')
        scatter(P.ABitpRC(:,2,s),P.ABitpRC(:,1,s),'k.')
    elseif strcmp(P.LorRall(s),'L')
        scatter(P.BitpRC(:,2,s),P.BitpRC(:,1,s),'k.')
    end
    axis image
    %xlim([1,PszXY(1)])
    ylim([1,PszXY(2)])
    set(gca,'YDir','reverse')

    subplot(5,2,5)
    imagesc(depth)
    title('depth')
    axis image
    colormap default
    colorbar
    %caxis([1.1 1.25])

    subplot(5,2,6)
    imagesc(depthB)
    title('depthB')
    axis image
    colormap default
    colorbar
    %caxis([1.1 1.25])

    subplot(5,2,7)
    imagesc(dsp)
    title('dsp')
    axis image
    colormap default
    colorbar

    subplot(5,2,8)
    imagesc(dspB)
    title('dspB')
    axis image
    colormap default
    colorbar

    subplot(5,2,9)
    if strcmp(P.LorRall(s),'L')
        imagesc(P.LdvnCrpAll(:,:,s));
        title('LDVN')
    elseif strcmp(P.LorRall(s),'R')
        imagesc(P.RdvnCrpAll(:,:,s));
        title('RDVN')
    end
    axis image
    colormap default
    colorbar

    subplot(5,2,10)
    if strcmp(P.LorRall(s),'L')
        imagesc(P.LdvnCrpAll(:,:,s));
        title('LDVN')
    elseif strcmp(P.LorRall(s),'R')
        imagesc(P.RdvnCrpAll(:,:,s));
        title('RDVN')
    end
    axis image
    colormap default
    colorbar

    drawnow
    waitforbuttonpress
end
