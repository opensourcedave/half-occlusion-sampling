%LOAD
LOADHOCP

%CHECK CENTER CROP ALIGNMENT OF PATCH
figure(nFn)
for i = 1:length(P.LorRall)
    imgSz=size(P.LphtCrpZerAll(:,:,i));
    imgZer=[P.LphtCrpZerAll(:,:,i), P.RphtCrpZerAll(:,:,i)].^.4;
    img   =[P.LphtCrpAll(:,:,i),    P.RphtCrpAll(:,:,i)].^.4;
    dvn   =[P.LdvnCrpAll(:,:,i),    P.RdvnCrpAll(:,:,i)];
    dvnZer=[P.LdvnCrpAll(:,:,i),    P.RdvnCrpAll(:,:,i)];
    dmap=P.depthImgDisp(:,:,i);

    XY=[ceil(imgSz(2)/2),floor(imgSz(1)/2)];
    XY=[XY; XY(1)+imgSz(2), XY(2)];

    subplot(2,2,1);
    imagesc(imgZer); hold on;
    scatter(XY(:,1),XY(:,2),'.r');
    colormap gray
    title([num2str(i), P.LorRall(i) ' Original'])
    axis image

    subplot(2,2,2);
    imagesc(img); hold on;
    scatter(XY(:,1),XY(:,2),'.r');
    colormap gray
    title(['With added disparity'])
    axis image

    subplot(2,2,3);
    imagesc(dvn); hold on;
    scatter(XY(:,1),XY(:,2),'.r');
    colormap gray
    title([num2str(i), P.LorRall(i) ' Original'])
    axis image

    subplot(2,2,4);
    imagesc(dvn); hold on;
    scatter(XY(:,1),XY(:,2),'.r');
    colormap gray
    title(['With added disparity'])
    axis image

    %subplot(2,2,4)
    %imagesc(dmap); hold on;
    %scatter(XY(1,1),XY(1,2),'.r')
    %axis image

    drawnow
    waitforbuttonpress
end
