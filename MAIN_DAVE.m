% --------------------------------------------------------------------
% README
% %    INSTRUCTIONS:
%        You do NOT need to add anything to your run path.
%        See "NOTE"s throughout code
%        See MAIN and functions in this directory for additional details
%        Functions in the 'functions' directory are dependencies, but are not be required to understand.
%
%    RUNNING
%        1. Download the database and place in this directory (or a symlink to it)
%        2. Change the USER SPECIFIED variables in the main script MAIN %        3. Simply run the main script with the full path specified
%           eg:
%                /home/dambam/Code/Dave/collab/MAIN
%
% --------------------------------------------------------------------
% THIS SCRIPT USES 2 MAIN FUNCTIONS
%   daVinciZoneSample  - sample and vet potential patch centers
%   daVinciZonePatches - create patch centers from vetted patch centers
% WHICH ARE PROCESSED USING 2 BATCH PROCESSING SCRIPTS
%   daVinciZoneSamplePatchBatch      - create patches at a given disparity and daVinci zone width
%   daVinciZoneSamplePatchBatchBatch - create all patches for all specified disparities and widths
% --------------------------------------------------------------------
%% USER SPECIFIED VARIABLES
%  SEE daVinciZoneSamplePatchBatchBatch for details on variables

clearvars -except Pnew Porig nDVN BAD
close all

profile clear
profile on

% NOTE THIS LINE IS THE ONLY ONE THAT DEVIATES FROM OTHERS
fileParams_DVN_NAT_NAT_Adj_AMA;
assignParams;

%%%%%%%%%%%%%%%%
% CREATE PATCHES
if bCreate>0

    % HANDLE DIRECTORY CHANGING AND DATABASE DIRECTORIES
    mfname=mfilename('fullpath');
    [oldPath,oldDir]=handlePath(mfname,H.rootDTBdir,bIsolate);

    % RUN
    if H.bDebug
        P = daVinciZoneSamplePatchBatchBatch(H)
    else
        try
            P = daVinciZoneSamplePatchBatchBatch(H)
        catch ME
        if ~isempty(oldDir) && isempty(oldPath)
            cd(oldDir);
            path(oldPath);
        end
        rethrow(ME);
        end
    end
    complete=1;

    % REMOVE RESTORE OLD PATH AND DIRECTORY
    if ~isempty(oldDir) && isempty(oldPath)
        cd(oldDir);
        path(oldPath);
    end

    if bSummary==1
        P = dspDistribution(P,D);
        plotRMSandDC(P);
    end
end

%%%%%%%%%%%%%%%%
% EXPERIMENT
if bCreate~=1

    %fileParams_HOC_NAT_NAT2D_est %DO NOT EDIT FOR THIS FILE
    if exist('Pnew','var')
        P=Pnew;
    elseif ~exist('complete','var')
      fname=daVinciPatchFnameHash(H)
      P=load(fname);
    end

    if bSummary==1
        [P,D] = dspDistribution(P,D);
        plotRMSandDC(P);
    end

    indTrl=1:size(P.LphtCrpAll,3);

    %% RUN EXPERIMENT
    try
        [R,P,D,exitflag] = ExpHOCadjust(D,C,P,G,indTrl,0,bTst);
        %[sfname,lfname]=depthProbeFname(D,P);
    catch ME
        assignin('base','R',P);
        assignin('base','P',P);
        assignin('base','D',D);
        scaa;
        rethrow(ME)
    end
end
