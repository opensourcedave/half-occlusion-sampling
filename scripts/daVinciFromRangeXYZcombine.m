bSave=1;
IPDm=LRSIcameraIPD();
DGcutoffs=2;
bPLOT=0;
rootDTBdir=[];
nDVNbuffer=200;

dvnFnameBase='/volume1/Data/Project_Databases/LRSI/Image/DVN2/DVN_';
xyzFnameBase='/volume1/Data/Project_Databases/LRSI/Image/XYZ2/XYZ_';
imgRange=1:98;
%imgRange=21;

for i = imgRange
    [Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz]  = loadLRSIimage(i,1,0,'PHT','img',rootDTBdir);
    if i==1
        blank=zeros(size(Lxyz));
    end

    [Lxyz,Ldvn]=processEye(Lxyz,Lpht,'L',nDVNbuffer,blank,IPDm,DGcutoffs,bPLOT);
    [Rxyz,Rdvn]=processEye(Rxyz,Rpht,'R',nDVNbuffer,blank,IPDm,DGcutoffs,bPLOT);

    progressreport(i,1,length(imgRange));
    dig=sprintf('%03d',i);
    fnameDvnL=[dvnFnameBase 'L_' dig];
    fnameDvnR=[dvnFnameBase 'R_' dig];
    fnameXyzL=[xyzFnameBase 'L_' dig];
    fnameXyzR=[xyzFnameBase 'R_' dig];
    if bSave==1;
        save(fnameDvnL,'Ldvn');
        save(fnameDvnR,'Rdvn');
        save(fnameXyzL,'Lxyz');
        save(fnameXyzR,'Rxyz');
    else
        disp(fnameDvnL);
        disp(fnameDvnR);
        disp(fnameXyzL);
        disp(fnameXyzR);
    end
end

function [Ixyz,Idvn] = processEye(Ixyz,Ipht,LorR,nDVNbuffer,blank,IPDm,DGcutoffs,bPLOT)
    %FIND OBJECTS LESS THAN BUFFER
    CC=bwconncomp(isnan(Ixyz));
    numels=cellfun(@(x) numel(x), CC.PixelIdxList);
    N=find(numels<nDVNbuffer);

    %COMBINE OBJECTS INTO SINGLE LOGICAL INDEX
    idxAll=vertcat(CC.PixelIdxList{N});
    blank(idxAll)=1;
    lind=logical(blank);


    %SET SIGNIFICANT NAN ZONES TO ZERO
    largeNans=(isnan(Ixyz) & ~lind);
    Ixyz(largeNans)=0;

    %FILL IN NANs BESIDES THOSE SIGNIFICANT
    Ixyz=fillmissing(Ixyz,'linear');

    %SET SIGNIFICANT NAN ZONES BACK TO NAN
    Ixyz(largeNans)=nan;

    %GET DVN REGIONS FROM VARIOUS METHODS
    %[Idg1,Igd1,Ibnd1,Idvn1] =    daVinciFromRangeXYZ(LorR,IPDm,Ixyz,DGcutoffs,0);
    [Idvn2,Idg2,Igd2,Ibnd2] = daVinciFromRangeXYZnew(LorR,IPDm,Ixyz,DGcutoffs,0);
    %Idvn=Idvn1 & Idvn2;
    Idvn=Idvn2;

    if bPLOT==1 && LorR=='L'
        figure(1)
        imagesc([Ipht.^.4,Ipht.^.4])
        colormap gray
        hold on
        [n1,m1]=find(Idvn1);
        [n2,m2]=find(Idvn2);
        [n3,m3]=find(Idvn);
        m2=m2+1920;
        plot(m1,n1,'r.')
        plot(m2,n2,'b.')
        plot(m3,n3,'k.')
        plot(m3+1920,n3,'k.')
        axis image
        hold off

        drawnow
    end


    %COMBINE
end