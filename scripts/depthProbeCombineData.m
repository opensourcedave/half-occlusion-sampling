dir='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/';
ldir='/Users/Shared/Data/Project_Psydata/HOC/NAT/NAT/PILOT/';

fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_1-1.mat';
ffname=[dir fname];
load(ffname);
S1=S;
ind1=S1.viewed;

fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_1-2.mat';
ffname=[dir fname];
load(ffname);
S2=S;
clear S
ind2=logical(S2.viewed);

ind1 = (ind1 & ~ind2);

SS=S2;
SS.Xhat=         [S1.Xhat(ind1); S2.Xhat(ind2)];
SS.dspArcMinTrue=[S1.dspArcMinTrue(ind1); S2.dspArcMinTrue(ind2)];
SS.flag=         [S1.flag(ind1); S2.flag(ind2)];
SS.X=            [S1.X(ind1); S2.X(ind2)];
SS.viewed=       [S1.viewed(ind1); S2.viewed(ind2)];
SS.XhatLocXYpix=       [S1.XhatLocXYpix(ind1,:); S2.XhatLocXYpix(ind2,:)];

[~,indSort]=sort(SS.X);
SS.Xsort=SS.X(indSort);
SS.XhatSort=SS.Xhat(indSort);
SS.XhatNrm = (SS.Xhat + 3)./(SS.X + 3);
%%%%

fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_2.mat';
ffname=[dir fname];
load(ffname);
S3=S;
clear S

SS.P=S3.P;
SS.D=S3.D;

[SS.Xstar] = depthProbeGeometricBatch(S.P,S.D,S.indTrl);
SS.XstarSort=SS.Xstar(indSort);
depthProbeAdjustPlotResults(SS);

fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_1.mat';
ffname=[dir fname];
lffname=[ldir fname];

save(ffname,'SS','-v7.3');
save(lffname,'SS','-v7.3');

%%%%%%%%%%%%%%%%%%%%%%%%%%
dir='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/';
ldir='/Users/Shared/Data/Project_Psydata/HOC/NAT/NAT/PILOT/';
fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_2.mat';
ffname=[dir fname];
lffname=[ldir fname];
load(lffname);
S2=S;

dir='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/';
ldir='/Users/Shared/Data/Project_Psydata/HOC/NAT/NAT/PILOT/';
fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_1.mat';
ffname=[dir fname];
lffname=[ldir fname];
load(ffname);
S1=S;
plot(S1.Xhat-S2.Xhat)

dir='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/';
ldir='/Users/Shared/Data/Project_Psydata/HOC/NAT/NAT/PILOT/';
fname='HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_JDB_PILOT_1.mat';
ffname=[dir fname];
lffname=[ldir fname];
load(ffname);

%%

[~,indSort]=sort(S.X);
S.Xsort=S.X(indSort);
S.XhatSort=S.Xhat(indSort);
S.XhatNrm = (S.Xhat + 3)./(S.X + 3);

[S.Xstar] = depthProbeGeometricBatch(S.P,S.D,S.indTrl);
S.XstarSort=S.Xstar(indSort);
depthProbeAdjustPlotResults(S);