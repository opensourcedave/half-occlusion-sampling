%loadHOC;
fields=fieldnames(P);
n=size(P.LorRall,1);
%bad=P.bad;
bad=(P.gdInd==-1);
for i = 1:length(fields)
    if ~any(size(P.(fields{i}))==n)
        continue
    end
    dim=find(size(P.(fields{i}))==n);
    try
        switch dim
        case 1
            P.(fields{i})(bad,:,:,:)=[];
        case 2
            P.(fields{i})(:,bad,:,:)=[];
        case 3
            P.(fields{i})(:,:,bad,:)=[];
        case 4
            P.(fields{i})(:,:,:,bad)=[];
        end
    catch
        continue
    end
end
