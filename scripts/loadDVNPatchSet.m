D.bPrbB=0;
D.prbLocType='m3';
D.stmMskMode=1;
D.prbHideOnMove=0;
fileParams_DVN_NAT_NAT_Adj %DO NOT EDIT FOR THIS FILE

dspArcMin=dspArcMinAll(dspArcMinInd,:);
minMaxPixDVN=minMaxPixDVNall(minMaxPixDVNind,:);
fname=daVinciPatchFnameHash(saveDTB, imgNum, LorRorB, fgndORbgnd, dspArcMinAll, PszXY, rndSd, nSmpPerImg, ctrORedg, minMaxPixDVNall, zoneTestType, zoneBufferRCall, zoneMinDensity, DC, RMS, Wk, D.hostName, bDisplayParams, D.stmXYdeg, bPreDispDsp, overlapPix, b2ctr, nDVN, nDVNbuffer, minZoneSep);
fname=[fname '_RAW']
%fname=[fname]

if ~exist('Porig','var')
    Porig=load(fname);
end
P=Porig;
