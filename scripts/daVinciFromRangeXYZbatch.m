%% SAVE DA VINCI (HALF-OCCLUSION ZONES)
imgNums = 0:98;

for i = imgNums,
    % LOAD IMAGES
    [Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(i,1,1,'PHT','img','','XYZ2');
    % BUILD FILE NAMES
    fnameL = buildFilenameLRSIimageDVN(i,'L');
    fnameR = buildFilenameLRSIimageDVN(i,'R');
    % COMPUTE DA VINCI IMAGES
    IPDm = LRSIcameraIPD;
    % DISPARITY GRADIENT CUTOFF (2.0 -> half-occlusion zones)
    DGcutoff = 2;
    % COMPUTE DA VINCI ZONES
    [Ldvn]= daVinciFromRangeXYZnew('L',IPDm,Lxyz,DGcutoff);
    [Rdvn]= daVinciFromRangeXYZnew('R',IPDm,Rxyz,DGcutoff);
    % SAVE DATA
    saveLRSIimagePRJ(fnameL,'DVN','server',1,Ldvn,'Ldvn');
    saveLRSIimagePRJ(fnameR,'DVN','server',1,Rdvn,'Rdvn');
end