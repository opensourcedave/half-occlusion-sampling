bPlot=0;
bSave=1;
maxZ=50;
for j=1:98
    %I = loadLRSIimagePacked(j,1,0,'PHT','img',[],'XYZ2');
    I = loadLRSIimagePacked(j,1,0,'PHT','img');
    [I.Ldvn,I.Rdvn] = loadLRSIimageDVN(j,'local',0,[],'DVN2');
    for l = 1:2

        if l==1
            LorR='L';
            Idvn=I.Ldvn;
        elseif l==2
            LorR='R';
            Idvn=I.Rdvn;
        end

        %MFedge
        [FgndCtrRC,FgndCtrInd] = daVinciZoneCenterOrEdge(Idvn,[1 inf],'fgnd',LorR,0);
        Fxyz=[I.Cx(FgndCtrInd) I.Cy(FgndCtrInd) I.Cz(FgndCtrInd)];

        %MBedge xyz Expected
        [~,~,~,LitpRCchk,RitpRCchk] = LRSIcorrespondingPointVec([],LorR,FgndCtrRC,I.Lxyz,I.Rxyz,I.LppXm,I.LppYm,I.RppXm,I.RppYm,0,[],I.ML,I.MR);
        BxyzExp = LRSIcorrespondingPoint2sceneXYZ(LitpRCchk,RitpRCchk,I.CppXm,I.CppYm,I.CppZm,I.IPDm);

        %MFedge xzy Observed
        [IdvnCtrRC,IdvnCtrInd,widthAll] = daVinciZoneCenterOrEdge(Idvn,[1 inf],'edg',LorR,0);
        BxyzObs=[I.Cx(IdvnCtrInd) I.Cy(IdvnCtrInd) I.Cz(IdvnCtrInd)];

        if LorR=='L'
            Aexp=intersectLinesFromPointsPlane(I.LxyzEye,BxyzExp,3,[],maxZ);
            Aobs=intersectLinesFromPointsPlane(I.LxyzEye,BxyzObs,3,[],maxZ);
            B=intersectLinesFromPointsPlane(I.LxyzEye,Fxyz,3,[],maxZ);
        elseif LorR=='R'
            Aexp=intersectLinesFromPointsPlane(I.RxyzEye,BxyzExp,3,[],maxZ);
            Aobs=intersectLinesFromPointsPlane(I.RxyzEye,BxyzObs,3,[],maxZ);
            B=intersectLinesFromPointsPlane(I.RxyzEye,Fxyz,3,[],maxZ);
        end
        Aexp=Aexp(:,1);
        Aobs=Aobs(:,1);
        B   =   B(:,1);

        %How much to shorten dvn region
        ObsDist=abs(B-Aobs);
        ExpDist=abs(B-Aexp);
        mod=ObsDist-ExpDist;
        mod=ceil(abs(mod./I.mPerPix));

        %Get indeces of new dvn region and apply
        IdvnNew=zeros(size(Idvn));
        if LorR=='L'
            s=IdvnCtrRC(:,2)+mod;
            e=IdvnCtrRC(:,2)+widthAll-1;
        elseif LorR=='R'
            s=IdvnCtrRC(:,2)-widthAll+1;
            e=IdvnCtrRC(:,2)-mod;
        end

        SubsAll=arrayfun(@(x,y,z) [repmat(z,numel(x:y),1) (x:y)'],s,e,IdvnCtrRC(:,1),'UniformOutput',false);
        SubsAll=vertcat(SubsAll{:});

        if LorR=='L'
            %SubsAll(:,1)=ceil(SubsAll(:,1));
            SubsAll(:,2)=ceil(SubsAll(:,2));
        elseif LorR=='R'
            %SubsAll(:,1)=floor(SubsAll(:,1));
            SubsAll(:,2)=floor(SubsAll(:,2));
        end

        badInd=any(isnan(SubsAll),2) | any(SubsAll<=0,2) | SubsAll(:,1)>size(Idvn,1) | SubsAll(:,2)>size(Idvn,2);
        SubsAll(badInd,:)=[];
        IndAll=sub2ind(size(IdvnNew),SubsAll(:,1),SubsAll(:,2));

        IdvnNew(IndAll)=1;

        if bPlot
            hold off
            figure(1)
            [n,m]=find(Idvn);
            plot(m,n,'b.'); hold on
            plot(SubsAll(:,2),SubsAll(:,1),'r.')
            set(gca,'Ydir','reverse')
            title(LorR)
            drawnow
            waitforbuttonpress
        end
        if bSave
            fname=['DVN_' LorR '_' sprintf('%03i',j)];
            dirn='/Volumes/Data/Project_Databases/LRSI/Image/DVN3/';
            ffull=[dirn fname];
            if LorR=='L'
                Ldvn=IdvnNew;
                save(ffull,'Ldvn');
            elseif LorR=='R'
                Rdvn=IdvnNew;
                save(ffull,'Rdvn');
            end
        end
    end
end
