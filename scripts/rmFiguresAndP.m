subjName='JDB';
en=  [1 1 1]; %experiment number
sen= [1 1 2]; %subexperiment number
pass=[1 2 1]; %pass number

%subjName='DNW';
%en=  [1 1 1 1, 2 2 2]; %experiment number
%sen= [1 1 2 3, 1 2 3]; %subexperiment number
%pass=[1 2 1 1, 1 1 1]; %pass number

%%%%%%%%%%%
%SAVE P & F
%[Sall,SUBJALL,PASSALL,ENALL,SENALL] = depthProbeLoadData(subjName,pass(2),en(2),sen(2));
%P=Sall{1}.P;
%D=Sall{1}.D;
%
%F=struct();
%F.fig1=P.fig1;
%F.fig2=P.fig3;
%F.fig4=P.fig4;
%F.figInd=P.figInd;
%
%P=rmfield(P,'fig1')
%P=rmfield(P,'fig3')
%P=rmfield(P,'fig4')
%P=rmfield(P,'Xxyz')
%fileParams_HOC_NAT_NAT2D_est %DO NOT EDIT FOR THIS FILE
%fnameP=daVinciPatchFname(saveDTB,dspArcMin,minMaxPixDVN,fgndORbgnd,ctrORedg,PszXY,rndSd);
%
%fnameF=[fnameP '_FIGURES'];
%save([fnameP '2'],'P');
%save(fnameF,'F','-v7.3');

%%%%%%%%%
%REMOVE P FROM S
for i = 1:length(en)
   try
     [Sall,SUBJALL,PASSALL,ENALL,SENALL] = depthProbeLoadData(subjName,pass(i),en(i),sen(i));
     S=Sall{1};
     S.Xxyz=S.P.Xxyz(S.indTrl,:);
     S=rmfield(S,'P')
     [sfname,lfname]=depthProbeFname(S.D,P,S,PASSALL(1),SUBJALL{1});
     save(sfname,'S','-v7.3')
   catch
       i
   end

end