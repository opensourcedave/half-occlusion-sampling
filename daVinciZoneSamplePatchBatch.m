function [P,figAll1,figAll2,figAll3] = daVinciZoneSamplePatchBatch(H,bWarn)
% function [P] = daVinciSamplePatch(varargin)
%
%   example call:
%      P = daVinciSamplePatchBatch();
%      P = daVinciSamplePatchBatch(H);
%
% Handle batch processing of batch patch creation
%    daVinciZoneSamplePatchBatch      - handle patch grabbing for given disparity and dvn width
%    daVinciZoneSamplePatchBatchBatch - Loops over all specified disparities and widths
% --------------------------------------------------------------------
% INPUT:
%
% H - struct: SEE daVinciZoneParamParse FOR DETAILS ON H FIELDS (optional)
% --------------------------------------------------------------------
% OUTPUT:
%
% P               - struct containing the following:
%   imgNumAll       - index of all images used for all other outputs  [nSmp x 1]
%   LorRAll         - index of all anchors used for all other outputs [nSmp x 1]
%   LctrCrp         - XXX [nSmp x 2]
%   RctrCrp         - XXX [nSmp x 2]
%   LitpRCchkDspAll - Left image patch center after adding disparity.
%                              Also the 'check' version of LitpRCall, meaning it uses the opposite eye to
%                              find the corresponding point, and thus is centered on the occluding
%                              surface.
%                              [nSmp x 2]
%
%   RitpRCchkDspAll - Right image patch center after adding disparity.
%                              Also the 'check' version of RitpRCall, meaning it uses the opposite eye to
%                              find the corresponding point, and thus is centered on the occluding
%                              surface.
%   CitpRCchkDspAll - XXX
%   LitpRCchkAll    - Left image patch center before adding disparity
%   RitpRCchkAll    - Right image patch center before adding diparity
%   LitpRCall       - Left image patch center of occluded region defined by daVinciZoneCenter
%                     if using left anchor, or found using LRSIcorrespondingPointVec if using
%                              right anchor
%   RitpRCall       - Right image patch center of occluded region defined by daVinciZoneCenter
%                     if using right anchor, or found using LRSIcorrespondingPointVec if using left anchor
%   LitpRCdspAll    - Right image patch center after adding disparity.
%   CitpRCdspAll    - XXX
%   RitpRCdspAll    - Left image patch center after adding disparity.
%   LphtCrpAll      - cropped luminance images centered on LitpRCchkDspAll
%                     [PszXY(2) x PszXY(1) x nSmp]
%   LrngCrpAll      - cropped range images centered on LitpRCchkDspAll
%                     [PszXY(2) x PszXY(1) x nSmp]
%   LxyzCrpAll      - cropped cartesian range images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x 3 x nSmp]
%   LdvnCrpAll      - cropped daVinci images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   LppXmCrpAll     - cropped left eye projection plane pixel locations in X
%                              [PszXY(2) x PszXY(1) x nSmp]
%   LppYmCrpAll     - cropped left eye projection plane pixel locations in Y
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RphtCrpAll      - cropped luminance images centered on LitpRCchkDspA
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RrngCrpAll      - cropped range images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RxyzCrpAll      - cropped cartesian range images centered on LitpRCc
%                              [PszXY(2) x PszXY(1) x 3 x nSmp]
%   RdvnCrpAll      - cropped daVinci images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RppXmCrpAll     - cropped right eye projection plane pixel locations in X
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RppYmCrpAll     - cropped right eye projection plane pixel locations in Y
%                              [PszXY(2) x PszXY(1) x nSmp]
%   fig1            - XXX
%   fig2            - XXX
%   fig3            - XXX
%     stmXYdeg       - size of
% --------------------------------------------------------------------
% HANDLE INPUT
H=daVinciZoneParamParse(H,0);
unpackOpts(H,1);
if ~isvar('bWarn')
    bWarn=1;
end

%INIT
indLookup = reshape(1:(IszRC(1)*IszRC(2)),IszRC);

%CREATE 'LandR' VARIABLE
if strcmp(LorRorB,'B')
    LandR={'L','R'};
	P.LorRall=repmat(['L';'R'], nSmpPerImg*length(imgNum),1);
else
	P.LorRall=repmat(LorRorB, 2*nSmpPerImg*length(imgNum),1);
    LandR=LorRorB;
end

%DEFINE 'nSmpPerImg' IF USING 'nSmp'
if exist('nSmpPerImg','var')~=1 || isempty(nSmpPerImg)
    nSmpPerImg=ceil(nSmp/length(imgNum)*2);  %maxmimum number of samples per image
end
if nSmpPerImg > 300
    nSmpPerImg=300;
    disp('Warning: changing nSmpPerImg to 300!')
end

P.imgNumAll=repelem(imgNum',nSmpPerImg*2,1);
P.maxSmp=zeros(length(imgNum),length(LandR));

%PREALLOCATION
P.LitpRCchkDspAll = zeros(2*length(imgNum)*nSmpPerImg,2);
P.RitpRCchkDspAll = zeros(2*length(imgNum)*nSmpPerImg,2);
P.CitpRCchkDspAll = zeros(2*length(imgNum)*nSmpPerImg,2);
P.RitpRCchkAll    = zeros(2*length(imgNum)*nSmpPerImg,2);
P.LitpRCchkAll    = zeros(2*length(imgNum)*nSmpPerImg,2);
P.LitpRCall       = zeros(2*length(imgNum)*nSmpPerImg,2);
P.RitpRCall       = zeros(2*length(imgNum)*nSmpPerImg,2);
P.LitpRCdspAll    = zeros(2*length(imgNum)*nSmpPerImg,2);
P.RitpRCdspAll    = zeros(2*length(imgNum)*nSmpPerImg,2);
P.CitpRCdspAll    = zeros(2*length(imgNum)*nSmpPerImg,2);
P.RctrCrp         = zeros(2*length(imgNum)*nSmpPerImg,2);
P.LctrCrp         = zeros(2*length(imgNum)*nSmpPerImg,2);

P.LphtCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LphtCrpZerAll   = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LrngCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LdvnCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LppXmCrpAll     = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LppYmCrpAll     = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.LxyzCrpAll      = zeros(PszXY(2), PszXY(1),3,2*length(imgNum)*nSmpPerImg);

P.RphtCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RphtCrpZerAll   = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RrngCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RdvnCrpAll      = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RppXmCrpAll     = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RppYmCrpAll     = zeros(PszXY(2), PszXY(1),  2*length(imgNum)*nSmpPerImg);
P.RxyzCrpAll      = zeros(PszXY(2), PszXY(1),3,2*length(imgNum)*nSmpPerImg);
figAll1           = cell(length(imgNum),2);
figAll2           = cell(length(imgNum),2);
figAll3           = cell(length(imgNum),2);
figInd            = zeros(length(imgNum),2);

plotFlag=bPlot;
OptsSample=packOpts(PszXY,ctrORedg,minMaxPixDVN,bTwoWay,nDVNbuffer,nDVN,minZoneSep,zoneTestType,zoneBufferRC,zoneMinDensity,rndSd,plotFlag,indLookup,rootDTBdir,overlapPix);
% --------------------------------------------------------------------
%GET VALID PATCH CENTERS

for i = 1:length(imgNum) % LOOP OVER IMAGE
    %LOAD PHOTO IMAGES, RARNGE IMAGES,
    try
        I = loadLRSIimagePacked(imgNum(i),1,0,'PHT','img',rootDTBdir,'XYZ2');
    catch
        if bWarn
            warning('daVinciZoneSamplePatchBatch: Reverting to old range data files');
        end
        I = loadLRSIimagePacked(imgNum(i),1,0,'PHT','img',rootDTBdir,[]);
    end
    bWarn=0;

    %GET 'daVinci' IMAGE
    [I.Ldvn,I.Rdvn] = loadLRSIimageDVN(imgNum(i),'local',0,rootDTBdir,'DVN3');
    bfsave=0;


	for k  = 1:length(LandR) % LOOP OVER ANCHOR EYE
        %HANDLE PLOTTING
		if bPlot==1 && any(plotImgNum==imgNum(i))
			if     strcmp(LorRorB,'B') && k==1
                %PLOTTING L of BOTH LorR
                plotFlag= 2;
			elseif strcmp(LorRorB,'B') && k==2
                %PLOTTING R of BOTH LorR
                plotFlag= 3;
			else
                %PLOTTING JUST 1 LorR
                plotFlag= 1;
			end
		else
			plotFlag=0;									 	 %DONT PLOT
		end

        %Handle pre-selected patch center
        if isvar(PctrIndPriAll)
            OptsSample.PctrIndPri=PctrIndPriAll{i,k};
        end

		% MAIN 1 - GET VETTED SAMPLE CENTERS
		[IctrRC,~,fig1] = daVinciZoneSample([],I,LandR{k},OptsSample);

		% MAIN 2 - GET PATCHES FROM VETTED CENTERES
		[Ptmp,~,fig2,fig3] = daVinciZonePatches([],I,LandR{k},IctrRC,...
                                PszXY,dspArcMin,fgndORbgnd,nSmpPerImg,plotFlag,rootDTBdir);

        % Pack Ptmp into P
        P = sampleStructPack(i,k,LandR,P,Ptmp,LorRorB,nSmpPerImg,IctrRC,imgNum);

        % SAVE FIGURES XXX
        if isvar('fig3')
            saveas(fig1,['tmp1.png']);
            saveas(fig2,['tmp2.png']);
            saveas(fig3,['tmp3.png']);
            figAll1{i,k}=imread('tmp1.png');
            figAll2{i,k}=imread('tmp2.png');
            figAll3{i,k}=imread('tmp3.png');
        end

    end
    progressreport(i,5,length(imgNum));
end

% --------------------------------------------------------------------
%% FIND INVALID INDECES (ANY SUBSCRIPT/INDEX EQUAL TO 1)
validAll=squeeze( any(P.LitpRCchkDspAll,2)  &  any(P.RitpRCchkDspAll,2) );

%ASSESS NUMBER OF SAMPLES ACCORDING TO nSmp IF SET
if exist('nSmp','var')==1 && ~isempty(nSmp)
    smpExtra=sum(validAll)-nSmp;
    if smpExtra<0
        %WARN IF INSUFFICIENT SAMPLES
        disp(['Warning: with paramaters provided, you are short ' num2str(abs(smpExtra)) ' samples!']);
        disp('    Increase sample buffer or sampling provide more leaniest sampleing conditions');
    else
        %REMOVE EXTRA SAMPLES RANDOMLY
        ind=find(validAll);
        rmInd=datasample(ind,smpExtra,'Replace',false);
        validAll(rmInd)=0;
    end
end

%REMOVE INVALID PATCHES AND INDECES
P.imgNumAll             = P.imgNumAll(validAll);
P.LorRall               = P.LorRall(validAll);
P.LctrCrp               = P.LctrCrp(validAll,:);
P.RctrCrp               = P.RctrCrp(validAll,:);
P.LitpRCchkDspAll       = P.LitpRCchkDspAll(validAll,:);
P.RitpRCchkDspAll       = P.RitpRCchkDspAll(validAll,:);
P.CitpRCchkDspAll       = P.CitpRCchkDspAll(validAll,:);
P.LitpRCdspAll          = P.LitpRCdspAll(validAll,:);
P.RitpRCdspAll          = P.RitpRCdspAll(validAll,:);
P.CitpRCdspAll          = P.CitpRCdspAll(validAll,:);
P.LitpRCchkAll          = P.LitpRCchkAll(validAll,:);
P.RitpRCchkAll          = P.RitpRCchkAll(validAll,:);
P.LitpRCall             = P.LitpRCall(validAll,:);
P.RitpRCall             = P.RitpRCall(validAll,:);
P.LphtCrpAll            = P.LphtCrpAll(:,:,validAll);
P.LphtCrpZerAll         = P.LphtCrpZerAll(:,:,validAll);
P.RphtCrpAll            = P.RphtCrpAll(:,:,validAll);
P.RphtCrpZerAll         = P.RphtCrpZerAll(:,:,validAll);
P.LrngCrpAll            = P.LrngCrpAll(:,:,validAll);
P.RrngCrpAll            = P.RrngCrpAll(:,:,validAll);
P.LdvnCrpAll            = P.LdvnCrpAll(:,:,validAll);
P.RdvnCrpAll            = P.RdvnCrpAll(:,:,validAll);
P.LxyzCrpAll            = P.LxyzCrpAll(:,:,:,validAll);
P.RxyzCrpAll            = P.RxyzCrpAll(:,:,:,validAll);
P.RppXmCrpAll           = P.RppXmCrpAll(:,:,validAll);
P.RppYmCrpAll           = P.RppYmCrpAll(:,:,validAll);
P.LppXmCrpAll           = P.LppXmCrpAll(:,:,validAll);
P.LppYmCrpAll           = P.LppYmCrpAll(:,:,validAll);
% --------------------------------------------------------------------

%ASSIGN INFORMATIONAL VARIABLES TO P
P.dspArcMinAll=dspArcMin;
P.minMaxPixDVN=minMaxPixDVN;
P.ctrORedg=ctrORedg;
P.PszXY=PszXY;
P.rndSd=rndSd;
P.fgndORbgnd=fgndORbgnd;
P.bPreDispDsp=bPreDispDsp;
P.nDVNbuffer=nDVNbuffer;

% --------------------------------------------------------------------
% Extra Parameters

if bDisplayParams == 1
    P = patches2FixedContrastIntensityImages([],P,DC,RMS,W,bByImage,monoORbino); %CREATE FIXED CONTRAST INTENSITY IMAGES FOR PSYCHOPYSHCIS EXPERIMNETS
    P = halfOcclusionPatchCPsBatch(P,PszXY,dspArcMin,stmXYdeg,fgndORbgnd,b2ctr,bPreDispDsp,hostName,rootDTBdir,0); %CREATE CP MAPS AND OPTIONALLY DISPARITY & DEPTH MAPS
    P = daVinciDivideZones2(P,0); %LABEL ZONES (BINOFG ETC)
    %P = patches2stats(P);         %COMPUTE STATS FOR INDIVIDUAL ZONES
    P = daVinciPatchWidths(P);    %COMPUTE PATCH WIDTHS
end

P = daVinciPostVet(P);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTIONS
end %#END_MAIN
function P = sampleStructPack(i,k,LandR,P,Ptmp,LorRorB,nSmpPerImg,IctrRC,imgNum)
    % COUNT NUMBER OF SAMPLES
    P.maxSmp(i,k)=size(IctrRC,1);
    if k==1
        P.figInd(i)=imgNum(i);
    end

    % SORT STRUCT BASED UPON ANCHOR EYE
    if strcmp(LorRorB,'B')
        mL=(i-1).*nSmpPerImg*2+(1:nSmpPerImg)*2-1; %odd positions
        mR=(i-1).*nSmpPerImg*2+(1:nSmpPerImg)*2;
    elseif strcmp(LorRorB,'R') || strcmp(LorRorB,'L')
        mL=(i-1).*nSmpPerImg*2+(1:nSmpPerImg);
        mR=(i-1).*nSmpPerImg*2+(1:nSmpPerImg);
    end
    if LandR{k}=='L'
        %non-occluded half from left anchor
        P.LctrCrp(mL,:)         = Ptmp.LctrCrp;
        P.LitpRCchkAll(mL,:)    = Ptmp.LitpRCchk;
        P.LitpRCchkDspAll(mL,:) = Ptmp.LitpRCchkDsp;
        P.LitpRCall(mL,:)       = Ptmp.LitpRC;
        P.LitpRCdspAll(mL,:)    = Ptmp.LitpRCdsp;
        P.LphtCrpAll(:,:,mL)    = Ptmp.LphtCrp;
        P.LphtCrpZerAll(:,:,mL) = Ptmp.LphtCrpZer;
        P.LrngCrpAll(:,:,mL)    = Ptmp.LrngCrp;
        P.LxyzCrpAll(:,:,:,mL)  = Ptmp.LxyzCrp;
        P.LdvnCrpAll(:,:,mL)    = Ptmp.LdvnCrp;
        P.LppXmCrpAll(:,:,mL)   = Ptmp.LppXmCrp;
        P.LppYmCrpAll(:,:,mL)   = Ptmp.LppYmCrp;
        %occluded half from left anchor
        P.RctrCrp(mL,:)         = Ptmp.RctrCrp;
        P.RitpRCchkAll(mL,:)    = Ptmp.RitpRCchk;
        P.RitpRCchkDspAll(mL,:) = Ptmp.RitpRCchkDsp;
        P.RitpRCall(mL,:)       = Ptmp.RitpRC;
        P.RitpRCdspAll(mL,:)    = Ptmp.RitpRCdsp;
        P.RphtCrpAll(:,:,mL)    = Ptmp.RphtCrp;
        P.RphtCrpZerAll(:,:,mL) = Ptmp.RphtCrpZer;
        P.RrngCrpAll(:,:,mL)    = Ptmp.RrngCrp;
        P.RxyzCrpAll(:,:,:,mL)  = Ptmp.RxyzCrp;
        P.RdvnCrpAll(:,:,mL)    = Ptmp.RdvnCrp;
        P.RppXmCrpAll(:,:,mL)   = Ptmp.RppXmCrp;
        P.RppYmCrpAll(:,:,mL)   = Ptmp.RppYmCrp;
        %center
        P.CitpRCchkDspAll(mL,:) = Ptmp.CitpRCchkDsp;
        P.CitpRCdspAll(mL,:)    = Ptmp.CitpRCdsp;
    elseif LandR{k}=='R'
        %occluded half from left anchor
        P.LctrCrp(mR,:)         = Ptmp.LctrCrp;
        P.LitpRCchkAll(mR,:)    = Ptmp.LitpRCchk;
        P.LitpRCchkDspAll(mR,:) = Ptmp.LitpRCchkDsp;
        P.LitpRCall(mR,:)       = Ptmp.LitpRC;
        P.LitpRCdspAll(mR,:)    = Ptmp.LitpRCdsp;
        P.LphtCrpAll(:,:,mR)    = Ptmp.LphtCrp;
        P.LphtCrpZerAll(:,:,mR) = Ptmp.LphtCrpZer;
        P.LrngCrpAll(:,:,mR)    = Ptmp.LrngCrp;
        P.LxyzCrpAll(:,:,:,mR)  = Ptmp.LxyzCrp;
        P.LdvnCrpAll(:,:,mR)    = Ptmp.LdvnCrp;
        P.LppXmCrpAll(:,:,mR)   = Ptmp.LppXmCrp;
        P.LppYmCrpAll(:,:,mR)   = Ptmp.LppYmCrp;

        %non-occluded half from left anchor
        P.RctrCrp(mR,:)         = Ptmp.RctrCrp;
        P.RitpRCchkAll(mR,:)    = Ptmp.RitpRCchk;
        P.RitpRCchkDspAll(mR,:) = Ptmp.RitpRCchkDsp;
        P.RitpRCall(mR,:)       = Ptmp.RitpRC;
        P.RitpRCdspAll(mR,:)    = Ptmp.RitpRCdsp;
        P.RphtCrpAll(:,:,mR)    = Ptmp.RphtCrp;
        P.RphtCrpZerAll(:,:,mR) = Ptmp.RphtCrpZer;
        P.RrngCrpAll(:,:,mR)    = Ptmp.RrngCrp;
        P.RxyzCrpAll(:,:,:,mR)  = Ptmp.RxyzCrp;
        P.RdvnCrpAll(:,:,mR)    = Ptmp.RdvnCrp;
        P.RppXmCrpAll(:,:,mR)   = Ptmp.RppXmCrp;
        P.RppYmCrpAll(:,:,mR)   = Ptmp.RppYmCrp;
        %center
        P.CitpRCchkDspAll(mR,:) = Ptmp.CitpRCchkDsp;
        P.CitpRCdspAll(mR,:)    = Ptmp.CitpRCdsp;
    end
end
