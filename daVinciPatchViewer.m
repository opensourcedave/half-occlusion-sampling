function [gdInd] = daVinciPatchViewer(P,wallORcross,gdInd)
%function [gdInd] = patchViewer(P,wallORcross,gdInd)
%
% Use to visualize patches and accept/reject patches from daVinciZonePatches
%
%   example call:
%       IctrRC=daVinciZoneSample(1,[130 52],'L',edg,18,'slice',[0 13],.2,1,1);
%       daVinciZoneSample([52 52],'B',8,18,'bgn','slice',13,0,.2,1,rootDTBdir,1,8,[])
%       dVinciPatchViewer(P,'wall')
%
% --------------------------------------------------------------------
%    INSTRUCTIONS
%    The middle column is the current, the left is pervious, the right is next.
% 	 Make sure figure window is selected for keystrokes to work
%
%    Space:   accept current patch
%    r:       reject current patch
%    Right:   Move to next patch if already analyzed
%    Left:    Move to previous patch
%    Esc:     finish viewing and return index of patches
%    0:       reset analysis
%
% --------------------------------------------------------------------
%    INPUTS
%    P            - struct containing patch information
%    wallORcross  - specifies whether to display stereo images for wall or cross fusers
%                  'wall'
%                  'cross'
%    gdInd        - start where you left off by including this optional parameter
% --------------------------------------------------------------------
%    OUTPUT
%    gdInd        - Output of patch reviewing
%                    0 -> didn't analyze
%                    1 -> accepted
%                   -1 -> rejected
% --------------------------------------------------------------------

%keycodes - see 'NOTE' below on how to figure out keycodes
keys.space=32; %Space
keys.L=28; %LEFT
keys.R=29; %RIGHT
keys.U=30; %UP
keys.G=103;
keys.D=31; %DOWN
keys.r=114; %DOWN
keys.esc=27;
keys.zero=48;

if ~exist('wallORcross','var') || isempty(wallORcross)
    wallORcross='wall';
end

%PRINT INSTRUCTIONS
clc
T=char({'',...
     'Instructions', ...
     'The middle column is the current, the left is pervious, the right is next.',...
	 'Make sure figure window is selected for keystrokes to work',...
     '',...
     'Space:   accept current patch',...
     'r:       reject current patch',...
     'Right:   Move to next patch if already analyzed',...
     'Left:    Move to previous patch',...
     'Esc:     finish viewing and return index of patches',...
     '0:       reset analysis',...
	 ''...
});
disp(T)

%INIT
absMaxInd=size(P.LphtCrpAll,3); %absolute maximum index, determined by n patches
PszRC(1)=size(P.LphtCrpAll,1);  %patch size
PszRC(2)=size(P.LphtCrpAll,2);
resetflag=0; %tells to reset left column upon reset - managed by reset key

%Handle continuation of previous session
if ~exist('gdInd','var')
    gdInd=zeros(absMaxInd,1);
    Ind=1;
	maxInd=1;
else
    Ind=find(gdInd==0,1);
    if isempty(Ind); Ind=1; end
	maxInd=find(gdInd==0,1);
end


fig=figure(182358);
set(gcf,'position',[126 539 1475 358]);

zerCol=zeros(PszRC(1),1);
while true

    for i = -1:1:1 %loop over relative indeces to plot
        j=i+2;     %convert relative indeces to columns to plot

        %IF NO INDEX TO LEFT, CLEAR LEFT COLUMN (ON FIRST INDEX)
        if (Ind + i) <= 0 || resetflag==1
            if gdInd(1)~=0 || resetflag==1
            for k = 1:3:7
                subplot(3,3,k)
                cla reset
                set(gca, 'Color', 'None');
                set(gca,'xtick',[]); set(gca,'ytick',[]);
                hAxes = gca;
                try
                    hAxes.XRuler.Axle.LineStyle = 'none';
                end
                axis off
            end
            end
            resetflag=0;
            continue;
        end
        %IF NO INDEX TO RIGHT, CLEAR RIGHT COLUMN (ON LAST INDEX)
        if (Ind + i) > absMaxInd
            for k = 3:3:9
                subplot(3,3,k)
                cla reset
                set(gca, 'Color', 'None');
                set(gca,'xtick',[]); set(gca,'ytick',[]);
                hAxes = gca;
                try
                    hAxes.XRuler.Axle.LineStyle = 'none';
                end
                axis off
            end
            continue;
        end

        %COLORS FOR REJECTED, ACCEPTED, NOT SEEN
        if gdInd(Ind+i)==-1 %REJECTED
            color='r';
        elseif gdInd(Ind+i)==0 %NOT SEEN
            color='k';
        elseif gdInd(Ind+i)==1 %ACCEPTED
            color='b';
        end

        %HANDLE WALL OR CROSS PLOTTING
        if strcmp(wallORcross,'wall')
            phtImg=[P.LphtCrpAll(:,:,Ind+i) zerCol P.RphtCrpAll(:,:,Ind+i)].^.5;
            dvnImg=[P.LdvnCrpAll(:,:,Ind+i) zerCol P.RdvnCrpAll(:,:,Ind+i)];
            rngImg=[P.LrngCrpAll(:,:,Ind+i) zerCol P.RrngCrpAll(:,:,Ind+i)];
            mBGImg=[   P.LmonoBGmain(:,:,Ind+i) zerCol    P.RmonoBGmain(:,:,Ind+i)];
            bFGImg=[   P.LbinoFG(:,:,Ind+i) zerCol    P.RbinoFG(:,:,Ind+i)];
            bBGImg=[   P.LbinoBG(:,:,Ind+i) zerCol    P.RbinoBG(:,:,Ind+i)];
            ordImg=[ P.LorderMap(:,:,Ind+i) zerCol  P.RorderMap(:,:,Ind+i)];
            edgImg=[  P.LedgeMap(:,:,Ind+i) zerCol   P.RedgeMap(:,:,Ind+i)];
            if P.LorRall(Ind+i)=='L'
                medImg=[ P.MedgeMap(:,:,Ind+i) zerCol  P.SedgeMap(:,:,Ind+i)];
            else
                medImg=[ P.SedgeMap(:,:,Ind+i) zerCol  P.MedgeMap(:,:,Ind+i)];
            end
        elseif strcmp(wallORcross,'cross')
            phtImg=[P.RphtCrpAll(:,:,Ind+i) zerCol P.LphtCrpAll(:,:,Ind+i)].^.5;
            dvnImg=[P.RdvnCrpAll(:,:,Ind+i) zerCol P.LdvnCrpAll(:,:,Ind+i)];
            rngImg=[P.RrngCrpAll(:,:,Ind+i) zerCol P.LrngCrpAll(:,:,Ind+i)];
            mBGImg=[   P.RmonoBGmain(:,:,Ind+i) zerCol    P.LmonoBGmain(:,:,Ind+i)];
            bFGImg=[   P.RbinoFG(:,:,Ind+i) zerCol    P.LbinoFG(:,:,Ind+i)];
            bBGImg=[   P.RbinoBG(:,:,Ind+i) zerCol    P.LbinoBG(:,:,Ind+i)];
            ordImg=[ P.RorderMap(:,:,Ind+i) zerCol  P.LorderMap(:,:,Ind+i)];
            edgImg=[  P.RedgeMap(:,:,Ind+i) zerCol   P.LedgeMap(:,:,Ind+i)];
            if P.LorRall(Ind+i)=='L'
                medImg=[ P.SedgeMap(:,:,Ind+i) zerCol  P.MedgeMap(:,:,Ind+i)];
            else
                medImg=[ P.MedgeMap(:,:,Ind+i) zerCol  P.SedgeMap(:,:,Ind+i)];
            end
        end

        % PLOT IMAGES

        %1st row
        subplot(7,3,j);
        imagesc(phtImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title([num2str(Ind+i) ' ' P.LorRall(Ind+i) ' ' num2str(P.imgNumAll(Ind+i)) newline 'Luminance'],'Color',color)

        %2rd row
        subplot(7,3,3+j);
        imagesc(rngImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['Range'],'Color',color)

        %3nd row
        subplot(7,3,6+j)
        imagesc(mBGImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['MonoBG'],'Color',color)

        %4th row
        subplot(7,3,9+j)
        imagesc(bFGImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['BinoFG'],'Color',color)

        %5th row
        subplot(7,3,12+j)
        imagesc(bBGImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['BinoBG'],'Color',color)

        %6th row
        subplot(7,3,15+j)
        imagesc(ordImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['Depth Order'],'Color',color)

        %7th row
        subplot(7,3,18+j)
        imagesc(edgImg)
		hold on; plot([PszRC(2)+1 PszRC(2)+1],[1 PszRC(1)],'r'); hold off
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        set(gca,'YDir','reverse');
        axis image
        colormap gray
        title(['Edge map'],'Color',color)
    end
    drawnow
    pause(.2)

    %HANDLE KEYBOARD INPUT
    while true %LOOP UNTIL VALID KEY IS PRESSED

        %WAIT FOR THEN CAPTURE KEYPRESS
        kk=waitforbuttonpress;
        k = double(get(gcf,'CurrentCharacter')); % NOTE uncomment to see keycodes on keypress

        %REJECT
        if     k==keys.r

            %MARK AS REJECTED
            gdInd(Ind)=-1;

            %INCREMENT REL MAX IF YOU CAN
            maxInd=find(gdInd==0,1);
            if isempty(maxInd); maxInd=absMaxInd+1; end

            %INCREMENT INDEX IF NOT TOO LARGE
            if Ind+1 < absMaxInd
                Ind=Ind+1;
            end

            break
        %ACCEPT
        elseif k==keys.space

            %MARK AS ACCEPTED
            gdInd(Ind)=1;

            %INCREMENT REL MAX IF YOU CAN
            maxInd=find(gdInd==0,1);
            if isempty(maxInd); maxInd=absMaxInd+1; end

            %INCREMENT INDEX IF NOT TOO LARGE
            if Ind+1 <= absMaxInd
                Ind=Ind+1;
            end

            break
        %RIGHT
        elseif k==keys.R

            if isempty(maxInd); maxInd=absMaxInd+1; end

            %MOVE INDEX RIGHT IF NEXT PATCH ALREADY VIEWED
            if Ind+1 <= maxInd && Ind+1 < absMaxInd
                Ind=Ind+1;
            end

            break
        %LEFT
        elseif k==keys.L

            %MOVE INDEX LEFT IF YOU CAN
            if Ind-1>0
                Ind=Ind-1;
            end

            break
        %RESET
        elseif k==keys.zero
            %EVERYTHING TO INITIAL VALUES
            Ind=1;
            maxInd=1;
            absMaxInd=size(P.LphtCrpAll,3);
            gdInd=zeros(absMaxInd,1);
            resetflag=1; %FLAG TO ERASE LEFT COLUMN
            break
        elseif k==keys.G
            breakflag=0;
            prompt = {'Jump to which image?'};
            titl = 'Jump to image number';
            definput = {'1'};
            while true
                response = inputdlg(prompt,titl,[1 40],definput);
                if isempty(response) %cancel button
                    break
                end
                try
                    response=str2double(response{1}); %non-number
                catch
                    continue
                end
                if ismember(response,1:length(P.LorRall))
                    Ind=response;
                    if maxInd<Ind
                        maxInd=Ind;
                    end
                    breakflag=1;
                    break
                end
            end
            if breakflag==1
                break
            end
        %EXIT
        elseif k==keys.esc
            close(fig)
            return
        end
    end
end
