function [Sensitivity LambdaNm] = ScotopicSensitivity(LambdaMinMax,LambdaStep)

% function [Sensitivity LambdaNm] = ScotopicSensitivity(LambdaMinMax,LambdaStep)
%
% Scotopic sensitivity at 5 nm resolution in units of Energy (linear) 
% from Wyszecki and Stiles (1982), directly off of CVRL website
% 
% LambdaMinMax: min and max values for which to return sensitivities
%                   default: [400 700]
% LambdaStep:   spacing between successive sensitivity samples
%                   default: 5
%
%
% NOTES: The standard scotopic luminosity function or V'(?), which was
% adopted by the CIE in 1951 (CIE, 1951), is based on measurements by Wald 
% (1945) and by Crawford (1949), and is considered to represent observers 
% under the age of 30. The two sets of measurements were made under very 
% different conditions. Wald's data, which are slightly steeper at longer 
% wavelengths, are thresholds for a small 1� diameter target, whereas 
% Crawford's are for a match between two 20� half fields. There is some 
% concern that Crawford's measurements were subject to cone intrusion at 
% long wavelengths.
%
% References: 
% Crawford (1949) The scotopic visibility function, Proceedings of the 
% Physical Society, B62, 321-334.
%
% CIE Proceedings (1951), Vol. 1, Sec 4; Vol 3, p. 37, Bureau Central 
% de la CIE, Paris, 1951.
%
% Wald, G. (1945). Human vision and the spectrum. Science, 101, 653-658.
%
% Wyszecki, G., & Stiles, W. S. (1982). Color Science: concepts and 
% methods, quantitative data and formulae. (2nd ed.). New York: Wiley
%
% http://www-cvrl.ucsd.edu/

if ~exist('LambdaMinMax','var') || isempty(LambdaMinMax)
   LambdaMinMax = [400 700]; 
end
if ~exist('LambdaStep','var') || isempty(LambdaStep)
   LambdaStep = 5; 
end

rawData = [ 
380	5.89E-04
385	1.11E-03
390	2.21E-03
395	4.53E-03
400	9.29E-03
405	1.85E-02
410	3.48E-02
415	6.04E-02
420	9.66E-02
425	1.44E-01
430	2.00E-01
435	2.63E-01
440	3.28E-01
445	3.93E-01
450	4.55E-01
455	5.13E-01
460	5.67E-01
465	6.20E-01
470	6.76E-01
475	7.34E-01
480	7.93E-01
485	8.51E-01
490	9.04E-01
495	9.49E-01
500	9.82E-01
505	9.98E-01
510	9.97E-01
515	9.75E-01
520	9.35E-01
525	8.80E-01
530	8.11E-01
535	7.33E-01
540	6.50E-01
545	5.64E-01
550	4.81E-01
555	4.02E-01
560	3.29E-01
565	2.64E-01
570	2.08E-01
575	1.60E-01
580	1.21E-01
585	8.99E-02
590	6.55E-02
595	4.69E-02
600	3.32E-02
605	2.31E-02
610	1.59E-02
615	1.09E-02
620	7.37E-03
625	4.97E-03
630	3.34E-03
635	2.24E-03
640	1.50E-03
645	1.01E-03
650	6.77E-04
655	4.59E-04
660	3.13E-04
665	2.15E-04
670	1.48E-04
675	1.03E-04
680	7.15E-05
685	5.01E-05
690	3.53E-05
695	2.50E-05
700	1.78E-05
705	1.27E-05
710	9.14E-06
715	6.60E-06
720	4.78E-06
725	3.48E-06
730	2.55E-06
735	1.87E-06
740	1.38E-06
745	1.02E-06
750	7.60E-07
755	5.67E-07
760	4.25E-07
765	3.20E-07
770	2.41E-07
775	1.83E-07
780	1.39E-07];

LambdaNm    = [LambdaMinMax(1):LambdaStep:LambdaMinMax(2)]';
Sensitivity = interp1(rawData(:,1),rawData(:,2),LambdaNm,'spline');

% CHECK FOR NEGATIVE INDICES
if ~isempty(find(Sensitivity < 0, 1))
    Sensitivity(Sensitivity < 0) = 0;
end

% WARN IF EXTRAPOLATING BEYOND LIMITS OF DATA
if min(LambdaMinMax) < min(rawData(:,1)) || max(LambdaMinMax) > max(rawData(:,1))
   disp(['ScotopicSensitivity: WARNING! extrapolating beyond measured data. minmax(rawdata) = [' num2str(min(rawData(:,1))) ' ' num2str(max(rawData(:,1))) '] vs LambdaMinMax [' num2str(LambdaMinMax(1)) ' ' num2str(LambdaMinMax(2)) ']']); 
end