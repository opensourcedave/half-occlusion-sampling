function [LppX,LppY,LppZ,LppXdeg,LppYdeg,RppX,RppY,RppZ,RppXdeg,RppYdeg] =  LRSIprojPlane(units,dnK)

% function [LppX LppY LppZ LppXdeg LppYdeg RppX RppY RppZ RppXdeg RppYdeg] =  LRSIprojPlane(units,dnK)
%
%   example call: [LppX LppY LppZ LppXdeg LppYdeg RppX RppY RppZ RppXdeg RppYdeg] =  LRSIprojPlane(1, 1);
%
% load in details of the (p)rojection (p)lane with which the luminance range images were sampled...
%
% the luminance range image data was projected onto a frontoparallel plane 3 meters from the observer
% the center of the projection plane in cyclopean eye coordinates is      0,0,3
% the center of the projection plane in left      eye coordinates is +IPD/2,0,3
% the center of the projection plane in right     eye coordinates is -IPD/2,0,3
%
% see LumRangeImages_ReadMe.txt in ../Project_ImageDatabases/LumRangeImages/
%
% units:     units in which to return distance estimates
% dnK:        units in which to return distance estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LppX:       left  eye x position samples in units
% LppY:       left  eye y position samples in units
% LppZ:       left  eye z position of the projection plane in units
% LppXdeg:    left  eye x position samples in visual deg
% LppYdeg:    left  eye x position samples in visual deg
% RppX:       right eye x position samples in units
% RppY:       right eye y position samples in units
% RppZ:       right eye z position of the projection plane in units
% RppXdeg:    right eye x position samples in visual deg
% RppYdeg:    right eye x position samples in visual deg

% INPUT HANDLING
if ~exist('units','var') || isempty(units) units = 1; end
if ~exist('dnK','var')   || isempty(dnK)     dnK = 1; end

% STEREO CAMERA IPD IN METERS
IPD     = LRSIcameraIPD(units);
% IMAGE SIZE IN PIXELS
I       = zeros(1080./dnK,1920./dnK);
% SCREEN SIZE IN METERS
screenX = 1.940/units;     %  see the file LumRangeImages_ReadMe.txt in
screenY = 1.118/units;     %  ../Project_ImageDatabases/LumRangeImages

[LppX,LppY,LppZ,LppXdeg,LppYdeg] =  LRSIprojPlaneAnchorEye('L',units, dnK);
[RppX,RppY,RppZ,RppXdeg,RppYdeg] =  LRSIprojPlaneAnchorEye('R',units, dnK);
