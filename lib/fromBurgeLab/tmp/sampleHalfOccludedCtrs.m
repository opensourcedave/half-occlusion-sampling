function [IctrInd,maxSmp] = sampleHalfOccludedCtrs(PszXY,LorR,imgNum,minPixDVN,minDVNdensityRatio,rndSd,bPlot,plotImgNum)

%% INIT
minSampleDist = max(PszXY);
IszRC         = [1080 1920]; %DATABASE IMAGE DIMENSIONS

%CONVERT B TO L & R
if strcmp(LorR,'B')
    LorRboth={'L','R'};
else
    LorRboth=LorR;
end

%DaVinci CONDITIONING VARIABLES - SEE "REMOVE INVALID SAMPLING POINTS" SECTION
DVNkernRadHW = [52,minPixDVN/2]; %HEIGHT (1) AND WIDTH (2) OF SURROUNDING REGION FOR minDVNdensityRatio - 2 values and MUST BE ODD!
DVNkernArea  = (DVNkernRadHW(1)*2+1)*(DVNkernRadHW(2)*2+1);

%SAMPLING VARIABLES - SEE "SAMPLING" SECTION
smpRadius=ceil(minSampleDist/2);
smpBorder=smpRadius*2;
indLookup=reshape(1:(IszRC(1)*IszRC(2)),IszRC);
rndFlag=0;
if rndSd==0
    bRnd=0;
else
    bRnd=1;
end

% preallocation
IctrInd = cell(98,length(LorRboth));
IctrRC  = cell(98,length(LorRboth));
maxSmp  = zeros(98,length(LorRboth));
% --------------------------------------------------------------------
%% FIND VALID SAMPLE POINTS

for iI = 1:length(imgNum) %FOR EACH IMAGE

    i=imgNum(iI);
    %LOAD PHOTO IMAGES, RARNGE IMAGES,
    [Lpht,Rpht] = loadLRSIimage(i,1,0,'PHT','img',rootDTBdir);

    %GET DaVinci IMAGE
    [Ldvn,Rdvn] = loadLRSIimageDVN(i,'local',0,rootDTBdir','DVN2');

    for k  = 1:length(LorRboth)
        LorRtmp=LorRboth{k};
        %HANDLE PLOTTING
        if i==plotImgNum && bPlot==1 && k==1
            bPlotTmp=1;
        else
            bPlotTmp=0;
        end

        % --------------------------------------------------------------------
        %% LOAD IMAGES FROM DATABASE

        if strcmp(LorRtmp,'L')
            Idvn=Ldvn;
        elseif strcmp(LorRtmp,'R')
            Idvn=Rdvn;
        end

        %PLOT DAVINCI IMAGE
        if bPlotTmp == 1
           figure(nFn)
           subplot(2,1,1);
           imagesc(~[Ldvn Rdvn]);
           formatFigure([],[],['Img=' num2str(1)]);
           set(gca,'xtick',[]); set(gca,'ytick',[]);
           axis image
           colormap gray

           subplot(2,1,2);
           imagesc(~[Ldvn Rdvn].*[Lpht Rpht].^.5)
           set(gca,'xtick',[]); set(gca,'ytick',[]);
           axis image
           colormap gray
        end


        % --------------------------------------------------------------------
        %% REMOVE INVALID SAMPLING POINTS
        % Pre sampling conditions
        % 1. Pixel is half occluded
        % 2. Sampled pixel is found within horizontal center of a half-occluded region/run with:
        %    a. Pixel width of half-occluded region >= minPixDVN
        %    b. Ratio of half occluded to non-HO in region surrounding pixel (dimensions specified by DVNkernRadHW) is >= minDVNdensityRatio
        %       This is meant to satisfy a "height" requirement, wheras a. satisfies a width requirement

        %1-2a: FIND CENTERS OF DaVinci REGIONS & THEIR WIDTHS - HORIZONTAL
        [IdvnCtrRC,IdvnCtrInd] = daVinciZoneCenter(Idvn,minPixDVN);

        %PLOT VALID POINTS
        if bPlotTmp==1
            figH=nFn; figure(figH)
            hold on
            if strcmp(LorRtmp,'L')==1
                imagesc(Lpht)
            elseif strcmp(LorRtmp,'R')==1
                imagesc(Rpht)
            end
            colormap gray
            hold on
            [U,V]=find(Idvn==1);
            scatter(V,U,'b.')
            idx=ismember([IdvnCtrRC(:,1),IdvnCtrRC(:,2)],[U,V],'rows');
            scatter(IdvnCtrRC(idx,2),IdvnCtrRC(idx,1),'w.');
            xlim([0,1920])
            ylim([0,1080])
            set(gca,'xtick',[]); set(gca,'ytick',[]);
            set(gca,'YDir','reverse');set(gca,'YDir','reverse');
        end

        %2b: FIND DaVinci INDICES THAT SATISFY A DENSITY REQUIREMENT
        valInd=[];
        ratio=zeros(length(IdvnCtrInd),1);
        for j = 1:length(IdvnCtrInd)
            vert=cell2mat(arrayfun(@(x) x-DVNkernRadHW(1):x+DVNkernRadHW(1),IdvnCtrRC(j,1),'UniformOutput',false));
            hori=cell2mat(arrayfun(@(x) x-DVNkernRadHW(2):x+DVNkernRadHW(2),IdvnCtrRC(j,2),'UniformOutput',false));
            vert=reshape(vert,numel(vert),1);
            hori=reshape(hori,numel(hori),1);
            vert=vert(vert>0 & vert<=IszRC(1));
            hori=hori(hori>0 & hori<=IszRC(2));
            ratio(j)=(sum(sum(Ldvn(vert,hori)))/DVNkernArea);
            if ratio(j)>minDVNdensityRatio
                valInd=[valInd; j];
            end
        end
        PctrInd=IdvnCtrInd(valInd);
        PctrRC=IdvnCtrRC(valInd,:);

        %PLOT VALID POINTS
        if bPlotTmp==1
            figure(figH)
            hold on
            scatter(PctrRC(:,2),PctrRC(:,1),'r.');
            xlim([0,1920])
            ylim([0,1080])
        end
        % ----------------------------------------------------------------
        %% SAMPLING
        %XXX change variable names
        %     quickly sample as many points as possible, without overlap - OPTIMIZED
        %     bRnd      - equals 1 if random seed (rndSd) is not empty
        %                 if equals 1, will randomly select next sampled index
        %                      different for each seed, but produces fewer points
        %                 if equals 0, will sample first available point.
        %                       produces more points, but will be the same each this code is ran
        %     IctrInd    - indeces sampled for all images cell [length(imgNum), 1]
        %     valSmp    - indeces sampled for a given image
        %     Lookup    - specifies valid sampling points: valid == 0, invalid > 0
        %                 after sampling patch index, surrounding region takes on a valeu of 1, as to not create patch overlap
        %     indLookup - indeces for each pixel for quick conversion from logical index
        %     valPot    - Lookup, where pseudo logcial indeces==0 are converted to reguar indeces
        %                 sampling occurs on from matrix
        %     N,M       - subscripts of valid sampling indeces. Used to apply 1s to surrounding region

        % Create smpBorder that limits sampling, multiply smpRadius by 3 due to buffering in LRSI sampling
        hold off
        Lookup                                              = ones(IszRC); %Lookup defaults to 1, invalid sampling point
        Lookup(PctrInd)                                     = 0; %Make valid sampling points equal to zero
        Lookup(1:end,               1:smpBorder)            = 1; %Do not sample around smpBorder
        Lookup(1:end,               IszRC(2)-smpBorder:end) = 1; %...
        Lookup(1:smpBorder,            1:end)               = 1; %...
        Lookup(IszRC(1)-smpBorder:end, 1:end)               = 1; %...

        valSmp=[];
        while true

            %VALID SAMPLING POINTS
            valPot=indLookup(Lookup==0);

            if isempty(valPot)
                break
            end

            %SAMPLE RANDOM INDEX IF bRnd
            if bRnd==1
                if rndFlag==0
                    rng(rndSd,'v4');
                    rndFlag=1;
                end
                ind=randi(length(valPot),1);
            %SAMPLE FIRST INDEX IF NOT
            else
                ind=1;
            end
            %SAVE APPLIED INDEX
            valSmp(end+1,1)=valPot(ind);

            %REMOVE VALID POINTS FROM AROUND SAMPLED INDEX
            [N,M]=ind2sub(IszRC,valPot(ind));
            vert=cell2mat(arrayfun(@(x) x-minSampleDist:x+minSampleDist,N,'UniformOutput',false));
            hori=cell2mat(arrayfun(@(x) x-minSampleDist:x+minSampleDist,M,'UniformOutput',false));
            vert=reshape(vert,numel(vert),1);
            hori=reshape(hori,numel(hori),1);
            Lookup(vert,hori)=Lookup(vert,hori)+1; %GOOD WAY TO VERIFY NO OVERLAP IS TO LOOK FOR VALUES GREATER THAN 2
        end
        IctrInd{i,k}=valSmp;
        [N,M]=ind2sub(IszRC,valSmp);
        IctrRC{i,k}=[N,M];
        maxSmp(i,k)=numel(valSmp);
        if bPlotTmp==1
            figure(figH)
            hold on
            scatter(M,N,'y','filled');
            xlim([0,IszRC(2)])
            ylim([0,IszRC(1)])
        end
    end
% --------------------------------------------------------------------
    %% PLOT SAMPLED POINTS
end

% --------------------------------------------------------------------
%% CLEANUP
%change back to original directory

%NOTE - these variables are no longer very useful as they change on loop
clear bPlotTmp i j k rndFlag tdir U V begs width ctr IdvnCtrRC IdvnCtrInd valInd oldDir Lookup valSmp hori vert N M

%SAVE
if ~isempty(fname)
	save(fname)
end
