function [ItpRCnewA,ItpRCnewB,dspArcMin,exitflag] = LRSIrmPatchCrossedDisparity(LorR,ItpRCA,ItpRCB,Cxyz,PszXY,bPlot)
exitflag=0;

[PxyzA,exitflag] = recenterPatchCtrRngXYZ(ItpRCA,Cxyz,PszXY,bPlot);
if exitflag==1; return; end
[PxyzB,exitflag] = recenterPatchCtrRngXYZ(ItpRCB,Cxyz,PszXY,bPlot);
if exitflag==1; return; end

if strcmp(LorR,'L')
    [Ldsp,Rdsp] = LRSIxyz2dsp(PxyzA,PxyzB,'center',PctrRC,1); % FROM RANGE INFO, GET DISPARITY MAPPING FOR PATCH
    dspArcMin=-1*max(max(Ldsp));                            % FIND LARGEST CROSSED DISPARITY IN PATCH

    %ADJUST IMAGE CTRS TO ELIMINATE CROSSED DISPARITY
    [ItpRCnewA, ItpRCnewB]=LRSIcorrespondingPointAddDisparityExact(ItpRC,ItpRCchk,dspArcMin,LppXm,LppYm,RppXm,RppYm,IppZm,IPDm);


elseif strcmp(LorR,'R')
    [Ldsp,Rdsp] = LRSIxyz2dsp(PxyzB,PxyzA,'center',PctrRC,1); % FROM RANGE INFO, GET DISPARITY MAPPING FOR PATCH
    dspArcMin=-1*max(max(Rdsp));                            % FIND LARGEST CROSSED DISPARITY IN PATCH

    %ADJUST IMAGE CTRS TO ELIMINATE CROSSED DISPARITY
    [ItpRCnewB, ItpRCnewA]=LRSIcorrespondingPointAddDisparityExact(ItpRCchk,ItpRC,dspArcMin,LppXm,LppYm,RppXm,RppYm,IppZm,IPDm);
end

%PLOT DETAILS OF SHIFTING PROCESS BASED ON DISPARITY FROM RANGE
if bPlot==1
    figure(nFn)
    hold on
    subplot(1,2,1)
    imagesc(Ldsp)
    axis square
    title('disparity map for Left eye')
    colorbar
    subplot(1,2,2)
    imagesc(Rdsp)
    axis square
    title('disparity map for Left eye')
    colorbar
    hold off
end
