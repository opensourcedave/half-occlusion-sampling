function dispArcMin = distance2disp(tgtDst,fixDst,IPD,units)

% function dispArcMin = distance2disp(tgtDst,fixDst,IPD,units)
%
%   example call: distance2disp(1100,1000,65,1e-3)  
% 
% convert distance to disparity for a specified fixation distance
% returns disparity in arcmin. distances are specified in arbitrary units
%
% tgtDst:  target distances           (in units)
%           must be positive valued
% IPD:      inter pupillary distance   (in units)
% fixDst:  distance to fixation point (in units)
% units:    units in which to enter and return results
%             1    -> meters
%             1e-2 -> centimeters
%             1e-3 -> millimeters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% dispArcMin: disparity in arc min
%             negative disp -> uncrossed (farther than fixation)
%             positive disp -> crossed   (closer than fixation)

numBadtgtDst = length(find(tgtDst <= 0));
numBadfixDst = length(find(fixDst <= 0));
if numBadtgtDst > 0
    % disp(['distance2disp: WARNING! ' num2str(numBadtgtDst) ' tgt points <= 0']);
end
if numBadfixDst > 0
    % disp(['distance2disp: WARNING! ' num2str(numBadfixDst) ' tgt points <= 0']);
end
depth      = tgtDst - fixDst;

IPDm     = IPD.*units;
tgtDstM = tgtDst.*units;
fixDstM = fixDst.*units;

dispArcMin = 60.*( 2.*atand(0.5.*IPDm./tgtDstM) - 2.*atand(0.5.*IPDm./fixDstM) );
% dispArcMin = depth2disp(depth,IPD,fixDst,units);