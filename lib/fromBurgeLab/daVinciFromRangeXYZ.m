function [Idg,Igd,Ibnd,Idvn] = daVinciFromRangeXYZ(LorR,IPDm,Ixyz,DGcutoffs,bPLOT)

% function [Idg Igd Ibnd Idvn] = daVinciFromRangeXYZ(LorR,IPDm,Ixyz,DGcutoffs,bPLOT)
%
%   example call: % POINTS EXCEEDING DISPARITY GRADIENT = 2.0 IN LE IMAGE (half-occlusions)
%                   daVinciFromRangeXYZ('L',0.065,Lxyz,2.0)
%
%                 % POINTS EXCEEDING DISPARITY GRADIENT = 2.0 IN RE IMAGE (half-occlusions)
%                   daVinciFromRangeXYZ('R',0.065,Rxyz,2.0)
%
%                 % POINTS EXCEEDING EACH OF SEVERAL DISPARITY GRADIENT CUTOFFS
%                   daVinciFromRangeXYZ('L',0.065,Lxyz,1.0:.25:2.0)
%
%                 % POINTS EXCEEDING CUTOFF FROM SAMPLED PATCHES
%                   daVinciFromRangeXYZ(S.LorR(c),S.IPDm,S.LxyzCrp(:,:,:,c),2.0,1);
%
% points exceeding a disparity gradient cutoff by computing the
% disparity gradient point n-neighbors. diff(Vergence)/diff(Version)
%
% this calculation is performed by taking the difference in visual angle
% between n-neighbors and dividing by the visual angle between them
%
% a brief explanation of n-neighbors
%   if n == 1, DG is computed for next   door neighbors
%   if n == 2, DG is computed for second door neighbors
%   if n == 3, DG is computed for third  door neighbors
%
% LorR:      which image to use as a reference
%            'L' -> left  image as reference
%            'R' -> right image as reference
% IPDm:      inter-pupillary (i.e. inter-nodal point) distance in meters
% Ixyz:      range xyz w/ coord system center at LorR eye  [ n x m x 3 ]
%            'X' -> x position in meters
%            'Y' -> y position in meters
%            'Z' -> z position in meters
% DGcutoffs: vector of disparity gradient cutoffs used to select points
%            important DG cutoff values
%             1  -> fusion   limit (approx)
%             2  -> half-occlusion
% bPLOT:      1  -> plot
%             0  -> not
% %%%%%%%%%%%%%%%%%%%%%%
% Idg:       cell array of length(DGcutoffs) w. col vectors of indices for points avoe    each value in DGcutoffs
% Igd:       cell array of length(DGcutoffs) w. col vectors of indices for points below   each value in DGcutoffs
%            NOTE: 'dg' -> points that exceed critical (d)isparity (g)radient
%                  'gd' -> points that should o.k... i.e. they are (g)oo(d)
% Ibnd:      cell array of length(DGcutoffs) w. col vectors of indices for points between each value in DGcutoffs
% Iimg:      array of logicals that is size of input image [ n x m ]
%            indicating whether pixel is above or below cutoff
%            if DGcutoff = 2.0,
%            1 -> half-occluded, 0 -> not half-occluded


if ~exist('bPLOT','var') || isempty(bPLOT), bPLOT = 0; end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERGENCE AND VERSION ANGLES %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERGENCE ANGLE (i.e. ABSOLUTE DISPARITY ASSUMING INFINITE GAZE)
IvrgDeg = vergenceFromRangeXYZVec(LorR,IPDm,Ixyz);
% VERSION  ANGLE OF POINT IN EPIPOLAR PLANE
%XXX need vectorized version - no pun intended 
IvrsDeg = versionFromRangeXYZ(LorR,IPDm,Ixyz);
% MAX PIXEL SHIFTS TO CHECK
maxPixShifts = min([75 max(size(Ixyz(:,:,1)))]);

% PREALLOCATE MEMORY: POINTS EXCEEDING DGcutoffs (dgc)
Idff = zeros([size(IvrgDeg) maxPixShifts]);
for i = 1:maxPixShifts,
    if strcmp(LorR,'L')
        %% DIFFERENCE KERNEL WITH DIFFERENENT SPATIAL SEPARATIONS
        Kdff = [1 zeros(1,i-1) -1 zeros(1,i)];
        % DISPARITY GRADIENT FOR N-NEIGHBORS (DG = ?disparity/?cyclopeanDirection)
        IvrgDegGx   = conv2(IvrgDeg, Kdff,'same');
        IvrsDegGx   = conv2(IvrsDeg, Kdff,'same');
        Idff(:,:,i) = IvrgDegGx./IvrsDegGx;
%         if max(max(Idff(:,:,i))) < 10
        % figure; imagesc(Idff(:,:,i)); colormap gray; colorbar; formatFigure(['Press button to continue...'],[],['Shift ' num2str(i) ' out of ' num2str(maxPixShifts)]); axis image; % waitforbuttonpress;
%         figure('position',[1000 50 700 1100]); subplot(4,2,1); imagesc(cropImage(IvrgDeg,[1 1],[80 80])); colorbar; subplot(4,2,2); imagesc(cropImage(IvrsDeg,[1 1],[80 80])); colorbar;
%                 subplot(4,2,3); imagesc(cropImage(conv2(IvrgDeg,Kdff,'same'),[1 1],[80 80])); colorbar; subplot(4,2,4); imagesc(cropImage(conv2(IvrsDeg,Kdff,'same'),[1 1],[80 80])); colorbar;
%                 subplot(4,2,[5:8]); imagesc(conv2(IvrgDeg,Kdff,'same')./conv2(IvrsDeg, Kdff,'same')); colorbar; caxis([-3 3]);
%         end
    elseif strcmp(LorR,'R')
        % DIFFERENCE KERNEL WITH DIFFERENENT SPATIAL SEPARATIONS
        Kdff = [zeros(1,i) -1 zeros(1,i-1) 1];
        % DISPARITY GRADIENT FOR N-NEIGHBORS (DG = ?disparity/?cyclopeanDirection)
        IvrgDegGx   = conv2(IvrgDeg, Kdff,'same');
        IvrsDegGx   = conv2(IvrsDeg,-Kdff,'same');
        Idff(:,:,i) = IvrgDegGx./IvrsDegGx;
        % Idff(:,:,i) = conv2(IvrgDeg, Kdff,'same')./conv2(IvrsDeg,-Kdff,'same'); % DG defined as ?disparity/?cyclopeanDirection
        % if max(max(Idff(:,:,i))) < 10
        % figure; imagesc(Idff(:,:,i)); colormap gray; colorbar; formatFigure(['Press button to continue...'],[],['Shift ' num2str(i) ' out of ' num2str(maxPixShifts)]); axis image; % waitforbuttonpress;
        % end
    end

    if mod(i,2) == 0
%        figure('position',[200 200 700 1300]); subplot(4,1,1); imagesc(IvrsDeg); subplot(4,1,2); plot(IvrsDegGx(8,:)); ylim([-4 4]); subplot(4,1,3); imagesc(Idff(:,:,i)); caxis([-1 3]); subplot(4,1,4); plot(Idff(8,:,i)); ylim([-1 3]); hold on; plot(xlim,[2 2],'k--'); plot([i+1 i+1],ylim,'k--');
    end
    % HANDLE BOUNDARY CONDITIONS
    if     strcmp(LorR,'L') indBdC = (i+1):size(IvrsDeg,2); ind = 1:(numel(IvrsDeg)-i*size(IvrsDeg,1));
    elseif strcmp(LorR,'R') indBdC = 1:i;                   ind = (i+1)*size(IvrsDeg,1):numel(IvrsDeg);
    end
    % Idff(:,indBdC,i) = 0;
    % BREAK OUT OF LOOP IF NO POINTS EXCEED CUTOFF
    if sum(sum(Idff(:,:,i) > min(DGcutoffs))) == 0 && i > 20 % min(IvrsDegGx(ind))>0 % && i > 20 % && min(IvrsDegGx(:)>0) %
        Idff(:,:,i:end) = []; i = i-1;
        break;
    end
end

if i == maxPixShifts
   disp(['daVinciFromRangeXYZ: WARNING! consider increasing maxPixShifts. maxPixShifts == ' num2str(maxPixShifts) ' and i=' num2str(i)]);
end
%%
% FIND POINTS EXCEEDING EACH DISPARITY GRADIENT CUTOFF
for d = 1:length(DGcutoffs)
    % ROWS AND COLUMNS OF LOCATIONS EXCEEDING CUTOFF FOR ALL N-NEIGHBORS
    [r,c,s] = ind2sub([size(IvrgDeg)],find(Idff>DGcutoffs(d)));
    % COLLAPSE TO A SINGLE
    RC    = unique([r c],'rows');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % INDICES OF LOCATIONS ABOVE CUTOFF % (if DG = 2.0, da Vinci points)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Idg{d} = sub2ind(size(IvrgDeg),RC(:,1),RC(:,2));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % INDICES OF LOCATIONS ABOVE CUTOFF % (if DG = 2.0, binocularly visible points)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargout > 1
    Igd{d}=setxor(Idg{d},[1:numel(IvrgDeg)]');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % INDICES OF LOCATIONS WITHIN DG RANGE (i.e. BAND) % ( DG(i-1) and DG(i)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargout > 2
        if d ==1, Ibnd{d} = Igd{d};
        else      Ibnd{d} = setxor(Idg{d-1}, Idg{d});
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % CREATE DG POINT IMAGE %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargout > 3
        if d == 1
        Idvn = false([size(Ixyz(:,:,1)) length(DGcutoffs)]);
        end
        Idvn(Idg{d}+(d-1).*numel(Ixyz(:,:,1))) = true;
    end
end

if bPLOT == 1
   if ~exist('Idvn','var') || isempty(Idvn)
       if d == 1
        Idvn = false([size(Ixyz(:,:,1)) length(DGcutoffs)]);
        end
        Idvn(Idg{d}+(d-1).*numel(Ixyz(:,:,1))) = true;
   end
   figure;
   imagesc(~Idvn);
   formatFigure([],[],'Half-occluded points')
   axis image; colormap gray
end
%%
% DGcutoff = 2.0;
% for i = 0:98,
%     i
%     [Limg Rimg Lrng Rrng Lxyz Rxyz]=loadLRSIimage(i);
%     [Ldg Lgd Lbnd] = daVinciFromRangeXYZ('L',IPDm,Lxyz,DGcutoff);
%     [Rdg Rgd Rbnd] = daVinciFromRangeXYZ('R',IPDm,Rxyz,DGcutoff);
%     fnameL = buildDaVinciLRSIfilename('L',DGcutoff,i);
%     fnameR = buildDaVinciLRSIfilename('R',DGcutoff,i);
%     save([fnameL],'Ldg','Lgd','Lbnd');
%     save([fnameR],'Rdg','Rgd','Rbnd');
% end
