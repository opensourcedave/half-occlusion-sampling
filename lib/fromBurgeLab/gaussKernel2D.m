function G = gaussKernel2D(GsigPix,GszPix,bPLOT)

% function G = gaussKernel2D(GsigPix,GszPix,bPLOT)
%
%   example call: G = gaussKernel2D(10,[],1)
%
% returns 2D Gaussian kernel with coefficients that sum to 1.0
%
% GsigPix: standard deviation of Gaussian kernel
% GszPix:    scalar value specifying size of the kernel 
%            xPix = yPix
%            defaults to odd sized kernel of size just larger than 6*sigma
%            specifically, [] -> size equals (+/-) 3*sigma and is 
% bPLOT:     1 -> plot
%            0 -> not
% %%%%%%%%%%%%%%%%%%%%%%
% G:         2D Gaussian   kernel 

if ~exist('GsigPix') || isempty(GsigPix)
   if exist('GszPix','var') && ~isempty(GszPix)
       GsigPix = min(GszPix)/6;
   else
      error(['gaussKernel2D: WARNING! if GsigPix not defined, GszPix must be']); 
   end
end
if ~exist('GszPix','var') || isempty(GszPix)
   GszPix = ceil(GsigPix.*6+1) + mod(ceil(GsigPix.*6+1)+1,2); 
end
if ~exist('bPLOT','var') || isempty(bPLOT)
   bPLOT = 0; 
end

[Xpix,Ypix] = meshgrid(-(GszPix-1)/2:(GszPix-1)/2);

% GAUSSIAN KERNEL (negative sign makes derivative kernel intuitive)
G    = (1./(2.*pi.*GsigPix)).*exp( -0.5.*(Xpix.^2 + Ypix.^2)./(GsigPix.^2) );
G    = G./sum(G(:));


if bPLOT
   figure;
   imagesc(G); colorbar;
   axis image
   axis xy
   formatFigure('X','Y','Gaussian Kernel');
end