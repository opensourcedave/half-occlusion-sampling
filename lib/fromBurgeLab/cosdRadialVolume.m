function [W,Xdeg,Ydeg,Zdeg] = cosdRadialVolume(Xdeg,Ydeg,Zdeg,frqCpd,A,DC,bPLOT)

% function [W,Xdeg,Ydeg,Zdeg] = cosdRadialVolume(Xdeg,Ydeg,Zdeg,frqCpd,A,DC,bPLOT)
% 
%   example call: [Ydeg Xdeg Zdeg] = ndgrid(smpPos(32,32));
%                 W = cosdRadialVolume(Xdeg,Ydeg,Zdeg,5,1,0,1);
%
% radially symmetric volumn cosine with specified frequency 
%
% Xdeg:        x position in deg    [ 1 x nSmp ] OR [ nSmp x nSmp x nSmp ]
% Ydeg:        y position in deg    [ 1 x nSmp ] OR [ nSmp x nSmp x nSmp ]
% Zdeg:        z position in deg    [ 1 x nSmp ] OR [ nSmp x nSmp x nSmp ]
% frqCpd:      frequency in cycles per deg
% A:           amplitude 
% DC:          mean
% bPLOT:       plot or not
%              1 -> plot
%              0 -> not
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% W:           value of cosine

if ~exist('bPLOT') bPLOT = 0; end

if isvector(Xdeg) && isempty(Ydeg) && isempty(Zdeg)
   [Ydeg,Xdeg,Zdeg] = ndgrid(Xdeg);  
elseif isvector(Xdeg) && isempty(Ydeg) && isvector(Zdeg) 
   [Ydeg,Xdeg,Zdeg] = ndgrid(Xdeg,Xdeg,Zdeg);
elseif isvector(Xdeg) && isvector(Ydeg) && isvector(Zdeg) 
   [Ydeg,Xdeg,Zdeg] = ndgrid(Xdeg,Ydeg,Zdeg);
end

W = A.*cosd(360.*frqCpd.*sqrt(Xdeg.^2 + Ydeg.^2 + Zdeg.^2)) + DC; 

if bPLOT == 1
    figure('position',[320   378   680   420]); 
    subplot(1,2,1);
    ind =floor(size(W,3)./2+1);
    imagesc(Xdeg(1,:,ind),Ydeg(:,1,ind)',W(:,:,ind)); 
    formatFigure('X (deg)','Y (deg)',['Z=' num2str(Zdeg(1,1,ind),'%.2f')]);
    axis square;
    subplot(1,2,2);
    for i = 1:size(Zdeg,3)
    imagesc(Xdeg(1,:,i),Ydeg(:,1,i)',W(:,:,i)); 
    formatFigure('X (deg)','Y (deg)',['Z=' num2str(Zdeg(1,1,i),'%.2f')]);
    axis square;
    colormap gray
    pause(.05);
    end
end