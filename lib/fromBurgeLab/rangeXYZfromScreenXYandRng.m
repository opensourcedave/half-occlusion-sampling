function [Ixyz] = rangeXYZfromScreenXYandRng(Irng,IppXm,IppYm,screenZm)

% function [Ixyz] = rangeXYZfromScreenXYandRng(Irng,IppXm,IppYm,screenZm)
%
%   example call: rangeXYZfromScreenXYandRng(Irng,LsmpX,LsmpY,3.0)
%
% transforms positions of pixles in projection plane to 
% rangeXYZ by solving simultaneous parametric line equations 
% through nodal points and corresponding points
% 
%   WARNING: Idsp, IppXm, and IppYm MUST be in same coordinate system!!!
%
% Irng:      distance of point in scene d = sqrt(x^2 + y^2 + z^2)
% IppXm:     x-positions of pixels in screen/projection-plane (meters)
% IppYm:     y-positions of pixels in screen/projection-plane (meters)
% screenZm:  z-position  of screen
%%%%%%%%%%%%%%%%%%%%%%%
% Ixyz:   coordinates of 3D point in cartesian coordinates assuming

if numel(screenZm)==1
   screenZm=screenZm*ones(size(IppXm)); 
end

% SCALE VECTORS BY POINTS IN SCREEN: (1) NORMALIZE VECTOR, (2) SCALE VECTOR
Ixyz = repmat(Irng,[1 1 3]).*bsxfun(@rdivide,cat(3,IppXm,IppYm,screenZm),sqrt( IppXm.^2 + IppYm.^2 + screenZm.^2 ));
