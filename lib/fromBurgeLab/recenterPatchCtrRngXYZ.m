function [Pxyz,PxyzO,exitflag] = recenterPatchCtrRngXYZ(ItpRC,Cxyz,PszXY,bPlot)
exitflag=0;

%GET RANGE IN CARTESIAN FOR PATCH
PxyzO=zeros(PszXY(1),PszXY(2),3);
for l = 1:3
    try
        [PxyzO(:,:,l)]=cropImageCtr(Cxyz(:,:,l),ItpRC,PszXY,[]); %XXX interp?
    catch
        exitflag=1;
        return
    end
end

%DETERMINE HOW FAR TO SHIFT COORDINATE SYSTEM
ItpXYZ    = Cxyz(round(ItpRC(1)),round(ItpRC(2)),:); %X cartesian of new Ictr

%RECENTER XY COORDINATES TO PATCH CENTER
Pxyz(:,:,1)=PxyzO(:,:,1)-ItpXYZ(:,:,1);
Pxyz(:,:,2)=PxyzO(:,:,2)-ItpXYZ(:,:,2);
Pxyz(:,:,3)=PxyzO(:,:,3);

if bPlot==1
    figure(nFn)
    subplot(2,2,1)
    imagesc(Pxyz(:,:,1))
    axis square
    title('x range shifted')
    colorbar
    subplot(2,2,2)
    imagesc(PxyzO(:,:,1))
    axis square
    title('x range orignal')
    colorbar
    subplot(2,2,3)
    imagesc(Pxyz(:,:,2))
    axis square
    title('y range shifted')
    colorbar
    subplot(2,2,4)
    imagesc(PxyzO(:,:,2))
    axis square
    title('y range original')
    colorbar
end
