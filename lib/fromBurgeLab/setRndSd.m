function rndSd = setRndSd(rndSd)
%
% function setRndSd(rndSd)
% Set random seed to the desired integer value
% example call setRndSd(rndSd)

% INPUT HANDLING
if ~exist('rndSd','var') || isempty(rndSd) || ~(uint8(rndSd) == rndSd ), error('WARNING! setRndSd : Random seed should be an integer'); end

rng(rndSd,'v4');
