function PindRC = ctr2crd(ctrRC,PszXY)

% function PindRC = ctr2crd(ctrRC,PszXY)
%
%   example call: ctr2crd([65 65],[128 128])
%
%                 ctr2crd([17 65],[128 32])
%
% convert 'center' pixel location to 'coordinate' pixel location where
% the 'coordinate' pixel location is the upper left hand corner of patch
%
% ctrRC: indices of center in row/column          [row col]
% PszXY:    size of patch in pixels               [ x   y ]
% %%%%%%%%%%%%%%
% PindRC:   indices of upper/left corner of patch [row,col]
%
%       *** also see crd2ctr.m & cropImageCrd.m ***

% EASY TO READ, LESS FLEXIBLE COMPUTE
% PindRC = ctrRC - floor(fliplr(PszXY)./2);

% HARD TO READ, MORE FLEXIBLE COMPUTE
PindRC = bsxfun(@minus,ctrRC,floor(fliplr(PszXY)./2));
