function psyDatapixxClose(localHostName)

% function bUseDatapixx = psyDatapixxClose(localHostName)
%
%   example call: psyDatapixxClose()
%
% turns datapixx on for specified local host computer
%
% localHostName:    local host computer
%                   'jburge-helmholtz'  -> ProPixx  projector  computer
%                   'jburge-wheatstone' -> ViewPixx haploscope computer
%                   'jburge'            -> Johannes Burge Laptop (no vpixx)
%                     etc...


% INPUT HANDLING
if ~exist('localHostName','var') || isempty(localHostName) 
    localHostName = psyLocalHostName(); 
    disp(['psyDatapixxClose: WARNING! assuming the current computer is :' localHostName]);
end

%%%%%%%%%%%%%%%%%%
% CLOSE DATAPIXX %
%%%%%%%%%%%%%%%%%%
if strcmp(localHostName,'jburge-helmholtz')         % PROJECTOR
    
    Datapixx( 'Close' );
    
elseif strcmp(localHostName,'jburge-wheatstone')    % HAPLOSCOPE
    %% TURN DATAPIXX OFF
    Datapixx('Open');
    Datapixx('SelectDevice',2,'LEFT');      % SELECT LEFT  VIEWPIXX MONITOR
    Datapixx('SetVideoGreyscaleMode',0);    % TURN OFF CUSTOM GRAYSCALE MODE
    Datapixx('SelectDevice',2,'RIGHT');     % SELECT RIGHT VIEWPIXX MONITOR
    Datapixx('SetVideoGreyscaleMode',0);    % TURN OFF CUSTOM GRAYSCALE MODE
    Datapixx('SelectDevice',-1);            % NORMAL OPERATION
    Datapixx('RegWr');                      % WRITE 
    % CLOSE
    Datapixx('Close');                          
end