function [Ldvn,Rdvn] = loadLRSIimageDVN(imgNum,serverORlocal,bPLOT,customRootDir,customDVNdir)

% function [Ldvn,Rdvn] = loadLRSIimageDVN(imgNum,serverORlocal,bPLOT)
%
%   example call: % LOAD IMAGE 8  FROM SERVER
%                   [Ldvn,Rdvn] = loadLRSIimageDVN(8,'server',1);
%
%                 % LOAD IMAGE 21 FROM LOCAL DIRECTORY
%                   [Ldvn,Rdvn] = loadLRSIimageDVN(21,'local',1);
%
% loads LRSI half-occlusion image...
%
%        ***   see the following files  ***
%        ***    daVinciFromRangeXYZ.m   ***
%        *** daVinciFromRangeXYZbatch.m ***
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ldvn: left  eye image w. right-eye occlusion zones... [ r x c ] boolean
% Rdvn: right eye image w.  left-eye occlusion zones... [ r x c ] boolean
%       1 -> half-occluded
%       0 -> visible to both eyes
% customRootDir: specifies a custom root directory for LRSI2 database, excluding up to 'LRSI'
%             directory. Overrides local/server
%            [] -> no custom direcotry.

if ~exist('customDVNdir','var');    customRootDir=[];     end
if ~exist('customRootDir','var')==1; customRootDir='';  end
if ~exist('serverORlocal','var') || isempty(serverORlocal); serverORlocal = 'local'; end
if ~exist('bPLOT','var')         || isempty(bPLOT);         bPLOT         = 0;       end

% BUILD FILE DIRECTORY
if isempty(customDVNdir)
    fdir  = buildFolderName('LRSI','image','DVN',serverORlocal,customRootDir);
else
    fdir  = buildFolderName('LRSI','image',customDVNdir,serverORlocal,customRootDir);
end

% BUILD FILE NAME
fnameL = buildFilenameLRSIimageDVN(imgNum,'L');
fnameR = buildFilenameLRSIimageDVN(imgNum,'R');

% LOAD IMAGE
load([ fdir filesep fnameL ]);
load([ fdir filesep fnameR ]);

% CONVERT TO SINGLE TYPE
Ldvn = double(Ldvn);
Rdvn = double(Rdvn);

if bPLOT == 1
   figure('position',[158         78        1142         820])
   subplot(2,1,1);
   [Lpht,Rpht] = loadLRSIimage(imgNum,0,1,'PHT','img');
   imagesc(~[Ldvn Rdvn]);
   formatFigure([],[],['Img=' num2str(imgNum)]);
   set(gca,'xtick',[]); set(gca,'ytick',[]);
   axis image
   colormap gray

   subplot(2,1,2);
   imagesc(~[Ldvn Rdvn].*[Lpht Rpht].^.5)
   set(gca,'xtick',[]); set(gca,'ytick',[]);
   axis image
   colormap gray
end
