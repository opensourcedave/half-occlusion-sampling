function [Srgb LambdaNm maxAperture] = VRPsensitivity(LambdaMinMax,LambdaStep,bPLOT)

% function [Srgb LambdaNm maxAperture] = VRPsensitivity(LambdaMinMax,LambdaStep,bPLOT)
%
%   example call: VRPsensitivity([400 700],1,1);
% 
% RGB pixel sensitivities of the eSight VRP lens sensor (Sony IMX230)
% Measuremenets were obtained directly from Sony, but probably do not 
% contain the spectral transmittance of the lens 
%
% LambdaMinMax: min and max values for which to return sensitivities
%                   default: [400 700]
% LambdaStep:   spacing between successive sensitivity samples
%                   default: 5
% bPLOT:        1 -> plot
%               0 -> not
%
% NOTE: the values stored in this file were computed by calling 
%   >> [Srgb lambdaNm,~,~,~,~,maxAperture] = cameraCalibLoadData('D7R',2,0);
% 
% %%%%%%%%%%
% Srgb:        Raw wavelength sensitivities for the red, green, and blue pixels
%                When multiplied by the radiance at each wavelength the Srgb 
%                functions give 16bit pixel responses.  For some applications, 
%                is may be desirable to each channel to a max of one.
% lambdaNm:    Peak wavelength of the monochrometer illuminant at which 
%                a measure of sensitivity was obtained
% maxAperture: maximum aperture that accompanying camera lens can acheive
%
%                   *** see cameraCalibLoadData.m ***

if ~exist('LambdaMinMax','var') || isempty(LambdaMinMax)
   LambdaMinMax = [400 700]; 
end
if ~exist('LambdaStep','var') || isempty(LambdaStep)
   LambdaStep = 5; 
end
if ~exist('bPLOT','var') || isempty(bPLOT)
   bPLOT = 0; 
end

% MAXIMUM APERTURE OF D7H CAMERA LENS
maxAperture = 2.8;

% SENSITIVITY OF eSIGHT VARIOPTIC SONY LENS SENSOR
rawData(:,2:4) = [0.1154    0.0669    0.0024
    0.1309    0.0691    0.0025
    0.1380    0.0708    0.0076
    0.1418    0.0727    0.0178
    0.1434    0.0744    0.0394
    0.1480    0.0763    0.0809
    0.1527    0.0775    0.1497
    0.1486    0.0760    0.2406
    0.1372    0.0729    0.3406
    0.1204    0.0665    0.4276
    0.1032    0.0573    0.5043
    0.0865    0.0481    0.5715
    0.0724    0.0433    0.6095
    0.0602    0.0397    0.6195
    0.0504    0.0348    0.6450
    0.0422    0.0293    0.6869
    0.0365    0.0278    0.7208
    0.0323    0.0321    0.7606
    0.0292    0.0398    0.7984
    0.0255    0.0448    0.8188
    0.0223    0.0432    0.8357
    0.0202    0.0413    0.8448
    0.0187    0.0499    0.8436
    0.0204    0.0847    0.8348
    0.0237    0.1619    0.8195
    0.0306    0.3171    0.7961
    0.0380    0.5358    0.7639
    0.0411    0.7047    0.7200
    0.0428    0.8278    0.6508
    0.0457    0.8983    0.5771
    0.0498    0.9415    0.4998
    0.0549    0.9703    0.4257
    0.0614    0.9900    0.3540
    0.0707    0.9969    0.2858
    0.0840    0.9967    0.2285
    0.0957    0.9994    0.1894
    0.0968    0.9972    0.1642
    0.0880    0.9936    0.1476
    0.0754    0.9863    0.1300
    0.0668    0.9664    0.1153
    0.0636    0.9419    0.1008
    0.0644    0.9107    0.0886
    0.0670    0.8712    0.0788
    0.0707    0.8269    0.0725
    0.0846    0.7796    0.0687
    0.1603    0.7360    0.0672
    0.4077    0.6891    0.0682
    0.7010    0.6435    0.0694
    0.8434    0.5863    0.0681
    0.8867    0.5116    0.0638
    0.8930    0.4395    0.0583
    0.8769    0.3605    0.0526
    0.8586    0.2968    0.0475
    0.8414    0.2485    0.0448
    0.8169    0.2092    0.0428
    0.8086    0.1881    0.0438
    0.8023    0.1730    0.0459
    0.7820    0.1604    0.0484
    0.7746    0.1514    0.0525
    0.7662    0.1426    0.0571
    0.7363    0.1336    0.0615
    0.7149    0.1284    0.0667
    0.6975    0.1290    0.0727
    0.6622    0.1339    0.0781
    0.6262    0.1421    0.0816
    0.6032    0.1552    0.0855
    0.5728    0.1715    0.0896
    0.5396    0.1877    0.0920
    0.5277    0.2057    0.0953
    0.5277    0.2252    0.1009
    0.5315    0.2445    0.1060
    0.5369    0.2585    0.1069
    0.5496    0.2667    0.1053
    0.5577    0.2672    0.1021
    0.5597    0.2618    0.0980
    0.5566    0.2552    0.0943
    0.5518    0.2498    0.0914
    0.5427    0.2486    0.0897
    0.5272    0.2514    0.0904
    0.5195    0.2641    0.0952
    0.5124    0.2767    0.0992
    0.5002    0.2837    0.1012
    0.4869    0.2896    0.1041
    0.4700    0.2909    0.1091
    0.4654    0.2999    0.1208
    0.4617    0.3078    0.1361
    0.4508    0.3123    0.1588
    0.4429    0.3236    0.1969
    0.4342    0.3339    0.2488
    0.4366    0.3501    0.3145
    0.4394    0.3648    0.3693
    0.4330    0.3629    0.4005
    0.4281    0.3656    0.4220
    0.4207    0.3693    0.4280
    0.4093    0.3644    0.4218
    0.4037    0.3695    0.4227
    0.4037    0.3723    0.4221
    0.3916    0.3646    0.4071
    0.3814    0.3623    0.4018
    0.3736    0.3613    0.3994
    0.3549    0.3489    0.3817
    0.3394    0.3378    0.3664
    0.3363    0.3367    0.3664
    0.3246    0.3261    0.3562
    0.3070    0.3105    0.3370
    0.3043    0.3051    0.3316
    0.2967    0.3020    0.3254
    0.2753    0.2781    0.3009
    0.2637    0.2616    0.2859
    0.2619    0.2636    0.2930
    0.2514    0.2556    0.2858
    0.2344    0.2344    0.2574
    0.2268    0.2267    0.2427
    0.2222    0.2261    0.2467
    0.2155    0.2204    0.2415
    0.2060    0.2078    0.2224
    0.1989    0.2035    0.2193
    0.1965    0.2045    0.2234
    0.1860    0.1908    0.2031
    0.1679    0.1716    0.1765
    0.1607    0.1659    0.1716
    0.1621    0.1693    0.1787
    0.1588    0.1658    0.1762
    0.1435    0.1474    0.1575
    0.1301    0.1317    0.1435
    0.1260    0.1279    0.1398
    0.1219    0.1239    0.1340
    0.1126    0.1137    0.1216
    0.1001    0.0999    0.1066
    0.0925    0.0925    0.0996
    0.0878    0.0893    0.0978];

% PEAK OF MONOCHROMETER ILLUMINATION DISTRIBUTION
rawData(:,1) = [350
         355
         360
         365
         370
         375
         380
         385
         390
         395
         400
         405
         410
         415
         420
         425
         430
         435
         440
         445
         450
         455
         460
         465
         470
         475
         480
         485
         490
         495
         500
         505
         510
         515
         520
         525
         530
         535
         540
         545
         550
         555
         560
         565
         570
         575
         580
         585
         590
         595
         600
         605
         610
         615
         620
         625
         630
         635
         640
         645
         650
         655
         660
         665
         670
         675
         680
         685
         690
         695
         700
         705
         710
         715
         720
         725
         730
         735
         740
         745
         750
         755
         760
         765
         770
         775
         780
         785
         790
         795
         800
         805
         810
         815
         820
         825
         830
         835
         840
         845
         850
         855
         860
         865
         870
         875
         880
         885
         890
         895
         900
         905
         910
         915
         920
         925
         930
         935
         940
         945
         950
         955
         960
         965
         970
         975
         980
         985
         990
         995
        1000];

LambdaNm    = [LambdaMinMax(1):LambdaStep:LambdaMinMax(2)]';
Srgb = interp1(rawData(:,1),rawData(:,2:4),LambdaNm,'spline');

% LENS TRANSMISSIVENESS
lambdaLensRaw = [400:25:750]';
SlensRaw      = [78   88     92   94    94.5  94.5  95    95.5   96    95.5  50   10    2   1 0 ]';
Slens = interp1(lambdaLensRaw,SlensRaw,LambdaNm,'pchip');

Srgb = bsxfun(@times,Srgb,Slens)./100;
% CHECK FOR NEGATIVE INDICES
if ~isempty(find(Srgb < 0, 1))
    Srgb(Srgb < 0) = 0;
end

% WARN IF EXTRAPOLATING BEYOND LIMITS OF DATA
if min(LambdaMinMax) < min(rawData(:,1)) || max(LambdaMinMax) > max(rawData(:,1))
   disp(['VRPsensitivity: WARNING! extrapolating beyond measured data. minmax(rawdata) = [' num2str(min(rawData(:,1))) ' ' num2str(max(rawData(:,1))) '] vs LambdaMinMax [' num2str(LambdaMinMax(1)) ' ' num2str(LambdaMinMax(2)) ']']); 
end

if bPLOT
    figure; 
    plot(LambdaNm,Srgb(:,1)./max(Srgb(:,1)),'r',LambdaNm,Srgb(:,2)./max(Srgb(:,2)),'g',LambdaNm,Srgb(:,3)./max(Srgb(:,3)),'b','linewidth',2);
    formatFigure('Wavelength (nm)','Sensitivity','VRP');
    axis square
    ylim([0 1.1])
end