function fname = buildFilenameLRSIimage(imgNum,LorR,imgORrng)

% function fname = buildFilenameLRSIimage(imgNum,LorR,imgORrng)
%
%   example call: % FILENAME FOR LE IMAGE DATA
%                   buildFilenameLRSIimage(8,'L','img')
%
%                 % FILENAME FOR RE RANGE DATA
%                   buildFilenameLRSIimage(8,'R','rng')
%
% builds filename for loading stereo luminance range images
% 
% imgNum:    image number
% LorR:      specifies which eye to load image for
%            'L' -> left  eye image
%            'R' -> right eye image
% imgORrng:  which type of image to load
%            'img'  -> linear     RGB image
%                      good for analysis, not for viewing
%            'Vimg' -> non-linear RGB image w. gamma correction built in
%                      good for viewing, not for analysis
%            'rng' -> range image 
%                     polar distance from nodal of eye to scene point  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fname:     filename to load

% DEFAULT VALUE
if ~exist('LorR','var') || isempty(LorR) LorR = 'L'; end

% INPUT HANDLING
if ~strcmp(LorR,'L') & ~strcmp(LorR,'R') & ~strcmp(LorR,'l') & ~strcmp(LorR,'r') 
    error(['buildFilenameLRSIimage: WARNING! unhandled LorR value: ' LorR]);
end

if strcmp(imgORrng,'img')
    dataTypeStr = 'Image';
    fileTypeStr = '.png';
elseif strcmp(imgORrng,'Vimg')
    dataTypeStr = 'Image';
    fileTypeStr = 'V.png';
elseif strcmp(imgORrng,'rng')
    dataTypeStr = 'Range';
    fileTypeStr = '.mat';
else
    error(['buildFilenameLRSIimage: WARNING! unhandled imgORrng value: ' imgORrng]);
end

fname = [lower(LorR) dataTypeStr num2str(imgNum,'%03d') fileTypeStr];
