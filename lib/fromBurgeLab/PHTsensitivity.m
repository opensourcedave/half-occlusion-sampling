function [S Lnm] = PHTsensitivity(LnmMinMax,LnmStep,bPLOT)

% function [S Lnm] = PHTsensitivity(LnmMinMax,LnmStep,bPLOT)
%
%   example call: PHTsensitivity([400 700],1,1);
%
% Photopic sensitivity at 5 nm resolution in units of Energy (linear) 
% from Wyszecki and Stiles (1982), directly off of CVRL website 
%                       *** 2-deg curve ***
% 
% LnmMinMax: min and max values for which to return sensitivities
%                   default: [400 700]
% LnmStep:   spacing between successive sensitivity samples
%                   default: 5
%
% CIE Photopic V(?) modified by Judd (1951) and Vos (1978) 
%
% NOTES:  The standard photopic luminosity function is based on a curious 
% combination of luminosity data from several sources and obtained by 
% several methods (Gibson & Tyndall, 1923; CIE, 1926). The uncertainty 
% surrounding it is illustrated by the fact that the values from the 
% different studies that were averaged to define it diverged by as much 
% as a factor of ten in the violet (CIE, 1926; Le Grand, 1968). The 
% function seriously underestimates sensitivity at short wavelengths.
%
% http://www-cvrl.ucsd.edu/

if ~exist('LnmMinMax','var') || isempty(LnmMinMax)  LnmMinMax = [400 700]; end
if ~exist('LnmStep','var')   || isempty(LnmStep)    LnmStep = 5;           end
if ~exist('bPLOT','var')     || isempty(bPLOT)      bPLOT = 0;             end

rawData = [
    380	2.00E-04
    385	3.96E-04
    390	8.00E-04
    395	1.55E-03
    400	2.80E-03
    405	4.66E-03
    410	7.40E-03
    415	1.18E-02
    420	1.75E-02
    425	2.27E-02
    430	2.73E-02
    435	3.26E-02
    440	3.79E-02
    445	4.24E-02
    450	4.68E-02
    455	5.21E-02
    460	6.00E-02
    465	7.29E-02
    470	9.10E-02
    475	1.13E-01
    480	1.39E-01
    485	1.70E-01
    490	2.08E-01
    495	2.58E-01
    500	3.23E-01
    505	4.05E-01
    510	5.03E-01
    515	6.08E-01
    520	7.10E-01
    525	7.95E-01
    530	8.62E-01
    535	9.15E-01
    540	9.54E-01
    545	9.80E-01
    550	9.95E-01
    555	1.00E+00
    560	9.95E-01
    565	9.79E-01
    570	9.52E-01
    575	9.16E-01
    580	8.70E-01
    585	8.16E-01
    590	7.57E-01
    595	6.95E-01
    600	6.31E-01
    605	5.67E-01
    610	5.03E-01
    615	4.42E-01
    620	3.81E-01
    625	3.21E-01
    630	2.65E-01
    635	2.17E-01
    640	1.75E-01
    645	1.38E-01
    650	1.07E-01
    655	8.17E-02
    660	6.10E-02
    665	4.43E-02
    670	3.20E-02
    675	2.35E-02
    680	1.70E-02
    685	1.19E-02
    690	8.21E-03
    695	5.77E-03
    700	4.10E-03
    705	2.93E-03
    710	2.09E-03
    715	1.48E-03
    720	1.05E-03
    725	7.40E-04
    730	5.20E-04
    735	3.61E-04
    740	2.49E-04
    745	1.72E-04
    750	1.20E-04
    755	8.46E-05
    760	6.00E-05
    765	4.24E-05
    770	3.00E-05
    775	2.12E-05
    780	1.50E-05
    785	1.06E-05
    790	7.47E-06
    795	5.26E-06
    800	3.70E-06
    805	2.61E-06
    810	1.84E-06
    815	1.30E-06
    820	9.11E-07
    825	6.36E-07];

Lnm = [LnmMinMax(1):LnmStep:LnmMinMax(2)]';
S   = interp1(rawData(:,1),rawData(:,2),Lnm,'spline');

% CHECK FOR NEGATIVE INDICES
if ~isempty(find(S < 0, 1))
    S(S < 0) = 0;
end

% WARN IF EXTRAPOLATING BEYOND LIMITS OF DATA
if min(LnmMinMax) < min(rawData(:,1)) || max(LnmMinMax) > max(rawData(:,1))
   disp(['PHTsensitivity: WARNING! extrapolating beyond measured data. minmax(rawdata) = [' num2str(min(rawData(:,1))) ' ' num2str(max(rawData(:,1))) '] vs LnmMinMax [' num2str(LnmMinMax(1)) ' ' num2str(LnmMinMax(2)) ']']); 
end

if bPLOT
    figure; 
    plot(Lnm,S(:,1),'k','linewidth',2);
    formatFigure('Wavelength (nm)','S','PHT');
    axis square
    ylim([0 1.1])
end