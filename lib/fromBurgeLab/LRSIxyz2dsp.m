function [Ldsp,Rdsp] = LRSIxyz2dsp(Lxyz,Rxyz,dspType,PctrRC,bQuiet)
% function [Ldsp,Rdsp] = LRSIpatchBIrng2dsp(Lrng,Rrng,dspType)
% Example call: [Ldsp,Rdsp] = LRSIpatchBIrng2dsp(Lrng,Rrng,'center')
%
% TAKES XYZ COORDINATES FOR LE AND RE AND RETURNS LE & RE DISPARITY MAPS
%
% INPUT ARGUMENTS
% Lxyz       : LE xyz coordinates
% Rxyz       : RE xyz coordinates
% dspType    : Options to create disparity map from vergence map by...
%             'mean'   : Subtracting mean vergence of patch
% PctrRC     : Patch center coordinates (row and column)
%
% OUTPUT ARGUMENTS
% Ldsp       : LE disparity map
% Rdsp       : RE disparity map

% DATA HANDLING

if exist('bQuiet','var')~=1 || isempty(bQuiet)
    bQuiet=0;
end
if ~isequal(size(Lxyz),size(Rxyz)), error('WARNING! LRSIxyz2dsp.m : Lxyz and Rxyz should of identical dimensions.'); end
if ~exist('dspType',  'var') || isempty(dspType), dspType = 'center' ; end                                                                                 % default to subtracting center pixel vergence demand
if ~(strcmp(dspType,'center')||strcmp(dspType,'mean')), error('WARNING! LRSIxyz2dsp.m : Invalid dspType'); end
if strcmp(dspType,'center') && (~exist('PctrRC',  'var') || isempty(PctrRC)), PszRC = fliplr([size(Lxyz,1) size(Lxyz,2)]); PctrRC = floor(PszRC./2+1); end % if center coordinates were not provided when needed

tic

% OBTAIN VERGENCE MAPS
ind = 1:size(Lxyz,4);
Lvrg = 60.*vergenceFromRangeXYZ('L',LRSIcameraIPD(),Lxyz(:,:,:,ind));
Rvrg = 60.*vergenceFromRangeXYZ('R',LRSIcameraIPD(),Rxyz(:,:,:,ind));

if strcmp(dspType,'center')
    % CONSTRUCT DISPARITY MAPS BY SUBTRACTING OFF CENTER-PIXEL VERGENCE
    Ldsp = bsxfun(@minus,Lvrg(:,:,ind),Lvrg(PctrRC(1),PctrRC(2),ind));
    Rdsp = bsxfun(@minus,Rvrg(:,:,ind),Rvrg(PctrRC(1),PctrRC(2),ind));
elseif strcmp(dspType,'mean')
    % CONSTRUCT DISPARITY MAPS BY SUBTRACTING OFF MEAN VERGENCE
    Ldsp = bsxfun(@minus,Lvrg(:,:,ind),mean(mean(Lvrg(:,:,ind))));
    Rdsp = bsxfun(@minus,Rvrg(:,:,ind),mean(mean(Rvrg(:,:,ind))));
end

if bQuiet==0
    toc
    disp(['LRSIxyz2dsp.m: Successfully created ' num2str(ind(end)) ' LE & RE disparity maps subtracting ' dspType ' vergence demand of patches']);
end
