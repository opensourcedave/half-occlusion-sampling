function S = structSingle2Double(S)

%function S = structSingle2Double(S)
% 
% S       : input structure with possibly some single precision fields
% S       : output structure where numberical field elements have double precision

fieldNames = fieldnames(S);

for i = 1:length(fieldNames)
    % FIELD VALUES
    F = S.(fieldNames{i});
    
    if isnumeric(F)
        S.(fieldNames{i}) = double(F);
    end
 
end
