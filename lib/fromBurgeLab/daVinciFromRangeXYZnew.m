function [Idvn,Idg,Igd,Ibnd,IdvnOLD] = daVinciFromRangeXYZnew(LorR,IPDm,Ixyz,DGcutoffs,bPLOT,bPLOTdebug,Ipht,PctrXY,PszXY)

% function [Idvn,Idg,Igd,Ibnd,IdvnOLD] = daVinciFromRangeXYZnew(LorR,IPDm,Ixyz,DGcutoffs,bPLOT,bPLOTdebug,Ipht,PctrXY,PszXY)
%
%   example call: % POINTS EXCEEDING DISPARITY GRADIENT = 2.0 IN LE IMAGE (half-occlusions) 
%                   daVinciFromRangeXYZnew('L',0.065,Lxyz,2.0,1);
%
%                 % POINTS EXCEEDING DISPARITY GRADIENT = 2.0 IN RE IMAGE (half-occlusions) 
%                   daVinciFromRangeXYZnew('R',0.065,Rxyz,2.0,1;)
%  
%                 % POINTS EXCEEDING EACH OF SEVERAL DISPARITY GRADIENT CUTOFFS 
%                   daVinciFromRangeXYZnew('L',0.065,Lxyz,1.0:.25:2.0)
%
%                 % POINTS EXCEEDING CUTOFF FROM SAMPLED PATCHES
%                   daVinciFromRangeXYZnew(S.LorR(c),S.IPDm,S.LxyzCrp(:,:,:,c),2.0,1);                      
%
% points exceeding a disparity gradient cutoff by computing the
% disparity gradient point n-neighbors. diff(Vergence)/diff(Version)
%
% this calculation is performed by taking the difference in visual angle
% between n-neighbors and dividing by the visual angle between them
% 
% a brief explanation of n-neighbors
%   if n == 1, DG is computed for next   door neighbors
%   if n == 2, DG is computed for second door neighbors
%   if n == 3, DG is computed for third  door neighbors
%
% LorR:      which image to use as a reference
%            'L' -> left  image as reference
%            'R' -> right image as reference
% IPDm:      inter-pupillary (i.e. inter-nodal point) distance in meters
% Ixyz:      range xyz w/ coord system center at LorR eye  [ n x m x 3 ]
%            'X' -> x position in meters
%            'Y' -> y position in meters
%            'Z' -> z position in meters
% DGcutoffs: vector of disparity gradient cutoffs used to select points
%            important DG cutoff values
%             1  -> fusion   limit (approx)
%             2  -> half-occlusion 
% bPLOT:      1  -> plot
%             0  -> not
% %%%%%%%%%%%%%%%%%%%%%%
% Idvn:      array of logicals that is size of input image [ n x m ]
%            indicating whether pixel is above or below cutoff
%            if DGcutoff = 2.0, 
%            1 -> half-occluded, 
%            0 -> not half-occluded
% Idg:       cell array of length(DGcutoffs) w. col vectors of indices for points avoe    each value in DGcutoffs
% Igd:       cell array of length(DGcutoffs) w. col vectors of indices for points below   each value in DGcutoffs  
%            NOTE: 'dg' -> points that exceed critical (d)isparity (g)radient 
%                  'gd' -> points that should o.k... i.e. they are (g)oo(d) 
% Ibnd:      cell array of length(DGcutoffs) w. col vectors of indices for points between each value in DGcutoffs


if ~exist('bPLOT','var')      || isempty(bPLOT),      bPLOT = 0;      end
if ~exist('bPLOTdebug','var') || isempty(bPLOTdebug), bPLOTdebug = 0; end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERGENCE AND VERSION ANGLES %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VERGENCE ANGLE (i.e. ABSOLUTE DISPARITY ASSUMING INFINITE GAZE)
IvrgDeg = vergenceFromRangeXYZ(LorR,IPDm,Ixyz);
% VERSION  ANGLE OF POINT IN EPIPOLAR PLANE
IvrsDeg = versionFromRangeXYZ(LorR,IPDm,Ixyz);
% disp(['daVinciFromRangeXYZnew: WARNING! testing versionFromRangeXYZ with cyclopean direction']);
% MAX PIXEL SHIFTS TO CHECK
maxPixShifts = min([75 max(size(Ixyz(:,:,1)))]); 

% PREALLOCATE MEMORY: POINTS EXCEEDING DGcutoffs (dgc)
Idff = zeros([size(IvrgDeg) maxPixShifts]);
for i = 1:maxPixShifts, 
    if strcmp(LorR,'L')
        %% DIFFERENCE KERNEL WITH DIFFERENENT SPATIAL SEPARATIONS
        Kdff = [1 zeros(1,i-1) -1 zeros(1,i)]; % NOTE!!! CONVOLUTION FLIPS KERNELS SO KERNEL IS REALLY [zeros(1,i) -1 zeros(1,i-1) 1];
        % DISPARITY GRADIENT FOR N-NEIGHBORS (DG = ?disparity/?cyclopeanDirection)  
        IvrgDegGx   = conv2(IvrgDeg,  Kdff,'same');
        IvrsDegGx   = conv2(IvrsDeg,  Kdff,'same');
        Idff(:,:,i)  = IvrgDegGx./IvrsDegGx; 
    elseif strcmp(LorR,'R')
        % DIFFERENCE KERNEL WITH DIFFERENENT SPATIAL SEPARATIONS
        Kdff = [zeros(1,i) -1 zeros(1,i-1) 1]; % NOTE!!! CONVOLUTION FLIPS KERNELS SO KERNEL IS REALLY [1 zeros(1,i-1) -1 zeros(1,i)];
        % DISPARITY GRADIENT FOR N-NEIGHBORS (DG = ?disparity/?cyclopeanDirection) 
        IvrgDegGx   = conv2(IvrgDeg, Kdff,'same');
        IvrsDegGx   = conv2(IvrsDeg,-Kdff,'same');
        Idff(:,:,i)  = IvrgDegGx./IvrsDegGx; 
    end
    % HANDLE BOUNDARY CONDITIONS
    if     strcmp(LorR,'L') indBdC = (i+1):size(IvrsDeg,2); ind = 1:(numel(IvrsDeg)-i*size(IvrsDeg,1));
    elseif strcmp(LorR,'R') indBdC = 1:i;                   ind = (i+1)*size(IvrsDeg,1):numel(IvrsDeg);
    end
    % Idff(:,indBdC,i) = 0;
    % BREAK OUT OF LOOP IF NO POINTS EXCEED CUTOFF
    if sum(sum(Idff(:,:,i) > min(DGcutoffs))) == 0 && i > 20 % min(IvrsDegGxR(ind))>0 % && i > 20 % && min(IvrsDegGxR(:)>0) %
        Idff(:,:,i:end)  = []; 
        i = i-1;
        break; 
    end
end

if i == maxPixShifts
   disp(['daVinciFromRangeXYZnew: WARNING! consider increasing maxPixShifts. maxPixShifts == ' num2str(maxPixShifts) ' and i=' num2str(i)]);
end
%%
% FIND POINTS EXCEEDING EACH DISPARITY GRADIENT CUTOFF
for d = 1:length(DGcutoffs)
    % ROWS AND COLUMNS OF LOCATIONS EXCEEDING CUTOFF FOR ALL N-NEIGHBORS
    [R,C,s] = ind2sub([size(IvrgDeg)],find(abs(Idff)>DGcutoffs(d)));
    % COLLAPSE TO A SINGLE
    RC    = unique([R C],'rows');
    % INDICES OF LOCATIONS ABOVE CUTOFF % (if DG = 2.0, da Vinci points)
    Idg{d} = sub2ind(size(IvrgDeg),RC(:,1),RC(:,2));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%% 1 == HALF-OCCLUDED POINT
    % CREATE DVN POINT IMAGE % 0 == BINOCULARLY VISIBLE POINT
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    if d == 1
        Idvn = false([size(IvrgDeg) length(DGcutoffs)]); 
    end
    Idvn(Idg{d}+(d-1).*numel(IvrgDeg)) = true;
    % INDICES OF LOCATIONS BELOW CUTOFF % (if DG = 2.0, binocularly visible points)
    if nargout > 2, Igd{d}=setxor(Idg{d},[1:numel(IvrgDeg)]'); end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % INDICES OF LOCATIONS WITHIN DG RANGE (i.e. BAND) % ( DG(i-1) and DG(i) 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargout > 3
        if d ==1, Ibnd{d} = Igd{d};
        else      Ibnd{d} = setxor(Idg{d-1}, Idg{d});
        end
    end
end

%%
if bPLOTdebug
    %%
   PctrRC = fliplr(PctrXY);
    PszRC  = fliplr(PszXY);
    ctrXY  = ceil((PszXY+1)./2);
    ctrRC  = fliplr(ctrXY);
    
    % CROP IMAGE TO PLOT PATCH
    IdvnCrp  = cropImageCtr(Idvn,round(PctrRC),[PszXY]);
    IphtCrp  = cropImageCtr(Ipht,round(PctrRC),[PszXY]);
    IvrsCrp  = cropImageCtr(IvrsDeg,round(PctrRC),[PszXY]);
    IvrgCrp  = cropImageCtr(IvrgDeg,round(PctrRC),[PszXY]);
    IxyzCrp  = cropImageCtr(Ixyz,round(PctrRC),[PszXY]);
    IdspCrp  = IvrgCrp - IvrgCrp(ctrRC(1),ctrRC(2));
    IdffCrp  = cropImageCtr(Idff,round(PctrRC),[PszXY]);
    
    figure('position',[ 560   313   999   635]);
    pix = [1:PszRC(2)]-ctrRC(2);
    for i = 1:length(pix);
        subplot(4,2,[1 3 5]); hold on; plot(  log2( abs(squeeze(Idff(PctrRC(1),PctrRC(2)+pix(i),:)))')); axis square; formatFigure(['\DeltaX'],'log2( DG )',['Idff: PctrRC=[' num2str(PctrRC(1)) ' ' num2str(PctrRC(2)) ']' ]); ylim([-2 12]); xlim([0 size(Idff,3)+1]);
        subplot(4,2,[7]);     hold on; stem(pix(i), Idvn(PctrRC(1),PctrRC(2)+pix(i)),'k'); formatFigure('Pixel Shift','\DeltaX'); xlim( minmax(pix)+[-1 1]); 
        pause(.01);
    end
    subplot(4,2,[1 3 5]); plot(xlim,[1 1],'k--'); % legend(legendLabel('X=',pix(1:i)));
    subplot(4,2,[2 4 6]); imagesc(pix,1:size(Idff,3),log2(abs(squeeze(Idff(PctrRC(1),PctrRC(2)+pix,:)))')>1); formatFigure('Pixel Shift','\DeltaX'); axis square; axis xy;
    subplot(4,2,[8]);     stem(pix, Idvn(PctrRC(1),PctrRC(2)+pix),'k'); formatFigure('Pixle Shift','DVN'); xlim( minmax(pix)+[-1 1]);     
    drawnow
    %
    figure('position',[400    56   618   900]);
    subplot(3,2,1);
    imagesc([IphtCrp.*~IdvnCrp].^.45); hold on
    colormap gray; axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'yx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'dvn=' num2str(IdvnCrp(ctrRC(1),ctrRC(2))) ]); cax = caxis;
    subplot(3,2,3);
    imagesc([IphtCrp].^.45); hold on
    colormap gray; axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'yx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'pht']); caxis(cax);
    h=subplot(3,2,5);
    imagesc([IdspCrp].*60); hold on; caxis([-15 15])
    colormap(h,cmapBWR); axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR]);
    h=subplot(3,2,2);
    imagesc([IvrgCrp]); hold on;
    colormap(h,jet); axis image; axis ij
    h=colorbar; set(h,'position',[ 0.9140    0.7111    0.0174    0.2100])
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'vrg']);
    
    h=subplot(3,2,4);
    imagesc([IvrsCrp ]); hold on;
    colormap(h,jet); axis image; axis ij
    h=colorbar; set(h,'position',[0.9149    0.4111    0.0174    0.2111])
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'vrs']);
    
    subplot(3,2,6); cla; hold on;
    if strcmp(LorR,'L')     indCut = ctrRC(2):PszXY(1);
    elseif strcmp(LorR,'R') indCut = 1:ctrRC(2);
    end
    xvalIdff  = [1:size(Idff,3)]+ctrXY(1)-1; % X VALUES FOR Idff
    plot(xvalIdff,abs(squeeze(Idff(PctrRC(1),PctrRC(2),:))),'k')
    plot(IdvnCrp(ctrRC(1),:),'r','linewidth',2);
    plot(2.*IdspCrp(ctrRC(1),:),'b','linewidth',2);
    plot(ctrRC(2),0,'kx')
    plot(xlim,[2 2],'k--',xlim,[-2 -2],'k--');
    ylim([-5 5]);
    xlim([0 PszXY(1)]);
    legend({'Idff','Idff2','Idvn','Idsp'});
    axis square
  
    drawnow;
end

if bPLOT == 1
    %%
   if ~exist('Idvn','var') || isempty(Idvn)
       if d == 1
        Idvn = false([size(Ixyz(:,:,1)) length(DGcutoffs)]); 
        end
        Idvn(Idg{d}+(d-1).*numel(Ixyz(:,:,1))) = true;
   end
   figure('position',[5 391 1676 955]); 
   if ~exist('Ipht','var') || isempty(Ipht)
   imagesc(~Idvn); 
   else
    imagesc((~Idvn).*Ipht.^.4);
   end
   formatFigure([],[],'Half-occluded points')
   axis image; colormap gray
end

%% LOOP TO REMOVE SPURIOUS DA VINCI POINTS
if strcmp(LorR,'L')
    for r = 1:size(Idvn,1)
        % TRANSITION POINTS
        %% 
        [indChg,runVal] = runLengthEncoder(Idvn(r,:));
        for c = 1:length(indChg)-1
            if runVal(c) == 1
                % LABELED HALF-OCCLUDED ZONE
                indZon = indChg(c):(indChg(c+1)-1);
                % DELTAX AT LAST PIXEL IN OCCLUDED ZONE
%                 indChkEnd = [squeeze(abs(Idff(r,indZon(max([1 numel(indZon)])),1:length(indZon))))>DGcutoffs(d)]';
%                 indNll = indChg(c):(indChg(c)+length(indChkEnd)-1);
%                 Idvn(r,indNll)=Idvn(r,indNll).*indChkEnd;
                indChkBgn = [squeeze(abs(Idff(r,indZon(  1),:)))>DGcutoffs(d)]';
                indChkBgn = indChkBgn(1:find(indChkBgn==1,1,'last'));
                indDeltaX = [ max([1 (length(indChkBgn)-length(indZon)+1)]):length(indChkBgn) ];
                indChkEnd = [squeeze(abs(Idff(r,indZon(end),indDeltaX)))>DGcutoffs(d)]';
                indNll = indChg(c):(indChg(c)+length(indChkEnd)-1);
                try
                if r == PctrXY(2) 
                    if indChg(c) == PctrXY(1)
                   %%
                       figure; set(gcf,'position',[888   454   484   884]);
                       subplot(5,1,[1:3]); imagesc(indZon-indZon(1),1:size(Idff,3),[squeeze(abs(Idff(r,indZon,:)))>DGcutoffs(d)]'); axis xy; axis square; formatFigure('Pixel Shift','\DeltaX'); axis square;  axis xy;
                       subplot(5,1,   4 ); stem(indChkBgn,'k'); formatFigure([],[],'indChkBgn'); subplot(5,1,   5 ); stem(indChkEnd,'k'); formatFigure([],[],'indChkEnd');                   
                       killer = 1;
                    end
                end
                end
                Idvn(r,indNll)=Idvn(r,indNll).*indChkEnd; 
            end
        end
    end
elseif strcmp(LorR,'R')
    for r = 1:size(Idvn,1)
        IdvnFLP = fliplr(Idvn(r,:));
        IdffFLP = fliplr(Idff(r,:,:));
        % TRANSITION POINTS
        [indChg,runVal] = runLengthEncoder(IdvnFLP);
        for c = 1:length(indChg)-1
            if runVal(c) == 1
                indZon = indChg(c):(indChg(c+1)-1);
% STAT OLD CODE                
%                 try
%                     indChk = [squeeze(abs(IdffFLP(1,indZon(max([1 numel(indZon)])),      1:length(indZon))))>DGcutoffs(d)]';
%                 catch
%                     killer =1;
%                 end
%                 [ind,val,cnt] = runLengthEncoder(indChk);
%                 % figure(22222); imagesc(indZon-indZon(1)+1,1:length(indZon),log2(abs(squeeze(IdffFLP(1,indZon,1:length(indZon))))')>1); formatFigure('Pixel Shift','\DeltaX'); axis square;  axis xy;
%                 indLst = find(val==1,1,'last');
%                 indNll = indChg(c):indChg(c)+length(indChk)-1;
%                 IdvnFLP(1,indNll) =  IdvnFLP(1,indNll).*indChk;
% END OLD CODE 
                indChkBgn = [squeeze(abs(IdffFLP(1,indZon(  1),:)))>DGcutoffs(d)]';
                indChkBgn = indChkBgn(1:find(indChkBgn==1,1,'last'));
                indDeltaX = [ max([1 (length(indChkBgn)-length(indZon)+1)]):length(indChkBgn) ];
                indChkEnd = [squeeze(abs(IdffFLP(1,indZon(end),indDeltaX)))>DGcutoffs(d)]';
                indNll = indChg(c):(indChg(c)+length(indChkEnd)-1);
                IdvnFLP(1,indNll) =  IdvnFLP(1,indNll).*indChkEnd;
            end
        end
        Idvn(r,:)=fliplr(IdvnFLP);
    end
end


if bPLOTdebug == 1
    %%
    PctrRC = fliplr(PctrXY);
    PszRC  = fliplr(PszXY);
    ctrXY  = ceil((PszXY+1)./2);
    ctrRC  = fliplr(ctrXY);
    
    IdvnCrp  = cropImageCtr(Idvn,round(PctrRC),[PszXY]);
    IphtCrp  = cropImageCtr(Ipht,round(PctrRC),[PszXY]);
    IvrsCrp  = cropImageCtr(IvrsDeg,round(PctrRC),[PszXY]);
    IvrgCrp  = cropImageCtr(IvrgDeg,round(PctrRC),[PszXY]);
    IxyzCrp  = cropImageCtr(Ixyz,round(PctrRC),[PszXY]);
    IdspCrp  = IvrgCrp - IvrgCrp(ctrRC(1),ctrRC(2));
    IdffCrp  = cropImageCtr(Idff,round(PctrRC),[PszXY]);
    
%     figure('position',[ 560   313   999   635]);
%     pix = [1:PszRC(2)]-ctrRC(2);
%     for i = 1:length(pix);
%         subplot(4,2,[1 3 5]); hold on; plot(  log2( abs(squeeze(Idff(PctrRC(1),PctrRC(2)+pix(i),:)))')); axis square; formatFigure(['\DeltaX'],'log2( DG )',['Idff: PctrRC=[' num2str(PctrRC(1)) ' ' num2str(PctrRC(2)) ']' ]); ylim([-2 12]); xlim([0 size(Idff,3)+1]);
%         subplot(4,2,[7]);     hold on; stem(pix(i), Idvn(PctrRC(1),PctrRC(2)+pix(i)),'k'); formatFigure('Pixel Shift','\DeltaX'); xlim( minmax(pix)+[-1 1]); 
%         pause(.01);
%     end
%     subplot(4,2,[1 3 5]); plot(xlim,[1 1],'k--'); % legend(legendLabel('X=',pix(1:i)));
%     subplot(4,2,[2 4 6]); imagesc(pix,1:size(Idff,3),log2(abs(squeeze(Idff(PctrRC(1),PctrRC(2)+pix,:)))')>1); formatFigure('Pixel Shift','\DeltaX'); axis square; axis xy;
%     subplot(4,2,[8]);     stem(pix, Idvn(PctrRC(1),PctrRC(2)+pix),'k'); formatFigure('Pixle Shift','DVN'); xlim( minmax(pix)+[-1 1]);     
%     drawnow
    %
    figure('position',[400    56   618   900]);
    subplot(3,2,1);
    imagesc([IphtCrp.*~IdvnCrp].^.45); hold on
    colormap gray; axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'yx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'dvn=' num2str(IdvnCrp(ctrRC(1),ctrRC(2))) ]); cax = caxis;
    subplot(3,2,3);
    imagesc([IphtCrp].^.45); hold on
    colormap gray; axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'yx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'pht']); caxis(cax);
    h=subplot(3,2,5);
    imagesc([IdspCrp].*60); hold on; caxis([-15 15])
    colormap(h,cmapBWR); axis image; axis ij
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR]);
    h=subplot(3,2,2);
    imagesc([IvrgCrp]); hold on;
    colormap(h,jet); axis image; axis ij
    h=colorbar; set(h,'position',[ 0.9140    0.7111    0.0174    0.2100])
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'vrg']);
    
    h=subplot(3,2,4);
    imagesc([IvrsCrp ]); hold on;
    colormap(h,jet); axis image; axis ij
    h=colorbar; set(h,'position',[0.9149    0.4111    0.0174    0.2111])
    plot(ctrXY(1),ctrXY(2),'cx','linewidth',2.5,'markersize',15);
    formatFigure([],[],[LorR 'vrs']);
    
    subplot(3,2,6); cla; hold on;
    if strcmp(LorR,'L')     indCut = ctrRC(2):PszXY(1);
    elseif strcmp(LorR,'R') indCut = 1:ctrRC(2);
    end
    xvalIdff  = [1:size(Idff,3)]+ctrXY(1)-1; % X VALUES FOR Idff
    plot(xvalIdff,abs(squeeze(Idff(PctrRC(1),PctrRC(2),:))),'k')
    plot(IdvnCrp(ctrRC(1),:),'r','linewidth',2);
    plot(2.*IdspCrp(ctrRC(1),:),'b','linewidth',2);
    plot(ctrRC(2),0,'kx')
    plot(xlim,[2 2],'k--',xlim,[-2 -2],'k--');
    ylim([-5 5]);
    xlim([0 PszXY(1)]);
    legend({'Idff','Idff2','Idvn','Idsp'});
    axis square
  
    drawnow;
    
    %     figure; hold on;
%     for i = 1:2:size(IxyzCrp,2)
%         plot([-IPDm/2 squeeze(IxyzCrp(ctrRC(1),i,1))],[0 squeeze(IxyzCrp(ctrRC(1),i,3))],'b-','linewidth',.25);
%         plot([+IPDm/2 squeeze(IxyzCrp(ctrRC(1),i,1))],[0 squeeze(IxyzCrp(ctrRC(1),i,3))],'r-','linewidth',.25);
%         plot(squeeze(IxyzCrp(ctrRC(1),i,1)),squeeze(IxyzCrp(ctrRC(1),i,3)),'ko','markerface','w','markersize',12);
%         
%     end
%     ind = IdvnCrp(ctrRC(1),:);
%     plot(squeeze(IxyzCrp(ctrRC(1),ind,1)),squeeze(IxyzCrp(ctrRC(1),ind,3)),'kx')
end

if bPLOT == 1
    %%
   if ~exist('Idvn','var') || isempty(Idvn)
       if d == 1
        Idvn = false([size(Ixyz(:,:,1)) length(DGcutoffs)]); 
       end
        Idvn(Idg{d}+(d-1).*numel(Ixyz(:,:,1))) = true;
   end
   figure('position',[5 391 1676 955]); 
   if ~exist('Ipht','var') || isempty(Ipht)
   imagesc(~Idvn); 
   else
    imagesc((~Idvn).*Ipht.^.4);
   end
   formatFigure([],[],'Half-occluded points')
   axis image; colormap gray
end


%%
% DGcutoff = 2.0; 
% IPDm=LRSIcameraIPD();
% for i = 0:98, 
%     i
%     [~,~,~,~,Lxyz,Rxyz]=loadLRSIimage(i); 
%     [Ldvn] = daVinciFromRangeXYZnew('L',IPDm,Lxyz,DGcutoff,0,0); 
%     [Rdvn] = daVinciFromRangeXYZnew('R',IPDm,Rxyz,DGcutoff,0,0); 
%     saveLRSIimagePRJ( ['DVN_' 'L' '_' num2str(i,'%03d') '.mat'],'DVN','local',1,Ldvn,'Ldvn');
%     saveLRSIimagePRJ( ['DVN_' 'R' '_' num2str(i,'%03d') '.mat'],'DVN','local',1,Rdvn,'Rdvn');
% end