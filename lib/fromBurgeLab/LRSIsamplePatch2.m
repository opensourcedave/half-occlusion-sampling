function [LphtCrp,LrngCrp,LxyzCrp,RphtCrp,RrngCrp,RxyzCrp,LdvnCrp,RdvnCrp,LitpRC,RitpRC,LitpRCchk,RitpRCchk]=LRSIsamplePatch2(imgNum,LorR,IctrRC,PszXY,dspArcMin,calibType,interpType,dnK,bPLOT,Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,imgType,bQuiet)

%XXX%new function L eye and R eye centers, return cropped luminance, range, rangecartesian
    %watch return order - follow loadlrsiimage
% function [LitpRC,RitpRC,LitpRCchk,RitpRCchk,RitpShftXpix,LphtCrp,LrngCrp,LxyzCrp,RphtCrp,RrngCrp,RxyzCrp,LdvnDstPix,RdvnDstPix,LfixDstM,RfixDstM,CsmpErrDeg,DvrgDffDeg,vrgErrItpArcSec]=LRSIsamplePatch(imgNum,natORflt,LorR,IctrRC,PszXY,dspArcMin,calibType,interpType,dnK,bPLOT,wallORcross,Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm)
%
%   example call: [LitpRC,RitpRC,LitpRCchk,RitpRCchk,LphtCrp LrngCrp LxyzCrp RphtCrp RrngCrp RxyzCrp]=LRSIsamplePatch(62,'NAT','L',[628 1048],[320 320],0,'PHT','linear',1,1);
%                 [LitpRC,RitpRC,LitpRCchk,RitpRCchk,LphtCrp LrngCrp LxyzCrp RphtCrp RrngCrp RxyzCrp]=LRSIsamplePatch(22,'NAT','L',[321 1297],[320 320],0,'PHT','linear',1,1);
%                 LRSIsamplePatch(21,'NAT','L',[321 1297],[320 320],    0,'PHT','linear',1,1);
%                 LRSIsamplePatch(40,'NAT','L',[641  945],[128 128]*2.5,0,'PHT','linear',1,1);
%                 LRSIsamplePatch(44,'NAT','L',[539  945],[128 128]*2.5,0,'PHT','linear',1,1);
%                 LRSIsamplePatch(46,'NAT','L',[539  945],[128 128]    ,0,'PHT','linear',1,1);
%                 LRSIsamplePatch(52,'NAT','L',[669 1245],[128 128]    ,0,'PHT','linear',1,1);
%
% imgNum:       image number
%                i  -> loads image data if valid scalar (valid nums:[0:98])
%               [ ] -> must input image data (for batch processing)
% IctrRC:       left image patch center ( row, col )  [ 1 x 2 ]
% PszXY:        patch size in x and y                 [ 1 x 2 ]
% dspArcMin:    disparity in arcmin
% calibType:    image calibration type
%                 'RGB'  -> linear red, green, blue channels (default)
%                 'PHT'  -> photopic luminance
% interpType:   interpolation type
%                 'none'   -> interpolation not performed sampled 3D point used raw
%                 'linear' -> linear interpolation         (default)
%                 'spline' -> spline interpolation... not recommended
% bPLOT:        1 -> plot
%               0 -> not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LitpRC:    LE row column index of interpolated patch
% RitpRC:    RE row column index of interpolated patch
% LitpRCchk: LE row column index of interpolated patch w. other eye as anchor
% LitpRCchk: RE row column index of interpolated patch w. other eye as anchor
% LphtCrp:   cropped left  RGB   image
% LrngCrp:   cropped left  range image (radial distance)
% LxyzCrp:   cropped left  catersian coordinates
% RphtCrp:   cropped right RGB   image
% RrngCrp:   cropped right range image
% RxyzCrp:   cropped right catersian coordinates a
% LdvnPix:   distance

if ~exist('bQuiet','var')      || isempty(bQuiet);      bQuiet      =        0; end
if ~exist('dspArcMin','var')   || isempty(dspArcMin);   dspArcMin   =        0; end
if ~exist('interpType','var')  || isempty(interpType);  interpType  = 'linear'; end
if ~exist('bPLOT','var')       || isempty(bPLOT);       bPLOT       =        0; end
if ~exist('imgType','var')     || isempty(imgType);     imgType     =    'img'; end

% SETUP
units = 1; % meters

% LOAD LUM RANGE STEREO IMAGE
if ~isempty(imgNum) && imgNum >= 0 && imgNum <= 98
    disp(['LRSIsamplePatch: loading imgNum ' num2str(imgNum)]);
    [Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(imgNum,dnK,1,calibType,imgType);
end
LppZm = LRSIprojPlaneDist(units);
RppZm = LRSIprojPlaneDist(units);
IPDm  = LRSIcameraIPD(units);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISPARITY IN PIXELS FROM ARCMIN %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(LorR,'L')
    degPerPix = diff(atand(LppXm(IctrRC(1),IctrRC(1):(IctrRC(1)+1))./LppZm));                         % degrees scaled for local  position
elseif strcmp(LorR,'R')
    degPerPix = diff(atand(RppXm(IctrRC(1),(IctrRC(1)-1):IctrRC(1))./RppZm));                         % degrees scaled for local  position
end
% DISPARITY IN PIXELS
dspPix   =  dspArcMin ./( 60.*degPerPix );
LdspPix  = -dspPix/2; % WARNING! NOT EXACTLY CORRECT
RdspPix  = +dspPix/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GET CORRESPONDING POINT %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
if     strcmp(LorR,'L')
    % FROM LE POINT GET CORRESPONDING BEST SAMPLED RE POINT AND INTERPOLATION TO REDUCE SAMPLING ERROR
    [LitpRC,   RitpRC        ] = LRSIcorrespondingPointL2R(      IctrRC, Lxyz,Rxyz,LppXm,LppYm,0); % row/col
    try [RitpRCchk,LitpRCchk ] = LRSIcorrespondingPointR2L(round(RitpRC),Rxyz,Lxyz,RppXm,RppYm,0); catch, LitpRCchk = [NaN NaN]; RitpRCchk = [NaN NaN]; end % row/col
elseif strcmp(LorR,'R')
    % FROM RE POINT GET CORRESPONDING BEST SAMPLED LE POINT AND INTERPOLATION TO REDUCE SAMPLING ERROR
    [RitpRC,   LitpRC,       ] = LRSIcorrespondingPointR2L(      IctrRC, Rxyz,Lxyz,RppXm,RppYm,0); % row/col
    try [LitpRCchk,RitpRCchk ] = LRSIcorrespondingPointL2R(round(LitpRC),Lxyz,Rxyz,LppXm,LppYm,0); catch, LitpRCchk = [NaN NaN]; RitpRCchk = [NaN NaN]; end % row/col
else; error(['LRSIsamplePatch: WARNING! unhandled LorR value: ' LorR]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CROP IMAGE W. DESIRED DISPARITY %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for d = 1:length(dspArcMin)
    LphtCrp(:,:,:,d) = cropImageCtrInterp(Lpht, LitpRC+[0 LdspPix(d)],PszXY,interpType);
    RphtCrp(:,:,:,d) = cropImageCtrInterp(Rpht, RitpRC+[0 RdspPix(d)],PszXY,interpType);
    LxyzCrp(:,:,:,d) = cropImageCtrInterp(Lxyz, LitpRC+[0 LdspPix(d)],PszXY,interpType);
    RxyzCrp(:,:,:,d) = cropImageCtrInterp(Rxyz, RitpRC+[0 RdspPix(d)],PszXY,interpType);
    LrngCrp(:,:,:,d) = cropImageCtrInterp(Lrng, LitpRC+[0 LdspPix(d)],PszXY,interpType);
    RrngCrp(:,:,:,d) = cropImageCtrInterp(Rrng, RitpRC+[0 RdspPix(d)],PszXY,interpType);
    %% %%%%%%%%%%%%%%%%
    % QUALITY CONTROL % ENSURE CENTER PIXEL FOR DSP == 0 POINT NOT HALF-OCCLUDED
    %%%%%%%%%%%%%%%%%%%
    if dspArcMin(d) == 0
        PctrRC       = floor(PszXY([2 1])/2+1);
        % OBTAIN XYZ COORDINATES OF LE AND RE CENTER PIXELS
        LxyzCrpCtr   = squeeze(LxyzCrp(PctrRC(1),PctrRC(2),:,d))'              ; % XYZ at left  eye corresponding point
        RxyzCrpCtr   = squeeze(RxyzCrp(PctrRC(1),PctrRC(2),:,d))' + [+IPDm 0 0]; % XYZ at right eye corresponding point (now in LE coordinates)
        % VERGENCE ANGLES FROM INTERPOLATED LE AND RE  POINTS (NOTE: Lxyz & Rxyz IN COORDINATE SYSTEM CENTERED ON LE & RE, RESPECTIVELY)
        LvrgDegCtr   = vergenceFromRangeXYZ('L',IPDm,LxyzCrpCtr); % vergence angle between idealized points (assumes no da vinci)
        RvrgDegCtr   = vergenceFromRangeXYZ('L',IPDm,RxyzCrpCtr); % vergence angle between idealized points (assumes no da vinci)
        % DIFFERENCE IN ANGLE BETWEEN THEORETICAL AND EMPIRICAL VERGENCE ANGLES
        vrgErrItpArcSec = (LvrgDegCtr-RvrgDegCtr)*3600;

        maxVrgErrItpArcSec = 5;
        if abs(vrgErrItpArcSec) > maxVrgErrItpArcSec && bQuiet == 0
           disp(['LRSIsamplePatch: WARNING! discrepancy between vergence angles at interpolated LE and RE corresponding points exceeds ' num2str(maxVrgErrItpArcSec) ' arcsec: ' num2str(vrgErrItpArcSec,'%.2f') 'arcsec...']);
        end

        % LOCATIONS OF HALF-OCCLUDED POINTS AT dspArcMin = 0
        DG = 2; % disparity gradient = 2.0
        [~,~,~,LdvnCrp] = daVinciFromRangeXYZ('L',IPDm,LxyzCrp(:,:,:,dspArcMin==0),DG,0);
        [~,~,~,RdvnCrp] = daVinciFromRangeXYZ('R',IPDm,RxyzCrp(:,:,:,dspArcMin==0),DG,0);
    end
end

if bPLOT
    IPDm = LRSIcameraIPD;
    PctrRC = [floor(PszXY(2)/2+1),floor(PszXY(1)/2+1)];
    LdspCrp = distance2disp(LrngCrp,LrngCrp(PctrRC(1),PctrRC(2)),IPDm,1);
    RdspCrp = distance2disp(RrngCrp,RrngCrp(PctrRC(1),PctrRC(2)),IPDm,1);
    %%
    for d = 1:length(dspArcMin)
    plotLRSIsamplePatch(LphtCrp(:,:,:,d),RphtCrp(:,:,:,d),LdspCrp(:,:,:,d),RdspCrp(:,:,:,d),LxyzCrp(:,:,:,d),RxyzCrp(:,:,:,d),IPDm,'wall');
    end
end
