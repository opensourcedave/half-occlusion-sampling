function [Sensitivity LambdaNm] = XYZsensitivity(LambdaMinMax,LambdaStep,bPLOT)

% function [Sensitivity LambdaNm] = XYZsensitivity(LambdaMinMax,LambdaStep,bPLOT)
%
% XYZ fundamentals at 1 nm resolution in units of Energy (linear) 
% from Stockman & Sharpe (2000) off of CVRL website. 1931 CIE 2-deg 
% XYZ color matching functions modified by Judd (1951) and Vos (1978) 
%
% LambdaMinMax: min and max values for which to return sensitivities
%                   default: [400 700]
% LambdaStep:   spacing between successive sensitivity samples
%                   default: 5
%
% Notes: In 1978, Vos made additional corrections to Judd's (1951) 
% revision of the CIE 1931 2 deg color matching functions (CIE, 1932) and 
% incorporated the infra-red color reversal described by Brindley (1955). 
% The Judd-Vos V(?) is the modified luminosity function VM(?) recently 
% adopted by the CIE.
%
% From Table 1 of Vos(1978). Also see Table I(5.5.2) of W&S (1982)
%
% References: 
% Judd, D. B. (1951). Report of U.S. Secretariat Committee on Colorimetry 
% and Artificial Daylight. In Proceedings of the Twelfth Session of the 
% CIE, Stockholm (vol. 1, pp. 11). Paris: Bureau Central de la CIE.
% 
% Vos, J. J. (1978). Colorimetric and photometric properties of a 2-deg 
% fundamental observer. Color Research and Application, 3, 125-128.
% 
% Wyszecki, G., & Stiles, W. S. (1982). Color Science: concepts and
% methods, quantitative data and formulae. (2nd ed.). New York: Wiley.
% 
% http://www.cvrl.org/


if ~exist('LambdaMinMax','var') || isempty(LambdaMinMax)
   LambdaMinMax = [400 700]; 
end
if ~exist('LambdaStep','var') || isempty(LambdaStep)
   LambdaStep = 5; 
end
if ~exist('bPLOT','var') || isempty(bPLOT)
    bPLOT = 0;
end
% rawData = [lambdaNm X Y Z]
rawData = ...
[380	2.69E-03	2.00E-04	1.23E-02
385	5.31E-03	3.96E-04	2.42E-02
390	1.08E-02	8.00E-04	4.93E-02
395	2.08E-02	1.55E-03	9.51E-02
400	3.80E-02	2.80E-03	1.74E-01
405	6.32E-02	4.66E-03	2.90E-01
410	9.99E-02	7.40E-03	4.61E-01
415	1.58E-01	1.18E-02	7.32E-01
420	2.29E-01	1.75E-02	1.07E+00
425	2.81E-01	2.27E-02	1.31E+00
430	3.11E-01	2.73E-02	1.47E+00
435	3.31E-01	3.26E-02	1.58E+00
440	3.33E-01	3.79E-02	1.62E+00
445	3.17E-01	4.24E-02	1.57E+00
450	2.89E-01	4.68E-02	1.47E+00
455	2.60E-01	5.21E-02	1.37E+00
460	2.33E-01	6.00E-02	1.29E+00
465	2.10E-01	7.29E-02	1.24E+00
470	1.75E-01	9.10E-02	1.11E+00
475	1.33E-01	1.13E-01	9.42E-01
480	9.19E-02	1.39E-01	7.56E-01
485	5.70E-02	1.70E-01	5.86E-01
490	3.17E-02	2.08E-01	4.47E-01
495	1.46E-02	2.58E-01	3.41E-01
500	4.85E-03	3.23E-01	2.64E-01
505	2.32E-03	4.05E-01	2.06E-01
510	9.29E-03	5.03E-01	1.54E-01
515	2.93E-02	6.08E-01	1.09E-01
520	6.38E-02	7.10E-01	7.66E-02
525	1.11E-01	7.95E-01	5.62E-02
530	1.67E-01	8.62E-01	4.14E-02
535	2.28E-01	9.15E-01	2.94E-02
540	2.93E-01	9.54E-01	2.00E-02
545	3.62E-01	9.80E-01	1.33E-02
550	4.36E-01	9.95E-01	8.78E-03
555	5.15E-01	1.00E+00	5.86E-03
560	5.97E-01	9.95E-01	4.05E-03
565	6.81E-01	9.79E-01	2.92E-03
570	7.64E-01	9.52E-01	2.28E-03
575	8.44E-01	9.16E-01	1.97E-03
580	9.16E-01	8.70E-01	1.81E-03
585	9.77E-01	8.16E-01	1.54E-03
590	1.02E+00	7.57E-01	1.23E-03
595	1.05E+00	6.95E-01	1.12E-03
600	1.06E+00	6.31E-01	9.06E-04
605	1.04E+00	5.67E-01	6.95E-04
610	9.92E-01	5.03E-01	4.29E-04
615	9.29E-01	4.42E-01	3.18E-04
620	8.43E-01	3.81E-01	2.56E-04
625	7.40E-01	3.21E-01	1.57E-04
630	6.33E-01	2.65E-01	9.77E-05
635	5.34E-01	2.17E-01	6.89E-05
640	4.41E-01	1.75E-01	5.12E-05
645	3.55E-01	1.38E-01	3.60E-05
650	2.79E-01	1.07E-01	2.42E-05
655	2.15E-01	8.17E-02	1.69E-05
660	1.62E-01	6.10E-02	1.19E-05
665	1.18E-01	4.43E-02	8.15E-06
670	8.58E-02	3.20E-02	5.60E-06
675	6.31E-02	2.35E-02	3.95E-06
680	4.58E-02	1.70E-02	2.79E-06
685	3.21E-02	1.19E-02	1.92E-06
690	2.22E-02	8.21E-03	1.31E-06
695	1.56E-02	5.77E-03	9.15E-07
700	1.11E-02	4.10E-03	6.48E-07
705	7.92E-03	2.93E-03	4.64E-07
710	5.65E-03	2.09E-03	3.33E-07
715	4.00E-03	1.48E-03	2.38E-07
720	2.83E-03	1.05E-03	1.70E-07
725	1.99E-03	7.40E-04	1.22E-07
730	1.40E-03	5.20E-04	8.71E-08
735	9.70E-04	3.61E-04	6.15E-08
740	6.68E-04	2.49E-04	4.32E-08];

LambdaNm    = [LambdaMinMax(1):LambdaStep:LambdaMinMax(2)]';
Sensitivity = interp1(rawData(:,1),rawData(:,2:4),LambdaNm,'spline');

% CHECK FOR NEGATIVE INDICES
if ~isempty(find(Sensitivity < 0, 1))
    Sensitivity(Sensitivity < 0) = 0;
end

% WARN IF EXTRAPOLATING BEYOND LIMITS OF DATA
if min(LambdaMinMax) < min(rawData(:,1)) || max(LambdaMinMax) > max(rawData(:,1))
   disp(['XYZsensitivity: WARNING! extrapolating beyond measured data. minmax(rawdata) = [' num2str(min(rawData(:,1))) ' ' num2str(max(rawData(:,1))) '] vs LambdaMinMax [' num2str(LambdaMinMax(1)) ' ' num2str(LambdaMinMax(2)) ']']); 
end


if bPLOT
    figure; 
    plot(LambdaNm,Sensitivity(:,1),'r',LambdaNm,Sensitivity(:,2),'g',LambdaNm,Sensitivity(:,3),'b','linewidth',2);
    formatFigure('Wavelength (nm)','Sensitivity','XYZ');
    axis square
    ylim([0 2])
end