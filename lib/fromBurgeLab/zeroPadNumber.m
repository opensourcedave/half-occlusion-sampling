function numStringPadded = zeroPadNumber(fNumber,numStringLength)

% function numStringPadded = zeroPadNumber(fNumber,numStringLength)
%
%   example call: zeroPadNumber(1,3)
%
% convert number to string with zero padding for desired length
%
% fNumber:         number to be zero padded
% numStringLength: length of zero padded number string
%%%%%%%%%%%%%%%%%
% numStringPadded: zero padded number string

if 10^numStringLength - fNumber < 0
   disp(['zeroPadNumber: WARNING! fNumber, ' num2str(fNumber) ', is greater than numStringLength, ' num2str(numStringLength)]);
   disp(['               Desired num string length impossible']);
   disp([' ']);
end

if (fNumber < 10)
    zeroPadNumber = num2str(zeros(1,numStringLength-1));
elseif (fNumber >= 10    && fNumber < 100)
    zeroPadNumber = num2str(zeros(1,numStringLength-2));
elseif (fNumber >= 100   && fNumber < 1000)
    zeroPadNumber = num2str(zeros(1,numStringLength-3));
elseif (fNumber >= 1000  && fNumber < 10000)
    zeroPadNumber = num2str(zeros(1,numStringLength-4));
elseif (fNumber >= 10000 && fNumber < 100000)
    zeroPadNumber = num2str(zeros(1,numStringLength-5));
else
    error(['filenameNumberAppend: unhandled fNumber ' num2str(fNumber) ])
end
zeroPadNumber(findstr(zeroPadNumber,' ')) = [];

numStringPadded = [zeroPadNumber num2str(fNumber)];