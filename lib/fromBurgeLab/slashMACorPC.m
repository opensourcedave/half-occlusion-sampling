function slash = slashMACorPC

% function slash = slashMACorPC
% 
% get operating system dependent 'slash' for building file names

if ismac
    slash = '/';
else
    slash = '\';
end
