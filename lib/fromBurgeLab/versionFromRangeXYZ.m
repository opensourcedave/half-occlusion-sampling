function [CdirDeg] = versionFromRangeXYZ(LorR,IPDm,Ixyz,bPLOT)

% function [CdirDeg] = versionFromRangeXYZ(LorR,IPDm,Ixyz,bPLOT)
%
%   example call: versionFromRangeXYZ('L',0.065,Lxyz,bPLOT)
%
% version angle (i.e. visual direction from cyclopean eye) in the epipolar
% plane defined by range map XYZ given the range values (XYZ), the 
% reference eye (L or R), and an IPD in meters
%
% NOTE! if LorR -> 'L' Ixyz should have a coordinate system centered at the LE nodal point
%       if LorR -> 'R' Ixyz should have a coordinate system centered at the RE nodal point
%       if LorR -> 'C' Ixyz should have a coordinate system centered at the CE nodal point
%
% LorR:      left or right image used as reference
%            'L' -> left      image used   as reference
%            'R' -> right     image used   as reference
%            'C' -> cyclopean image 
% IPDm:      inter-ocular distance to simulate
% Ixyz:      range data in cartesian coordinates [ n x m x 3 ]
%            I(:,:,1) -> x-values
%            I(:,:,2) -> y-values
%            I(:,:,3) -> z-values
% bPLOT:     plot or not
%            1 -> plot
%            0 -> not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CdirDeg:   version angle to point
%
% see also 

if size(Ixyz,3) ~= 3, error(['versionFromRangeXYZ: WARNING! size(Ixyz,3) = ' num2str(size(Ixyz,3)) ' instead of 3']); end
if ~exist('bPLOT','var') || isempty(bPLOT) bPLOT = 0; end

if     strcmp(LorR,'L') % COORD SYSTEM CENTER -> LEFT EYE
  % CYCLOPEAN ANGLE (VISUAL DIRECTION) IN EPIPOLAR PLANE TO EACH XYZ POINT: asin( rng /  )  
    CdirDeg = asind( (Ixyz(:,:,1)-IPDm/2) ./ sqrt(sum(bsxfun(@minus,Ixyz,reshape([ IPDm/2 0 0],[1 1 3])).^2,3)) );
elseif strcmp(LorR,'C') % COORD SYSTEM CENTER -> CYCLOPEAN EYE
  % CYCLOPEAN ANGLE (VISUAL DIRECTION) IN EPIPOLAR PLANE TO EACH XYZ POINT 
    CdirDeg = asind( (Ixyz(:,:,1)       ) ./ sqrt(sum(bsxfun(@minus,Ixyz,reshape([      0 0 0],[1 1 3])).^2,3)) );
elseif strcmp(LorR,'R') % COORD SYSTEM CENTER -> RIGHT EYE
  % CYCLOPEAN ANGLE (VISUAL DIRECTION) IN EPIPOLAR PLANE TO EACH XYZ POINT 
    CdirDeg = asind( (Ixyz(:,:,1)+IPDm/2) ./ sqrt(sum(bsxfun(@minus,Ixyz,reshape([-IPDm/2 0 0],[1 1 3])).^2,3)) );
else
    error(['vergenceAngleFromRangeXYZ: WARNING! unhandled LorR string: ' LorR])
end

if bPLOT
   figure; imagesc(CdirDeg); 
   axis image;
   caxis(max(abs(caxis))*[-.5 .5]); 
   colormap(cmapBWR(256)); 
end

