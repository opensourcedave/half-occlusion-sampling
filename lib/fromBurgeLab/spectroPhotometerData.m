function [WavelengthNm Spectrum Luminance Radiance LambdaPeakNm rawData XYZ xyzGamut] = spectroPhotometerData(fdir,fname,lambdaNmRange,lambdaNmSpacing,interpMethod)

% function [WavelengthNm Spectrum Luminance Radiance LambdaPeakNm rawData XYZ xyzGamut] = spectroPhotometerData(fdir,fname,lambdaNmRange,lambdaNmSpacing,interpMethod)
%
%   example calls: spectroPhotometerData(['..' slashMACorPC 'Project_CameraCalibration' slashMACorPC 'D7HCAL' slashMACorPC] ,'D7H570V1.TXT',[400 700],2)
%
%                  spectroPhotometerData(['ThreeMonitorCalib' slashMACorPC 'Spectrophotometer' slashMACorPC] ,'M1B100.TXT',[400 700],2)  
% 
% extract radiance (watts/ster*m^2, lambda at peak energy, wavelength in 
% WavelengthNm at which Spectrum is collected and the Spectrum
% 
% fdir:            directory where spectrophotometer files are stored          
% fname:           individual file names                                    
% lambdaNmRange:   range of sampled wavelengths (nanometers)                
% lambdaNmSpacing: spacing between sampled wavelengths (nanometers)                 
% interpMethod:    used only if lamdaNmRange OR lambdaNmSpacing are non-empty        
%                  'linear' (default)
%                  'spline'...
%%%%%%%%%%%%%%%%%%%%%%%%%
% WavelengthNm: wavelengths at which samples are taken [nx1 vector]
% Spectrum:     energy at the sampled wavelengths      [nx1 vector]
% Luminance:    luminance in cd / m^2                  [scalar]
% Radiance:     radiance  in watts / ster * m^2        [scalar]
% LambdaPeakNm: wavelength at max(Spectrum)
% rawData:      array of WavelengthNm and Spectrum     [nx2 vector]
% XYZ:          X,Y,Z color matching function values   [1x3 vector]
% xyzGamut:     xyz chromaticitiy coordinates
%
%      (rawData is output only for old programs... delete eventually)
%
%                 *** see files named cameraCalib*.m ***


%% OPEN EACH FILE
fid = fopen([fdir fname],'r');
if fid == -1
    error(['spectroPhotometerData: WARNING! invalid file name or path: ' [fdir fname]]);
end
% SCROLL THROUGH FILE ONE LINE AT A TIME
line = 1;
tline = 'start search';
while ischar(tline)
    tline = fgetl(fid); 
    line = line+1;
    % LINE 24 STORES THE INTEGRAL OF THE TOTAL RADIANCE
    if line == 24
        Radiance  = fscanf(fid,'%f');         % total energy
    end
    if line == 30
        Luminance = fscanf(fid,'%f');         % total energy
    end
    if line == 33
        XYZ = fscanf(fid,'%f %f %f');         % total energy
    end
    if line == 34
        xyzGamut = fscanf(fid,'%f %f %f');         % total energy
    end
    % LINE 46-END STORES THE ENERGY MEASURED AT EACH WAVELENGTH
    if line >= 46
        line;
        try
            rawData(line-45,:) = fscanf(fid,'%f',2)'; % C1=Wavelength, C2=Radiance at that Wavelength
        catch
            break
        end
    end
end
fclose(fid);

% UNPACK RAWDATA
WavelengthNm = rawData(:,1);
Spectrum     = rawData(:,2);
% PEAK WAVELENGTH (max wavelength)
LambdaPeakNm(:,1) = rawData(find(Spectrum == max(Spectrum),1),1);
% LambdaPeakNm(:,1) = sum(WavelengthNm.*(Spectrum./sum(Spectrum)));

% TRUNCATE AND/OR INTERP VALUES AT DESIRED LOCATIONS
if (~exist('lambdaNmRange','var') || isempty(lambdaNmRange)) && (~exist('lambdaNmSpacing','var') || isempty(lambdaNmSpacing))
    return;
else
    if ~exist('lambdaNmRange','var') || isempty(lambdaNmRange)
        lambdaNmRange = minmax(WavelengthNm);
    end
    if ~exist('lambdaNmSpacing','var') || isempty(lambdaNmSpacing)
        lambdaNmSpacing = floor(mean(diff(WavelengthNm(:))));
    end
    if ~exist('interpMethod','var') || isempty(interpMethod)
        interpMethod = 'linear';
    end
    WavelengthNmNEW = min(lambdaNmRange):lambdaNmSpacing:max(lambdaNmRange);
    Spectrum = interp1(WavelengthNm,Spectrum,WavelengthNmNEW,interpMethod);
    WavelengthNm = WavelengthNmNEW;
end
killer = 1;

