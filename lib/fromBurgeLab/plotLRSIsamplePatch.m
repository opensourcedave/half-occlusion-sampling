function plotLRSIsamplePatch(LphtCrp,RphtCrp,LdspCrp,RdspCrp,LxyzCrp,RxyzCrp,IPDm,wallORcross,figh,imgNum,vrgErrItpArcSec,i,IctrRC,LorR)

% function plotLRSIsamplePatch(LphtCrp,RphtCrp,LdspCrp,RdspCrp,LxyzCrp,RxyzCrp,IPDm,wallORcross,figh,imgNum,vrgErrItpArcSec,i,IctrRC,LorR)
%
%   example call: plotLRSIsamplePatch(S.LphtCrp,S.RphtCrp,S.LdspCrp,S.RdspCrp,S.LxyzCrp,S.RxyzCrp,S.IPDm,'wall') 
%
% plot sampled binocular image patch with known disparity
% 
% LphtCrp:  see  ***   LRSIsamplePatch.m   ***
% RphtCrp:  
% LdspCrp:  
% RdspCrp:  
% LxyzCrp:  
% RxyzCrp:  
% IPDm:            interocular distance in meters
% wallORcross:     'wall'  -> for wall eyed fusing
%                  'cross' -> for cross-eyed fusing
% figh:            figure handle, may not be functional
% imgNum:          image number 
% vrgErrItpArcSec: vergence angle difference (vrgR2L - vrgL2R) 
% i:               patch index
% IctrRC:          patch center in the anchor eye
% LorR:            anchor eye 
%                  'L' -> left  eye is anchor eye
%                  'R' -> right eye is anchor eye



% OPEN FIGURE IF NECESSARY
if ~exist('figh','var') || isempty(figh)
   figure; figh = gcf; 
   set(figh,'position',[400 200 600 920]);
end


% GET PATCH SIZE AND CENTER COORDINATES
PszRC   = size(LphtCrp);
PszXY   = fliplr(PszRC);
PctrRC  = floor(PszRC./2 + 1);
PctrXY  = fliplr(PctrRC);
PctrInd = sub2ind( PszRC, PctrRC(1), PctrRC(2) );

% [Ldvn]=daVinciFromRangeXYZnew('L',IPDm,LxyzCrp,2.0,0);
% [Rdvn]=daVinciFromRangeXYZnew('R',IPDm,RxyzCrp,2.0,0);
[~,~,~,Ldvn]=daVinciFromRangeXYZ('L',IPDm,LxyzCrp,2.0,0);
[~,~,~,Rdvn]=daVinciFromRangeXYZ('R',IPDm,RxyzCrp,2.0,0);
LdvnCrp = bwdist(Ldvn);
RdvnCrp = bwdist(Rdvn);
LdvnDstPix = LdvnCrp(PctrInd);
RdvnDstPix = RdvnCrp(PctrInd);

% WINDOW FOR COMPUTING STATISTICS
W100 = cosWindow(PszRC,1);

%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT LUMINANCE IMAGE
%%%%%%%%%%%%%%%%%%%%%%%%
subplot(3,1,1);
if     strcmp(wallORcross,'wall')
    imagesc( [ LphtCrp RphtCrp].^.4 );
elseif strcmp(wallORcross,'cross')
    imagesc( [ RphtCrp LphtCrp].^.4 );
end
axis image; hold on; 
plotSquare(floor(PszXY./2+1),             [8 8],'y',1);  
plotSquare(floor(PszXY./2+1)+[PszXY(1) 0],[8 8],'y',1); 
plot(      floor(PszXY./2+1),              floor(PszXY./2+1),'y.'); 
plot(      floor(PszXY./2+1)+[PszXY(1) 0], floor(PszXY./2+1),'y.'); 
colormap(gca, gray(256));
try
formatFigure([],[],['Img=' num2str(imgNum) ', Patch=' num2str(i) ', \theta_{err}=' num2str(vrgErrItpArcSec,'%.2f') 'arcsec']); axis off; 
catch
formatFigure([],[],[wallORcross ': dstDv=' num2str(min([LdvnDstPix RdvnDstPix]),'%.1f') 'pix']); axis off; 
end
% truesize; 

%%%%%%%%%%%%%%%%%%%%%%%%
%% PLOT DISPARITY IMAGE
%%%%%%%%%%%%%%%%%%%%%%%%
% ABSOLUTE DISPARITY (ASSUMING INFINITE FIXATION)
IPDm = LRSIcameraIPD;
LdspCrp  = 60.*vergenceFromRangeXYZ('L',IPDm,LxyzCrp);
RdspCrp  = 60.*vergenceFromRangeXYZ('R',IPDm,RxyzCrp);

subplot(3,1,2);
if     strcmp(wallORcross,'wall')
    imagesc( [LdspCrp - LdspCrp(PctrRC(1),PctrRC(2)) RdspCrp - RdspCrp(PctrRC(1),PctrRC(2))] ); 
elseif strcmp(wallORcross,'cross')
    imagesc( [RdspCrp - RdspCrp(PctrRC(1),PctrRC(2)) LdspCrp - LdspCrp(PctrRC(1),PctrRC(2)) ] ); 
end
axis image; hold on; 
plotSquare(floor(PszXY./2+1),             [8 8],'k',1);  
plotSquare(floor(PszXY./2+1)+[PszXY(1) 0],[8 8],'k',1); 
plot(      floor(PszXY./2+1),             floor(PszXY./2+1),'k.'); 
plot(      floor(PszXY./2+1)+[PszXY(1) 0],floor(PszXY./2+1),'k.'); 
caxis([-10 10]);
colormap(gca, cmapBWR);
hcb = colorbar;
set(hcb,'position',[0.8583    0.4105    0.0333    0.2073])
try
% formatFigure([],[],['\rho_{lum}=' num2str(S.rhoPht100(i),'%.2f') ', \rho_{dsp}=' num2str(S.rhoDsp100(i),'%.2f') ', fDst_L=' num2str(S.LfixDstM(i),'%.1f') ', fDst_R=' num2str(S.RfixDstM(i),'%.1f') ]); axis off; 
formatFigure([],[],['\rho_{lum}=' num2str(S.rhoPht100(i),'%.2f') ', \rho_{dsp}=' num2str(S.rhoDsp100(i),'%.2f') ', fDst_L=' num2str(S.LfixDstM(i),'%.1f') ', fDst_R=' num2str(S.RfixDstM(i),'%.1f') ]); axis off; 
catch
formatFigure([],[],[]); axis off; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOT HALF-OCCLUDED POINTS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(3,1,3);
if     strcmp(wallORcross,'wall')
    imagesc( [~Ldvn ~Rdvn].*[LphtCrp RphtCrp].^.4 ); 
elseif strcmp(wallORcross,'cross')
    imagesc( [~Rdvn ~Ldvn].*[RphtCrp LphtCrp].^.4 ); 
end

axis image; hold on; 
plotSquare(floor(PszXY./2+1),             [8 8],'k',1);  
plotSquare(floor(PszXY./2+1)+[PszXY(1) 0],[8 8],'k',1); 
plot(      floor(PszXY./2+1),             floor(PszXY./2+1),'k.'); 
plot(      floor(PszXY./2+1)+[PszXY(1) 0],floor(PszXY./2+1),'k.'); 
colormap(gca, gray);
% caxis([0 1]);
box on
formatFigure([],[],['LdvnDst=' num2str(LdvnDstPix,'%.2f') ', RdvnDst=' num2str(RdvnDstPix,'%.2f')]); 
set(gca,'tickLength',[0; 0])
set(gca,'xtick',[]);
set(gca,'ytick',[])