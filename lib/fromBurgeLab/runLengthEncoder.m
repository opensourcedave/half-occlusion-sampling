function [indChg,runVal,runCnt] = runLengthEncoder(x)

% function [indChg,runVal,runCnt] = runLengthEncoder(x)
%
%   example call: [indChg,runVal,runCnt] = runLengthEncoder([0 0 1 1 1 0 1 1 0 0])
%
% uses run length encoding algorithm to compress string of booleans
%
% x:         vector of booleans         [ 1 x n ] or [ n x 1 ]
%%%%%%%%%%%%%%%%%%%%
% indChg:    indices of change point    [ 1 x nRun+1 ]
%            [1 length(x)]
% runVal:    value   of each run        [ 1 x nRun   ]
% runCnt:    length  of each run        [ 1 x nRun   ]

if size(x,1) > size(x,2), x = x'; end % if x is a column vector, tronspose
if size(x,1) > 1 && size(x,2) > 1, error(['runLengthEncoder: WARNING! x must be a vector. Currently size(x)=[' num2str(size(x)) ']' ]); end

i = [ find(x(1:end-1) ~= x(2:end)) length(x) ];
runVal    = x(i);
runCnt = diff([ 0 i ]);
indChg    = [1 cumsum(runCnt(1:end-1))+1 length(x)];
