function [sensRGB lambdaNm Luminance Radiance XYZ] = threeMonitorSpectra(whichMonitor,brightnessLevel,lambdaNmRange,lambdaNmSpacing,interpMethod,bPLOT)

% function [sensRGB lambdaNm Luminance Radiance XYZ] = threeMonitorSpectra(whichMonitor,brightnessLevel,lambdaNmRange,lambdaNmSpacing,interpMethod,bPLOT)
%
%   example call: threeMonitorSpectra('M1',10,[400 700],2,[],1)
%
% returns phosphor output as a function of wavelength for Dell 2007fp 
% monitors on GeislerLab optical bench setup
%
% whichMonitor:   'M1' monitor 1
%                 'M2' monitor 2
%                 'MF' fixation monitor 
% brightnessLevel: brightness level for specified monitor
% lambdaNmRange:   min and max wavelength in nm to return spectra with
% lambdaNmSpacing: wavelength resolution to return spectra with.
%                      interpolates if necessary
% interpMethod:    'linear' (default)
%                  'spline'
% bPLOT:           1 -> plot
%                  0 -> not
%%%%%%%%%%%%%
% sensRGB:
% lambdaNm: 
% Luminance:
% Radiance: 
% xyY: 
% 

slash = slashMACorPC;

% INPUT CHECKING
if ~exist('lambdaNmRange','var')
    lambdaNmRange =[];
end
if ~exist('lambdaNmSpacing','var')
    lambdaNmSpacing =[];
end
if ~exist('interpMethod','var')
   interpMethod = []; 
end
if ~exist('bPLOT','var')
    bPLOT = 0;
end

% SPECTROPHOTOMETER DIRECTORY
fdir = [  'ThreeMonitorCalib' slash 'Spectrophotometer' slash]; % spectrophotometer data

% SPECTROPHOTOMETER FILE NAMES
if     strcmp(whichMonitor,'M1') || strcmp(whichMonitor,'M2') || strcmp(whichMonitor,'MF')  
fnames = [whichMonitor 'R' zeroPadNumber(brightnessLevel,3) '.TXT'; ... % R % M1R010 -> [Monitor 1, R channel, monitor set to 10% brightness, 255 pix value] 
          whichMonitor 'G' zeroPadNumber(brightnessLevel,3) '.TXT'; ... % G
          whichMonitor 'B' zeroPadNumber(brightnessLevel,3) '.TXT'];    % B
else
    error(['threeMonitorSpectra: WARNING! unhandled whichMonitor: ' whichMontior]);
end

% LOAD SPECTROPHOTOMETER DATA
for f = 1:size(fnames,1) 
    [lambdaNm sensRGB(:,f) Luminance(f,1) Radiance(f,1),~,~,XYZ(f,:),xyzGamut(f,:)] = spectroPhotometerData(fdir,fnames(f,:),lambdaNmRange,lambdaNmSpacing,interpMethod);
end


% PLOT
if bPLOT
    figure('position',[591         245        1142         543]);
	hold on;
    color = 'rgb';
    subplot(1,2,1); hold on
    for c = 1:size(sensRGB,2)
    plot(lambdaNm,sensRGB(:,c),'-','color',color(c),'linewidth',2);
    end
    formatFigure('Wavelength (nm)','Radiance',[],0,0,18,14);
    writeText(1,.925,{['R=' num2str(Radiance(1),3) ' w/(str*m^2)']},'ratio',14,'right');
    writeText(1,.875,{['G=' num2str(Radiance(2),3) ' w/(str*m^2)']},'ratio',14,'right');
    writeText(1,.825,{['B=' num2str(Radiance(3),3) ' w/(str*m^2)']},'ratio',14,'right');
    
    writeText(0.075,.925,{['R=' num2str(Luminance(1),3) ' cd/m^2']},'ratio',14,'left');
    writeText(0.075,.875,{['G=' num2str(Luminance(2),3) ' cd/m^2']},'ratio',14,'left');
    writeText(0.075,.825,{['B=' num2str(Luminance(3),3) ' cd/m^2']},'ratio',14,'left');
    
    subplot(1,2,2); hold on
    for c = 1:size(sensRGB,2)
    plot(lambdaNm,bsxfun(@rdivide,sensRGB(:,c),sqrt(sum(sensRGB(:,c).^2))),'-','color',color(c),'linewidth',2);
    end
    formatFigure('Wavelength (nm)','Normalized Radiance',[],0,0,18,14);
    
    suptitle([whichMonitor ' phosphor spectra. brightness=' num2str(brightnessLevel) '%'],20);
end
