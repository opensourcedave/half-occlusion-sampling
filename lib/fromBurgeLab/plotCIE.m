function plotCIE()

[XYZsensitivityEnergy Lambda] = XYZsensitivity;

%Get xBar, yBar, and zBar...
xBar = XYZsensitivityEnergy(:, 1);
yBar = XYZsensitivityEnergy(:, 2);
zBar = XYZsensitivityEnergy(:, 3);

%Get x, y and z
x = xBar./(xBar + yBar + zBar);
y = yBar./(xBar + yBar + zBar);
z = 1 - x - y;

%Plot the CIE color map...
rgb = reshape([x,y,z],[length(xBar) 1 3]);
rgb(find(rgb<0)) = 0;
%%
figure('position',[840   131   663   647]);
patch(x,y,rgb);
formatFigure('x','y','CIE');
axis square;
axis([0 1 0 1]);