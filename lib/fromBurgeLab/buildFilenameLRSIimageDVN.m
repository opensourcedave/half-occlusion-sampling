function fname = buildFilenameLRSIimageDVN(imgNum,LorR)

% function fname = buildFilenameLRSIimageDVN(imgNum,LorR)
%
%   example call: 
% 
% filename for LRSI da vinci images (half-occlusion zones) 
%
% imgNum:    image numbers
% LorR:      'L' left  eye
%            'R' right eye
% %%%%%%%%%%
% fname:     filename

if ~strcmp(LorR,'L') && ~strcmp(LorR,'R')
   error(['buildFilenameLRSIimageDVN: WARNING! invalid LorR value: ' LorR]); 
end

fname = ['DVN_' LorR '_' num2str(imgNum,'%03d') '.mat'];
