function [Iweb,DC] = contrastImageVec(I,W,bPreWndw)

% function [Iweb,DC] = contrastImageVec(I,W,bPreWndw)
%
%   example calls: Iweb =    contrastImageVec(I)
%                  Iweb =    contrastImageVec(I,W)
%                  Iweb = W.*contrastImageVec(I,W)
%
% matrix of weber contrast images in column vector form
% from matrix of intensity images in column vector form
%
% I:         matrix of images in column vector form      [ Nd x nStm ]
% W:         window under which to compute contrast      [ Nd x  1   ]
% bPreWndw:  boolean indicating whether images have been pre-windowed
%            1 -> images have     been pre-windowed
%            0 -> images have NOT been pre-windwoed
%%%%%%%%%%%%
% Iweb:      weber contrast image                        [ Nd x nStm ]
% DC:        mean of image                               [ 1  x nStm ]

% INPUT HANDLING
if ~exist('W','var')        || isempty(W)        W = ones(size(I,1),1); end
if ~exist('bPreWndw','var') || isempty(bPreWndw) bPreWndw = 0;        end

tol = 0.2; % MINIMUNM MEAN THAT DOES NOT THROW AN ERROR
if sum(mean(I) < tol),         disp( ['contrastImageVec: WARNING! mean luminance is less than ' num2str(tol) ' in ' num2str(sum(mean(I) < tol)) ' images... Enter a luminance image']);   end
if size(W,2) ~= 1,             error(['contrastImageVec: WARNING! window is not in column vector form: size(W)=[' num2str(size(W,1)) ' ' num2str(size(W,2)) ']']);   end
if size(W,1) ~= size(I,1),     error(['contrastImageVec: WARNING! window size [' num2str(size(W,1)) ' ' num2str(size(W,2)) '] does not match image size [' num2str(size(I,1)) ' ' num2str(size(I,2)) ']']); end
if sum(W(:)>1) > 1,            error(['contrastImageVec: WARNING! check input W. Values greater than 1.0: max(W(:))=' num2str(max(W(:)))]); end

if bPreWndw == 0 % UNWINDOWED
    % MEAN: EASY TO READ (DC = sum(I(:).*W(:))./sum(W(:))
    DC   = bsxfun(@rdivide,sum(bsxfun(@times,I    ,W) ),sum(W));
    % CONTRAST IMAGE
    Iweb  = bsxfun(@rdivide,    bsxfun(@minus,I    ,DC),DC   );
elseif bPreWndw == 1 % PREWINDOWED
    % NON-ZERO INDICES
    indGd = W(:)>0;
    % MEAN OF PREWINDOWED IMAGE
    DC   = mean(I,1);
    % CONTRAST IMAGE
    Iweb  = bsxfun(@rdivide,    bsxfun(@minus,I    ,DC) ,DC   );
end