function [A0 B0 A0B0dist] = intersectionPointVec(A1,A2,B1,B2)

% function intersectionPoint(A1,A2,B1,B2)
%
%   example call: intersectionPoint([0 0 0],[0 0 1],[0.065 0 0],[-.05 0 1])
%
% find intersection point of two lines, each defined by two points
%
% A1: point 1 from line A   [n x 3] or [r x c x 3]
% A2: point 2 from line A   [n x 3] or [r x c x 3]
% B1: point 1 from line B   [n x 3] or [r x c x 3]
% B2: point 2 from line B   [n x 3] or [r x c x 3]
% %%%%%%%%%%%%%%%%%%%%%%%
% A0: nearest point to B on A
% B0: nearest point to A on B

% MAKE SURE POINTS LIVE IN 3-SPACE
% if length(size(A1)) == 3, A1 = reshape(A1,[],3)'; end
% if length(size(A2)) == 3, A2 = reshape(A2,[],3)'; end
% if length(size(B1)) == 3, B1 = reshape(B1,[],3)'; end
% if length(size(B2)) == 3, B2 = reshape(B2,[],3)'; end
% if prod(size(A1)~=size(A2))==0, A1 = repmat(A1,[1 size(A2,2) ]); end
% if prod(size(B1)~=size(B2))==0, B1 = repmat(B1,[1 size(B2,2) ]); end

% COMPUTE INTERSECTION POINT (IF IT EXISTS)
n=size(A2,1);

if all(size(A1)==size(B1)) && all(size(A2)==size(B2)) && any(size(A1)~=size(B2))
    N=n;
else
    N=1;
end
nA = sum(cross(bsxfun(@minus,B2,B1),repmat(bsxfun(@minus,A1,B1),N,1)).*cross(bsxfun(@minus,A2,A1),bsxfun(@minus,B2,B1)),2);
nB = sum(cross(bsxfun(@minus,A2,A1),repmat(bsxfun(@minus,A1,B1),N,1)).*cross(bsxfun(@minus,A2,A1),bsxfun(@minus,B2,B1)),2);

d  = sum(cross(bsxfun(@minus,A2,A1),bsxfun(@minus,B2,B1)).*cross(bsxfun(@minus,A2,A1),bsxfun(@minus,B2,B1)),2);
if any(size(A1)~=size(A2))
    N=n;
else
    N=1;
end
A0 = repmat(A1,N,1) + repmat((nA./d),1,3).*(bsxfun(@minus,A2,A1)); % nearest point on A to B

if any(size(B1)~=size(B2))
    N=n;
else
    N=1;
end
B0 = repmat(B1,N,1) + repmat((nB./d),1,3).*(bsxfun(@minus,B2,B1)); % nearest point on B to A

% COMPUTE DISTANCE BETWEEN LINE A & B NEAREST POINTS
A0B0dist = sqrt(sum(A0 - B0).^2);

% PRINT WARNING IF NO INTERSECTION
if roundDec(A0B0dist,1e-8) ~= 0
    % disp(['intersectionPoint: WARNING! lines A and B do not intersect. Distance between lines is : ' num2str(A0B0dist,3)]);
end
