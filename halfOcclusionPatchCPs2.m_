function [AitpRC, BitpRC, ABitpRC] = halfOcclusionPatchCPs(dspArcMin,ItpRC,LorR,fgndORbgnd,PszXY,stmXYdeg,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,IppZm,b2ctr,bPlot,bLoop)
%function [AitpRC, BitpRC, ABitpRC] = halfOcclusionPatchCPs(dspArcMin,ItpRC,LorR,fgndORbgnd,PszXY,stmXYdeg,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,IppZm)
% Get all CPs for a half-occlusion patch, given the anchor center. CPs are adjusted to patch coordinates and the plane is brought to fixation
%
% dspArcMin  - how many arc-minutes were added to fixation
% ItpRC      - anchor center in non-cropped image
% LorR       - whether the anchor center corresponds to the left or right eye
% fgndORbgnd - whether the foreground
% PszXY      - patch size in pixels
% Lxyz       - LRSI range data for left eye
% Rxyz       - LRSI range data for right eye
% LppXm      - X component of LRSI mesh-grid for left eye projection plane
% LppYm      - Y component of LRSI mesh-grid for left eye projection plane
% RppXm      - X component of LRSI mesh-grid for left eye projection plane
% RppYm      - Y component of LRSI mesh-grid for left eye projection plane
% IppZm      - distance from observer to LRSI projection plane
% b2ctr      - whether bring fixation to the projection plane by shifting CPs by a fixed amount
% bPlot      - plot CPs
% bLoop      - if plotting CPs in loop, use waitforbuttonpress to flip through
%
% AitpRC     - CPs for every pixel in anchor eye, including occluded zone
% BitpRC     - CPs for every pixel in non-anchor eye
% ABitpRC    - CPs for every pixel in non-anchor eye, including occluded surface


%DEFAULTS
if ~exist('bPlot','var') || isempty(bPlot)
    bPlot=0;
end
if ~exist('bLoop','var') || isempty(bLoop)
    bLoop=0;
end
if ~exist('b2ctr','var') || isempty(b2ctr)
    b2ctr=0;
end
if ~exist('IppZm','var') || isempty(IppZm)
    IppZm=3;
end
IPDm=LRSIcameraIPD(1);

%GET CORRESPONDING POINTS
[LitpRC,RitpRC,~,LitpRCchk,RitpRCchk,indNaN] = LRSIcorrespondingPointVec([],LorR,ItpRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,0,'wall');
[LitpRCchkDsp, RitpRCchkDsp]                 = LRSIcorrespondingPointAddDisparityExact(LitpRCchk,RitpRCchk,dspArcMin,LppXm,LppYm,RppXm,RppYm,IppZm,IPDm);

if strcmp(LorR,'L')
    [LitpRCall]       = ctrCP2patchCPs(LitpRC,PszXY)
    [LitpRCchkAll]    = ctrCP2patchCPs(LitpRCchk,PszXY)
    [LitpRCdspAll]    = ctrCP2patchCPs(LitpRCdsp,PszXY)
    [LitpRCchkDspAll] = ctrCP2patchCPs(LitpRCchkDsp,PszXY)

    [LitpRCchkDspAll,RitpRCchkDspAll] = LRSIcorrespondingPointVec([],LorR,LitpRCdspAll,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,0,'wall');

elseif strcmp(LorR,'R')
    [RitpRCall]       = ctrCP2patchCPs(RitpRC,PszXY)
    [RitpRCchkAll]    = ctrCP2patchCPs(RitpRCchk,PszXY)
    [RitpRCdspAll]    = ctrCP2patchCPs(RitpRCdsp,PszXY)
    [RitpRCchkDspAll] = ctrCP2patchCPs(RitpRCchkDsp,PszXY)
end

%GET ALL INDECES FOR PSZXY
ctrPix=[PszXY(2), PszXY(1)]/2;

%ADJUST CORRESPONDING POINTS TO PATCH COORDINATES AND BRING FIXATION TO THE PLANE
%   subtract by center LRSI image pixel to bring to zero, then add to convert to patch coordinates
if b2ctr==1
    if strcmp(LorR,'L')
        if strcmp(fgndORbgnd,'fgnd')
            AitpRC  = bsxfun(@minus,LitpRCchkDspAll, LitpRCchkDsp)+ctrPix;
            ABitpRC = bsxfun(@minus,LitpRCdspAll   , LitpRCdsp)+ctrPix;
        elseif strcmp(fgndORbgnd,'bgnd')
            AitpRC  = bsxfun(@minus,LitpRCdspAll   , LitpRCdsp)+ctrPix;
            ABitpRC = bsxfun(@minus,LitpRCchkDspAll, LitpRCchkDsp)+ctrPix;
        end
        BitpRC      = bsxfun(@minus,RitpRCdspAll   , RitpRCdsp)   +ctrPix;
    elseif strcmp(LorR,'R')
        if strcmp(fgndORbgnd,'fgnd')
            AitpRC  = bsxfun(@minus,RitpRCchkDspAll, RitpRCchkDsp)+ctrPix;
            ABitpRC = bsxfun(@minus,RitpRCdspAll   , RitpRCchkDsp)+ctrPix;
        elseif strcmp(fgndORbgnd,'bgnd')
            AitpRC  = bsxfun(@minus,RitpRCdspAll   , RitpRCchkDsp)+ctrPix;
            ABitpRC = bsxfun(@minus,RitpRCchkDspAll, RitpRCchkDsp)+ctrPix;
        end
        BitpRC      = bsxfun(@minus,LitpRCall   , LitpRCdsp)   +ctrPix;
    end
elseif b2ctr==0
    if strcmp(LorR,'L')
        if strcmp(fgndORbgnd,'fgnd')
            AitpRC  = bsxfun(@minus, LitpRCchkAll , LitpRC)+ctrPix;
            ABitpRC = bsxfun(@minus, LitpRCall    , LitpRC)+ctrPix;
        elseif strcmp(fgndORbgnd,'bgnd')
            AitpRC  = bsxfun(@minus, LitpRCall    , LitpRC)+ctrPix;
            ABitpRC = bsxfun(@minus, LitpRCchkAll , LitpRC)+ctrPix;
        end
        BitpRC      = bsxfun(@minus,RitpRCall     , RitpRC)+ctrPix;
    elseif strcmp(LorR,'R')
        if strcmp(fgndORbgnd,'fgnd')
            AitpRC  = bsxfun(@minus,RitpRCchkAll  , RitpRC)+ctrPix;
            ABitpRC = bsxfun(@minus,RitpRCall     , RitpRC)+ctrPix;
        elseif strcmp(fgndORbgnd,'bgnd')
            AitpRC  = bsxfun(@minus,RitpRCall     , RitpRC)+ctrPix;
            ABitpRC = bsxfun(@minus,RitpRCchkAll  , RitpRC)+ctrPix;
        end
        BitpRC      = bsxfun(@minus,LitpRCall     , LitpRC)+ctrPix;
    end
end

%TEST RECENTERED CPs
%bPlot=1
%bLoop=1
if bPlot==1
    %TEST ALL CPs AFTER RECENTERING AND TO FIXATION PLANE
    figure(1343)
    subplot(1,3,1)
    scatter(AitpRC(:,2),AitpRC(:,1),'k.')
    title([LorR ' AitpRC'])
    axis square
    subplot(1,3,2)
    scatter(ABitpRC(:,2),ABitpRC(:,1),'k.')
    title('ABitpRC')
    axis square
    subplot(1,3,3)
    scatter(BitpRC(:,2),BitpRC(:,1),'k.')
    title('BitpRC')
    axis square
    if bLoop==1
        drawnow
    end

    figure(9349)
    %TEST ALL CPs BEFORE RECENTERING AND TO FIXATION PLANE
    subplot(4,2,1)
    scatter(LitpRCall(:,2),LitpRCall(:,1),'.')
    title('LitpRCall')
    axis square
    subplot(4,2,2)
    scatter(RitpRCall(:,2),RitpRCall(:,1),'.')
    title('RitpRCall')
    axis square
    subplot(4,2,3)
    scatter(LitpRCdspAll(:,2),LitpRCdspAll(:,1),'.')
    title('LitpRCdspAll')
    axis square
    subplot(4,2,4)
    scatter(RitpRCdspAll(:,2),RitpRCdspAll(:,1),'.')
    title('RitpRCdspAll')
    axis square
    subplot(4,2,5)
    scatter(LitpRCchkAll(:,2),LitpRCchkAll(:,1),'.')
    title('LitpRCchkAll')
    axis square
    subplot(4,2,6)
    scatter(RitpRCchkAll(:,2),RitpRCchkAll(:,1),'.')
    title('RitpRCchkAll')
    axis square
    subplot(4,2,7)
    scatter(LitpRCchkDspAll(:,2),LitpRCchkDspAll(:,1),'.')
    title('LitpRCchkDspAll')
    axis square
    subplot(4,2,8)
    scatter(RitpRCchkDspAll(:,2),RitpRCchkDspAll(:,1),'.')
    title('RitpRCchkDspAll')
    axis square
    if bLoop==1
        disp('Press key to display next!')
        drawnow
        pause(20)
        waitforbuttonpress
    end
end