classdef samplesMap < handle
% Sample from DaVinci regions to find potential patch centers that satisfy the following:
%   Are the center or leftmost pixel in the davinci region (ctrORedg)
%   In a DaVinci region with a minimum width (minMaxPixMap(1)) and optionally w/ max (minMaxPixMap(2))
%       This width Regions width extends through vertical of specified patch size
%       [with some buffering (zoneBufferRC)
%           OR (zoneTestType)
%       DaVinci region within patch size meets a DaVinci density requirement
%   Potential Patches do not overlap
%
%   example call:
%       IctrRC=samplesMap(1,[130 52],'L',edg,18,'slice',[0 13],.2,1,1);
%       daVinciZoneSample([52 52],'B',8,18,'bgn','slice',13,0,.2,1,rootDTBdir,1,8,[])
%
%   INPUTS
    if obj.strcmp(mapMType=,'DVN')
    %   imgNum   - range of images to sue from DVN[numImages x 1]
        end
%               *Optional DVN3 including Lpht,Rpht,Lrng,Rrng,Lmap,Rmap
%   PszXY    - patch size [2 x 1]
%   LorR     - which anchor to use
%              'L' -> Left eye ancho
%              'R' -> Right eye anchor
%              'B' -> Both eyes as anchors (individually not togehter)
%   ctrORedg - whether the patch center is at the beginning or end of DaVinci region
%   minMaxPixMap - [2 x 1]
%              (1) minimum width of DaVinci region for patch center
%              (2) maximum width of DaVinci region for patch center
%                 'edg' -> patch center is at center pixel of DaVicni region
%                 'bgn' -> patch center is at leftmost pixerl of DaVinci region
%   nMapbuffer   - size of Map region in corresponding patch to be considered significant (superSlice method only)
%   nMap         - number of Map regions allowed in each corresponding patch (superSlice method only)
%   minZoneSep   - required horizontal distance between central and additional significant Map zones. (superSlice method only)
%   zoneTestType - whether to use 'slice' or 'density' vetting for patch center
%              'slice' - use slice method, where DaVinci region center width must be present
%               within every row of the patch
%              'superSlice' - slice method, but allows for non-significant Map zones, and allows
%                             a specified number of sigificant Map zones if they are seperated
%                             horizontally by a specified distance.
%                             See nMapbuffer, nMap, minZoneSep.
%              'density' - DaVinci region must have a specified density within patch region
%   zoneBufferRC(2) - buffer for 'slice' method: width can be minMaxPixMap(1)-zoneBufferRC(2) in length
%              to count as an extending region in patch area
    Wk
    stmSXYdeg
%   zoneBufferRC(1) - buffer for 'slice' method: if there are X number of rows in a patch, there
%              must be X-zoneBufferRC(1) rows with width of minMaxPixMap(1)-zoneBufferRC(2) to be
%              classified as a valid patch
%   zoneMinDensity - density of DaVinci pixels to non-DaVinci pixels in patch region to be
%              classified as a valid patch
%   rndSd    - random seed to use for random sampling
%              0 -> non-random sampling that maximizes number of samples
%   plotFlag - whether to plot and/or which part of the image to plot
%               0 -> do not plot
%               1 -> plot for only L or R anchor only (whatever is being used)
%               2 -> plot for Left anchor
%               3 -> plot for RIght anchor
%               2 & 3 are meant to be used in a loop
%   Lpht     - left luminance image  [PszXY(2) x PszXY(1) ]
%   Rpht     - right luminance image [PszXY(2) x PszXY(1) ]
%   Lrng     - left range image      [PszXY(2) x PszXY(1) ]
%   Rrng     - right range image     [PszXY(2) x PszXY(1) ]
%   Lmap     - left DaVinci image    [PszXY(2) x PszXY(1) ]
%   Rmap     - right DaVinci image   [PszXY(2) x PszXY(1) ]
%   rootDTBdir - root directory of database, exlcuding the 'LRSI' directory
%               *optional for burge lab - TODO add this functionality
%   indLookup - index lookup table defined as:
%                indLookup=reshape(1:(IszRC(1)*IszRC(2)),IszRC)
%                defines this outside of this function and loops to make things speedy
%   overlapPix- number of pixels patches are allowed to overlap vertically
%  dspArcMin   - how much disparity in arcMinutes to add to stereo-patches
%  fgndORbgnd  - Whether to have cyclopian focus on the foreground or background
%  nSmpPerImg  - number of samples per image desired. Determined from nSmp if unspecified.
%
%
%
%   OUTPUTS
%   IctrRC     - veted patch center subscripts  [maxSmp x 2]
%   IctrInd    - veted patch center indeces     [maxSmp x 1]

%   ImapCtrRC  - left- or right-eye subscripts of zone centers
%   ImapCtrInd - left- or right-eye indeces of zone centers
%   widthAll   - widths of ImapCtr*

%   PctrRC     - valid patch center subscripts
%   PctrInd    - valid patch center index
%              - struct containing the following:
%          LitpRCchkDsp- Left image patch center after adding disparity.
%                            Also the 'check' version of LitpRCall, meaning it uses the opposite eye to
%                            find the corresponding point, and thus is centered on the occluding
%                            surface.
%                            [nSmp x 2]
%
%          RitpRCchkDsp- Right image patch center after adding disparity.
%                            Also the 'check' version of RitpRCall, meaning it uses the opposite eye to
%                            find the corresponding point, and thus is centered on the occluding
%                            surface.
%          LitpRCchk- Left image patch center before adding disparity
%          RitpRCchk- Right image patch center before adding diparity
%          LitpRCall       - Left image patch center of occluded region defined by daVinciZoneCenter
%                            if using left anchor, or found using LRSIcorrespondingPointVec if using
%                            right anchor
%          RitpRCall       - Right image patch center of occluded region defined by daVinciZoneCenter
%                            if using right anchor, or found using LRSIcorrespondingPointVec if using
%                            left anchor
%          LitpRCdsp- Right image patch center after adding disparity.
%          RitpRCdsp- Left image patch center after adding disparity.
%          LphtCrp- cropped luminance images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LphtCrpZer - cropped luminance images centered on LitpRCchk (no disparity added)
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LrngCrp- cropped range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LxyzCrp- cropped cartesian range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x 3 x nSmp]
%          LmapCrp- cropped DaVinci images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LppXmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LppYmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RphtCrp- cropped luminance images centered on LitpRCchkDspA
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RrngCrp- cropped range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RxyzCrp- cropped cartesian range images centered on LitpRCc
%                            [PszXY(2) x PszXY(1) x 3 x nSmp]
%          RmapCrp- cropped DaVinci images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RppXmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RppYmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%
% --------------------------------------------------------------------
% XXXX % function [P] = daVinciSamplePatch(varargin)
%
%   example call:
%      P = daVinciSamplePatchBatch();
%      P = daVinciSamplePatchBatch(H);
%
% Handle batch processing of batch patch creation
%    daVinciZoneSamplePatchBatch      - handle patch grabbing for given disparity and map width
%    daVinciZoneSamplePatchBatchBatch - Loops over all specified disparities and widths
% --------------------------------------------------------------------
% INPUT:
%
% H - struct: SEE daVinciZoneParamParse FOR DETAILS ON H FIELDS (optional)
% --------------------------------------------------------------------
% OUTPUT:
%
% P               - struct containing the following:
%   imgNumAll       - index of all images used for all other outputs  [nSmp x 1]
%   LorRAll         - index of all anchors used for all other outputs [nSmp x 1]
%   LctrCrp         - XXX [nSmp x 2]
%   RctrCrp         - XXX [nSmp x 2]
%   LitpRCchkDspAll - Left image patch center after adding disparity.
%                              Also the 'check' version of LitpRCall, meaning it uses the opposite eye to
%                              find the corresponding point, and thus is centered on the occluding
%                              surface.
%                              [nSmp x 2]
%
%   RitpRCchkDspAll - Right image patch center after adding disparity.
%                              Also the 'check' version of RitpRCall, meaning it uses the opposite eye to
%                              find the corresponding point, and thus is centered on the occluding
%                              surface.
%   CitpRCchkDspAll - XXX
%   LitpRCchkAll    - Left image patch center before adding disparity
%   RitpRCchkAll    - Right image patch center before adding diparity
%   LitpRCall       - Left image patch center of occluded region defined by daVinciZoneCenter
%                     if using left anchor, or found using LRSIcorrespondingPointVec if using
%                              right anchor
%   RitpRCall       - Right image patch center of occluded region defined by daVinciZoneCenter
%                     if using right anchor, or found using LRSIcorrespondingPointVec if using left anchor
%   LitpRCdspAll    - Right image patch center after adding disparity.
%   CitpRCdspAll    - XXX
%   RitpRCdspAll    - Left image patch center after adding disparity.
%   LphtCrpAll      - cropped luminance images centered on LitpRCchkDspAll
%                     [PszXY(2) x PszXY(1) x nSmp]
%   LrngCrpAll      - cropped range images centered on LitpRCchkDspAll
%                     [PszXY(2) x PszXY(1) x nSmp]
%   LxyzCrpAll      - cropped cartesian range images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x 3 x nSmp]
%   LmapCrpAll      - cropped daVinci images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   LppXmCrpAll     - cropped left eye projection plane pixel locations in X
%                              [PszXY(2) x PszXY(1) x nSmp]
%   LppYmCrpAll     - cropped left eye projection plane pixel locations in Y
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RphtCrpAll      - cropped luminance images centered on LitpRCchkDspA
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RrngCrpAll      - cropped range images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RxyzCrpAll      - cropped cartesian range images centered on LitpRCc
%                              [PszXY(2) x PszXY(1) x 3 x nSmp]
%   RmapCrpAll      - cropped daVinci images centered on LitpRCchkDspAll
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RppXmCrpAll     - cropped right eye projection plane pixel locations in X
%                              [PszXY(2) x PszXY(1) x nSmp]
%   RppYmCrpAll     - cropped right eye projection plane pixel locations in Y
%                              [PszXY(2) x PszXY(1) x nSmp]
%   fig1            - XXX
%   fig2            - XXX
%   fig3            - XXX
%     stmXYdeg       - size of
% --------------------------------------------------------------------
properties
    Lpht
    Rpht
    Ipht
    Lrng
    Rrng
    Irng
    Lmap
    Rmap
    Imap
    Omap
    Lxyz
    Rxyz
    Ixyz
    LppXm
    RppXm
    LppYm
    RppYm
    ML % XXX ?
    MR

    IppZm
    IPDm
    interpType
    nSmpPerImg
    fgndORbgnd
    dspArcMin

    LitpRC
    RitpRC
    BitpRC

    rootDTBdir
    saveDTB
    blank

    imgNums
    imgNumsOrig
    LandR
    LorRorB

    imgNum
    PszXY
    PszRC
    LorR
    IszRC

    zoneTestType
    zoneBufferRC
    zoneMinDensity
    bTwoWay
    nMapbuffer
    nMap
    minZoneSep
    minMaxPixMap
    overlapPix
    ctrORedg
    bExtra
    bPreDispDSP % XXX
    bDisplayParams  % XXX
    b2ctr % XXX
    monoORbino % XXX
    bByImage

    bWarn
    bDebug
    bSaveFig
    bSave
    bPlot
    bPlotCtrORedg
    plotImgNum
    plotFlag
    titl

    stmXYdeg
    hostName

    PctrIndPri
    PctrIndPriAll

    RMS
    DC
    W
    Wk

    ImapCtrRC
    ImapCtrInd
    widthAll

    PctrRC
    PctrInd
    MapcpKernRadHW
    MapkernRadHW
    MapkernArea
    valInd
    vert
    hori
    vertCP
    horiCP

    IctrInd
    IctrRC

    figVet
    figSamples
    figCtrOrEdg
end
properties(Hidden = true)
    j
    co
    N
    continueFlag
    MIN=inf;
end
methods
    P=new_patches_Map(obj);

    % XXX Input parser
    function obj = samplesMap(Opts)
        obj.init_opts(Opts);
        obj.batch_proces();
    end

    function obj = batch_process()
        [obj,P]=obj.init_batch(obj)

        pr=prog(length(obj.imgNums),'batch Map samples')
        for i = 1:length(obj.imgNums) % LOOP OVER IMAGE
            I=obj.get_LRSI_image(i);
            obj.imgNum=obj.imgNums(i);
            obj.LorR=LandR{k};

	        for k  = 1:length(LandR) % LOOP OVER ANCHOR EYE
                obj.get_plot_flag(k);
                Ptmp = obj.proces_new_image(I);
                P    = obj.batch_pack(i,k,P,Ptmp);
            end
            pr=pr.update();
        end
        P = obj.batch_finalize(P);
        P = obj.extra(P);
        P = daVinciPostVet(P);
    end

    function obj=process_new_image(obj,I)
        obj.init_image(I);
        obj.get_center_or_edge();
        if isempty(obj.ImapCtrInd)
            return
        end
        obj.get_CPs();
        obj.sample_vet();
        obj.sample()
        obj.plot();
    end

    % -----------------------------------------------------------------
    function obj=init_image(obj,I)
        unpackOpts(I,1);

        if ~exist('Lpht','var') || ~exist('Rpht','var') || ...
           ~exist('Lrng','var') || ~exist('Rrng','var')
            [obj.Lpht,obj.Rpht,obj.Lrng,obj.Rrng] = loadLRSIimage(obj.imgNum,1,0,'PHT','img',rootDTBdir,'XYZ2');
        else
            obj.Lpht=Lpht;
            obj.Rpht=Rpht;
            obj.Lrng=Lrng;
            obj.Rrng=Rrng;
        end

        if   ~exist('Lmap','var') || ~exist('Rmap','var')
            [obj.Lmap,obj.Rmap]           = loadLRSIimageMap(obj.imgNum,'local',0,rootDTBdir,'Map3');
        else
            obj.Lmap=Lmap;
            obj.Rmap=Rmap;
        end
        obj.IszRC = size(obj.Lpht);
        obj.indLookup = reshape(1:(obj.IszRC(1)*obj.IszRC(2)),obj.IszRC);

        % XXX ?
        if exist('ML','var')
            obj.ML=ML;
        end
        if exist('MR','var')
            obj.MR=MR;
        end

        obj.Lxyz =Lxyz;
        obj.Rxyz =Rxyz;
        obj.LppXm=LppXm;
        obj.RppXm=RppXm;
        obj.LppYm=LppYm;
        obj.RppYm=RppYm;


        %SPECIFY CORRECT IMAGE FOR ACHOR
        if strcmp(LorR,'L')
            obj.Imap=obj.Lmap;
            obj.Irng=obj.Lrng;
            obj.Omap=logical(obj.Rmap);
            obj.Ixyz=obj.Lxyz;
            obj.Ipht=obj.Lpht;
        elseif strcmp(LorR,'R')
            obj.Imap=obj.Rmap;
            obj.Irng=obj.Rrng;
            obj.Omap=logical(obj.Lmap);
            Ixyz=Rxyz;
            obj.Ipht=obj.Rpht;
        end

        % XXX
        obj.Imap=ceil(obj.Imap);
    end

    function init_opts(obj,imgNum,I,LorR,Opts)
        % XXX
        bBatch=1;

        p = inputParser();
        if bBatch
            p.addParameter('zoneBufferRCall',-1,@isnumeric);
            p.addParameter('dspArcMinAll',0,@isnumeric);
            p.addParameter('minMaxPixMapall',[18 inf],@isnumeric);
        else
            p.addParameter('zoneBufferRC',-1,@isnumeric);
            p.addParameter('dspArcMin',0,@isnumeric);
            p.addParameter('minMaxPixMap',[18 inf],@isnumeric);
            % REMOVE IRRELEVANT FIELDS IF CREATED FROM BATCH
            if isvar('Opts') && isfield(Opts,'zoneBufferRCall')
                Opts=rmfield(Opts,'zoneBufferRCall');
            end
            if isvar('Opts') && isfield(Opts,'dspArcMinAll')
                Opts=rmfield(Opts,'dspArcMinAll');
            end
            if isvar('Opts') && isfield(Opts,'minMaxPixMapall')
                Opts=rmfield(Opts,'minMaxPixMapall');
            end
            if isvar('Opts') && isfield(Opts,'Wk')
                Opts=rmfield(Opts,'Wk');
            end
            if isvar('Opts') && isfield(Opts,'bCreate')
                Opts=rmfield(Opts,'bCreate');
            end
        end

        p.addParameter('rootDTBdir','',@ischar);
        p.addParameter('saveDTB','',@ischar);
        p.addParameter('bSave',1,@isnumeric);
        p.addParameter('imgNums',8,@isnumeric);
        p.addParameter('bPlot',1,@isnumeric);
        p.addParameter('plotImgNum',8,@isnumeric);
        p.addParameter('bSaveFig',0,@isnumeric);
        p.addParameter('LorRorB','B',@ischar);
        p.addParameter('fgndORbgnd','fgnd',@ischar);
        p.addParameter('PszXY',[130 50],@isnumeric);
        p.addParameter('rndSd',1,@isnumeric);
        p.addParameter('nSmpPerImg',20,@isnumeric);
        p.addParameter('nSmp',100,@isnumeric);
        p.addParameter('ctrORedg','edg',@ischar);
        p.addParameter('zoneTestType','slice',@ischar);
        p.addParameter('zoneMinDensity',.2,@isnumeric);
        p.addParameter('DC',[],@isnumeric);
        p.addParameter('RMS',[],@isnumeric);
        p.addParameter('hostName',[],@ischar);
        p.addParameter('bDisplayParams',0,@isnumeric);
        p.addParameter('bPreDispDsp',0,@isnumeric);
        p.addParameter('stmXYdeg',[3 1],@isnumeric);
        p.addParameter('b2ctr',0,@isnumeric);
        p.addParameter('overlapPix',0,@isnumeric);
        p.addParameter('nMap',                    1,          @isnumeric);
        p.addParameter('nMapbuffer',              30,          @isnumeric);
        p.addParameter('minZoneSep',              10,          @isnumeric);
        p.addParameter('bTwoWay',              0,               @isnumeric);
        p.addParameter('monoORbino', 'bino', @ischar);
        p.addParameter('bByImage', 0, @isnumeric);
        p.addParameter('bDebug',0, @isnumeric);
        p.addParameter('IppZm',3,@isnumeric);
        p.addParameter('IPDm',.065,@isnumeric);
        p.addParameter('interpType','linear',@ischar);
        p.addParameter('bPlotCtrORedg',0,@isnumeric);
        p.addParameter('bWarn',1,@isnumeric);
        p.addParameter('PctrIndPri',[],@isnumeric); % XXX
        p.addParameter('PctrIndPriAll',{},@iscell); % XXX
        p.addParameter('imgNumsOrig',[], @isnumeric);
        p.addParameter('Wk',0,@isnumeric);
        p.addParameter('W',[],@isnumeric);

        if isvar('Opts') && isfield(Opts,'bDebug')
            bDebug=Opts.bDebug;
        else
            bDebug=0;
        end

        p=parseStruct(Opts,p,~bDebug,~bDebug);
        obj.IppZm         = p.IppZm;
        obj.IPDm          = p.IPDm;
        obj.interpType    = p.interpType;
        obj.PszXY         = p.PszXY;
        obj.zoneTestType  = p.zoneTestType;
        obj.zoneBufferRC  = p.zoneBufferRC;
        obj.zoneMinDensity= p.zoneMinDensity;
        obj.bTwoWay       = p.bTwoWay;
        obj.nMapbuffer    = p.nMapbuffer;
        obj.nMap          = p.nMap;
        obj.minZoneSep    = p.minZoneSep;
        obj.minMaxPixMap  = p.minMaxPixMap;
        obj.overlapPix    = p.overlapPix;
        obj.ctrORedg      = p.ctrORedg;
        obj.indLookup     = p.indLookup;
        obj.rndSd         = p.rndSd
        obj.nSmpPerImg    = p.nSmpPerImg;
        obj.nSmp          = p.nSmp
        obj.fgndORbgnd    = p.fgndORbgnd;
        obj.bSave         = p.bSave;
        obj.imgNums       = p. imgNums;
        obj.saveDTB       = p.saveDTB
        obj.bPlotCtrORedg = p.bPlotCtrORedg;
        obj.bPlot         = p. bPlot
        obj.plotImgNum    = p.plotImgNum
        obj.bWarn         = p.bWarn;
        obj.DC            = p.DC
        obj.RMS           = p.RMS;
        obj.Wk            = p.Wk;
        obj.W             = p.W;
        obj.b2ctr         = p.b2ctr;
        obj.stmXYdeg      = p.stmXYdeg;
        obj.bTwoWay       = p.bTwoWay;
        obj.monoORbino    = p.monoORbino
        obj.bByImage      = p.bByImage;
        obj.imgNumsOrig   = p.imgNumsOrig;

        % XXX is this a good idea?
        if any(mod(obj.PszXY,2)==0)
            warning('PszXY must be odd!')
        end

        %MODIFY SOME PARAMETERS
        if isequal(obj.zoneBufferRCall,-1)
            obj.zoneBufferRCall(:,2) = obj.minMaxPixMapall(:,1)-5;
            obj.zoneBufferRCall(:,1) = zeros(size(obj.minMaxPixMapall,1),1);
        end

        % XXX
        %imgExc = [8,36,43,60,65,86,94]; %Images that have bad registration, don't change!
        imgExc = [36,43,60,65,86,94]; %Images that have bad registration, don't change!
        obj.imgNumsOrig=obj.imgNums;
        ind=ismember(obj.imgNum,imgExc);
        obj.imgNums=obj.imgNums(~ind);
        if isempty(obj.PctrIndPriAll)
            obj.PctrIndPriAll=cell(length(obj.imgNums),1);
        elseif isequal(length(obj.PctrIndPriAll),obj.imgNumsOrig)
            obj.PctrIndPriAll=obj.PctrIndPriAll(~ind);
        end

        if sum(ind)~=0
            warning(['daVinciZoneSampleBatch: excluding image(s) ' num2rangeStr(obj.imgNumOrig(ind)) '.'])
        end

        if ~isequal(obj.Wk , 0)
            % XXX
            obj.W=[];
            %obj.W = cosWindowRect([obj.PszXY(2),obj.PszXY(1)],obj.Wk);
        else
            obj.W=[];
        end

        if strcmp(obj.zoneTestType,'superSlice') && ~isempty(obj.nMapbuffer)
            error('davinciZoneSampleVet: superSlice method requires nMapbuffer to be specified')
        end
        if strcmp(obj.zoneTestType,'superSlice') && isempty(obj.nMap)
            error('davinciZoneSampleVet: superSlice method requires nMap to be specified')
        end
        if strcmp(obj.zoneTestType,'superSlice') && isempty(obj.minZoneSep)
            error('davinciZoneSampleVet: superSlice method requires minZoneSep to be specified')
        end

        %DEFINE 'nSmpPerImg' IF USING 'nSmp'
        if isempty(obj.nSmpPerImg)
            obj.nSmpPerImg=ceil(obj.nSmp/length(obj.imgNums)*2);  %maxmimum number of samples per image
        end
        if obj.nSmpPerImg > 300
            obj.nSmpPerImg=300;
            disp('Warning: changing nSmpPerImg to 300!')
        end
        if mod(PszXY(1),2)==0
            obj.PszXY(1)=obj.PszXY(1)+1;
        end
        if mod(PszXY(2),2)==0
            obj.PszXY(2)=PszXY(2)+1;
        end
        if numel(obj.minMaxPixMap)==1
            obj.minMaxPixMap=[obj.minMaxPixMap inf]
        end
        obj.PszRC=fliplr(obj.PszXY);
        obj.blank=zeros(obj.PszRC);


        %HEIGHT (1) AND WIDTH (2) OF REGION TO CHECK FOR Map
        obj.MapcpKernRadHW  = fliplr(floor(obj.PszXY./2));
        %HEIGHT (1) AND WIDTH (2) OF REGION TO CHECK FOR Map
        obj.MapkernRadHW    = fliplr(floor(obj.PszXY./2));
        %DENSITY ONLY
        obj.MapkernArea     = (obj.MapkernRadHW(1)*2+1)*(obj.MapkernRadHW(2)*2+1);

        % XXX
        obj.PctrRC=ceil(obj.PszRC/2);
        obj.PctrInd=sub2ind(obj.PszRC,obj.PctrRC(1),obj.PctrRC(2));
        obj.plotFlag % XXX
    end

    function [obj,P]=init_batch(obj)

        %CREATE 'LandR' VARIABLE
        if strcmp(obj.LorRorB,'B')
            obj.LandR={'L','R'};
            P.LorRall=repmat(['L';'R'], obj.nSmpPerImg*length(obj.imgNums),1);
        else
            P.LorRall=repmat(obj.LorRorB, 2*obj.nSmpPerImg*length(obj.imgNums),1);
            obj.LandR=obj.LorRorB;
        end

        P.imgNumAll=repelem(obj.imgNums',obj.nSmpPerImg*2,1);
        P.maxSmp=zeros(length(obj.imgNums),length(obj.LandR));

        P.LitpRCchkDspAll = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.RitpRCchkDspAll = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.CitpRCchkDspAll = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.RitpRCchkAll    = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.LitpRCchkAll    = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.LitpRCall       = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.RitpRCall       = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.LitpRCdspAll    = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.RitpRCdspAll    = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.CitpRCdspAll    = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.RctrCrp         = zeros(2*length(imgNums)*obj.nSmpPerImg,2);
        P.LctrCrp         = zeros(2*length(imgNums)*obj.nSmpPerImg,2);

        P.LphtCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LphtCrpZerAll   = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LrngCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LmapCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LppXmCrpAll     = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LppYmCrpAll     = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.LxyzCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),3,2*length(imgNums)*obj.nSmpPerImg);

        P.RphtCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RphtCrpZerAll   = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RrngCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RmapCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RppXmCrpAll     = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RppYmCrpAll     = zeros(obj.PszXY(2), obj.PszXY(1),  2*length(imgNums)*obj.nSmpPerImg);
        P.RxyzCrpAll      = zeros(obj.PszXY(2), obj.PszXY(1),3,2*length(imgNums)*obj.nSmpPerImg);

        %ASSIGN INFORMATIONAL VARIABLES TO P
        P.dspArcMinAll=obj.dspArcMin;
        P.minMaxPixMap=obj.minMaxPixMap;
        P.ctrORedg=obj.ctrORedg;
        P.PszXY=obj.PszXY;
        P.rndSd=obj.rndSd;
        P.fgndORbgnd=obj.fgndORbgnd;
        P.bPreDispDsp=obj.bPreDispDsp;
        P.nMapbuffer=obj.nMapbuffer;
    end


    % --------------------------------------------------------------------
    function obj=sample_vet(obj)
        if strcmp(obj.zoneTestType,'superSlice')
            obj.vet_by_density_loop;
        elseif strcmp(obj.zoneTestType,'superSlice')
            obj.vet_by_super_slice_loop;
        elseif strcmp(obj.zoneTestType,'slice')
            obj.vet_by_slice_loop();
        end
        obj.sample_vet_finalize();
    end
    function obj=sample_vet_finalize(obj)
    %USE VALIND AS INDEX FOR VALID ImapCtrInd
        obj.PctrInd=obj.ImapCtrInd(obj.valInd);
        obj.PctrRC=obj.ImapCtrRC(obj.valInd,:);
    end
    % --------------------------------------------------------------------
    function obj=sample(obj)
    % PctrIndPri  XXX ?

    % DAVE MAX SAMPLING METHOD
        [obj.IctrInd,obj.IctrRC] = samplePatchCentersMax(obj.PctrIndPri,obj.PctrInd,obj.obj.IszRC,obj.PszXY,obj.indLookup,obj.rndSd,[],obj.overlapPix);
    end
    % --------------------------------------------------------------------
    function obj=vet_by_density_loop(obj)
        ratio=zeros(length(obj.ImapCtrInd),1);
        for obj.j = 1:length(obj.ImapCtrInd)
            obj.vet_by_density();
        end
    end
    function obj=vet_by_slice_loop(obj)
        for obj.j =1:length(obj.ImapCtrInd)
            obj.vet_by_slice();
            obj.vet_by_slice_part2();
        end
    end
    function obj=vet_by_super_slice_loop(obj)
        obj.MIN=inf;
        for obj.j =1:length(obj.ImapCtrInd)
            obj.vet_by_slice();
            obj.vet_by_super_slice();
        end
    end
    function obj=vet_by_density(obj)

        %DO NOT INCLUDE PATCHES OUTSIDE OF RANGE
        obj.get_window()
        if obj.continueFlag==1; return; end

        %DO NOT INCLUDE PATCHES WITH NANS
        if any(isnan(obj.Ixyz(obj.vert,obj.hori)))
            continue
        end

        %CALCULATE DENSITY
        tmp=obj.Imap(obj.vert,obj.hori);
        ratio(obj.j)=tmp(:)./obj.MapkernArea;

        %VALID IF DENSITY REQUIREMENT MET
        if ratio(obj.j)>obj.zoneMinDensity
            obj.valInd=vertcat(obj.valInd, obj.j);
        end
    end

    function obj=vet_by_slice(obj)
        obj.get_window();
        if obj.continueFlag==1; return; end

        obj.get_window_CP();
        if obj.continueFlag==1; return; end

        try
            CPpatchMap=Omap(obj.vertCP,obj.horiCP);
        catch
            return
        end

    %DO NOT INCLUDE PATCHES WITH NANS IN RANGE
        if any(isnan(obj.Ixyz(obj.vert,obj.hori)))
            return % XXX
        end
    end

    function obj=vet_by_super_slice(obj)
    % see nMapbuffer & nMap
    % NOTE this method takes much longer

        obj.check_sig_zones_CP();
        if obj.continueFlag==1; return; end

        obj.check_sig_zones();
        if obj.continueFlag==1; return; end

        obj.get_central_pixel();
        if obj.continueFlag==1; return; end

        obj.get_central_object();
        if obj.continueFlag==1; return; end

        obj.check_branching();
        if obj.continueFlag==1; return; end

        obj.check_width();
        if obj.continueFlag==1; return; end

        obj.check_neighboring();
        if obj.continueFlag==1; return; end

    %VALID
        obj.valInd=vertcat(obj.valInd, obj.j);
    end

    function obj=vet_by_slice_part2(obj)
        %COUNT NUMBER OF ROWS THAT ARE VALID
        count=0;
        exitflag=0;
        lastChng=[];
        for i = 1 % XX  X
            %GET WIDTH
            % XXX v ?
            [indChng,runVal,runCnt]=runLengthEncoder(obj.Imap(obj.vert(obj.v),obj.hori));
            width=runCnt(runVal==1);

            %DON'T INCLUDE MULTIPLE HALF OCCLUSION ZONES
            ind1=runVal==1;
            ind0=runVal==0;
            if sum(ind1) > 1 || sum(ind0) > 2
                exitflag=1;
                break
            end

            %INSURE Map ZONE CONTINUITY
            ind=find(runVal==1);
            curChng=[indChng(ind):indChng(ind+1)];
            if ~isempty(lastChng) && ~any(ismember(curChng,lastChng))
                exitflag=1;
                break
            end
            lastChng=curChng;

            %CHECK IF MEETS WIDTH REQUIREMENT
            if obj.bTwoWay==0 && width>=obj.minMaxPixMap-obj.zoneBufferRC(2)
                count=count+1;
            elseif obj.bTwoWay==1 && width>=obj.minMaxPixMap-obj.zoneBufferRC(2) && width<=obj.minMaxPixMap+obj.zoneBufferRC(2)
                count=count+1;
            end
        end
        if exitflag==1
            continue
        end

        % IF COUNT IS LARGE ENOUGH, CONSIDER AS VALID
        if count<obj.PszXY(2)-obj.zoneBufferRC(1)
            continue
        end

        % VALID
        obj.valInd=vertcat(obj.valInd, obj.j);
    end

    %% --------------------------------------------------------------------
    function I = get_LRSI_image(obj,i)
        %LOAD PHOTO IMAGES, RARNGE IMAGES,
        try
            I = loadLRSIimagePacked(obj.imgNums(i),1,0,'PHT','img',obj.rootDTBdir,'XYZ2');
        catch
            if bWarn
                warning('daVinciZoneSamplePatchBatch: Reverting to old range data files');
            end
            I = loadLRSIimagePacked(obj.imgNums(i),1,0,'PHT','img',obj.rootDTBdir,[]);
        end
        bWarn=0;

%GET 'daVinci' IMAGE
        if strcmp(obj.mapType,'DVN')
            [I.Lmap,I.Rmap] = loadLRSIimageDVN(imgNums(i),'local',0,obj.rootDTBdir,'DVN3');
        end
    end


    function obj = get_center_or_edge(obj)
        for obj.j = 1:obj.IszRC(1)
            obj.get_center_or_edge_main()
        end

        %MAKE SURE THESE REGIONS ARE WITHIN RANGE
        obj.ImapCtrRC=obj.ImapCtrRC(obj.ImapCtrRC(:,2)<=IszRC(2) & obj.ImapCtrRC(:,1)<=obj.IszRC(1),:);

        %CONVERT TO INDECES
        obj.ImapCtrInd=sub2ind(obj.IszRC,obj.ImapCtrRC(:,1),obj.ImapCtrRC(:,2));
        obj.plot_center_or_edge();
    end
    function obj=get_center_or_edge_main()
        obj.continueFlag=0;

        %COUNT RUNS OF HALF OCCLUDED PIXELS
        [indChg,runVal,runCnt]=runLengthEncoder(obj.Imap(obj.j,:));

        %INEX OF RUN STARTS
        begs=indChg(1:end-1);
        begs=begs(runVal==1); %first Map pixel

        %WIDTHS OF RUNS
        width=runCnt(runVal==1);

        %INDEX OF RUN ENDS
        ends=begs+width-1;  %Last Map pixel
                            %minus 1 because begs is index 1

        %CONDITION WIDTHS AND BEGINNINGS ON BEING LARGER THAN minPixMap
        valInd = (width>=obj.minMaxPixMap(1) & width<=obj.minMaxPixMap(2));
        begs=begs(valInd);
        ends=ends(valInd);
        width=width(valInd);

        %MID POINT OF RUN
        %XXX
        ctr=floor(width./2+1);

        if strcmp(obj.ctrORedg,'edg')

            %BEGININGS ARE VALID POINTS - GET THEIR SUBSCRIPTS FROM BEGS/ENDS
            if strcmp(obj.LorR,'L')
                obj.ImapCtrRC=[obj.ImapCtrRC; repmat(obj.j,length(begs),1), begs'];
            elseif strcmp(obj.LorR,'R')
                obj.ImapCtrRC=[obj.ImapCtrRC; repmat(obj.j,length(ends),1), ends'];
            end

        elseif strcmp(ctrORedg,'ctr')

            %CENTERS ARE VALID POINTS - GET THEIR SUBSCRIPTS FROM BEGS
            obj.ImapCtrRC=[obj.ImapCtrRC; repmat(obj.j,length(begs),1), begs'+ctr'];
        elseif strcmp(obj.ctrORedg,'fgnd')
            if strcmp(obj.LorR,'L')
                obj.ImapCtrRC=[obj.ImapCtrRC; repmat(obj.j,length(ends),1), ends'+1];
            elseif strcmp(LorR,'R')
                obj.ImapCtrRC=[obj.ImapCtrRC; repmat(obj.j,length(begs),1), begs'-1];
            end
        else
            error('Unhandled ctrORedg value');
        end

        if isempty(ctr)
            obj.continueFlag=1;
        else
            obj.widthAll=vertcat(obj.widthAll, width');
        end
    end

    function obj=get_CPs(obj)
        [obj.ImapCtrRC(:,1),obj.ImapCtrRC(:,2)]=ind2sub(obj.IszRC,obj.ImapCtrInd);
        [obj.LitpRC,obj.RitpRC,~,obj.LitpRCchk,obj.RitpRCchk] = LRSIcorrespondingPointVec([],obj.LorR,obj.ImapCtrRC,obj.Lxyz,obj.Rxyz,obj.LppXm,obj.LppYm,obj.RppXm,obj.RppYm,0,[],obj.ML,obj.MR);

        if strcmp(obj.LorR,'L')
            obj.BitpRC=obj.RitpRC;
    %adj=1;
        elseif strcmp(obj.LorR,'R')
            obj.BitpRC=obj.LitpRC;
    %adj=-1;
        end
    end
    % --------------------------------------------------------------------

    function obj=get_window(obj)
        obj.continueflag=0;

        %GET ALL INDECES OF POTENTIAL PATCH
        obj.vert=cell2mat(arrayfun(@(x) x-obj.MapkernRadHW(1):x+obj.MapkernRadHW(1),ImapCtrRC(j,1),'UniformOutput',false));
        obj.hori=cell2mat(arrayfun(@(x) x-obj.MapkernRadHW(2):x+obj.MapkernRadHW(2),ImapCtrRC(j,2),'UniformOutput',false));
        obj.vert=reshape(obj.vert,numel(obj.vert),1);
        obj.hori=reshape(obj.hori,numel(obj.hori),1);

        %DO NOT INCLUDE PATCHES OUTSIDE OF RANGE
        if any(obj.vert<=0) || any(obj.hori<=0) || max(obj.vert)>=obj.IszRC(1) || max(obj.hori)>=obj.IszRC(2)
            obj.continueFlag=1
        end
    end

    function obj=get_window_CP(obj)
        obj.continueFlag=0

        %GET ALL INDECES OF NON-ANCHOR CORRESPONDING PATCH
        obj.vertCP=round(cell2mat(arrayfun(@(x) x-obj.MapcpKernRadHW(1):x+obj.MapcpKernRadHW(1),obj.BitpRC(obj.j,1),'UniformOutput',false)));
        obj.horiCP=round(cell2mat(arrayfun(@(x) x-obj.MapcpKernRadHW(2):x+obj.MapcpKernRadHW(2),obj.BitpRC(obj.j,2),'UniformOutput',false)));
        obj.vertCP=reshape(obj.vertCP,numel(obj.vertCP),1);
        obj.horiCP=reshape(obj.horiCP,numel(obj.horiCP),1);

        %REMOVE INVALID SUBSCRIPTS
        obj.vertCP(obj.vertCP<0)=[];
        obj.horiCP(obj.horiCP<0)=[];
        obj.vertCP(obj.vertCP>obj.IszRC(1))=[];
        obj.horiCP(obj.horiCP>obj.IszRC(2))=[];

        %DO NOT INCLUDE PATCHES WITH INVALID _CORRESPONDING_ PATCHES
        if isempty(obj.vertCP) || isempty(obj.horiCP)
            continueFlag=1;
        end
    end

    function obj=get_central_object(obj)
    %QUERY ONLY THE CENTRAL OBJECT
        continueFlag=0;

        O=CC.PixelIdxList{obj.N(obj.co)};
        obj.Object=obj.blank;
        try
            obj.Object(O)=1;
        catch ME
            size(obj.Object)
            size(obj.Imap(obj.vertCP,obj.horiCP))
            size(obj.Imap(obj.vert,obj.hori))
            max(O)
            rethrow(ME)
            %continueFlag=1; % XXX ?
        end
    end

    function obj=get_central_pixel(obj)
    %FIND MOST CENTRAL Map ZONE
        obj.continueFlag=0;
        obj.co=[];
        for i = 1:length(obj.N)
            if ismember(obj.PctrInd,CC.PixelIdxList{obj.N(i)})
                obj.co=i;
                break
            end
        end

    %IF MOST CENTRAL ZONE IS INSIGNIFICANT, DISCARD
        if isempty(obj.co)
            obj.continueFlag=1;
        end
    end

    function obj=get_plot_flag(obj,k)
        if obj.bPlot==1 && any(obj.plotImgNum==obj.imgNums(i))
			if     strcmp(obj.LorRorB,'B') && k==1
            %PLOTTING L of BOTH LorR
                obj.plotFlag= 2;
			elseif strcmp(obj.LorRorB,'B') && k==2
            %PLOTTING R of BOTH LorR
                obj.plotFlag= 3;
			else
            %PLOTTING JUST 1 LorR
                obj.plotFlag= 1;
			end
		else
			obj.plotFlag=0;									 	 %DONT PLOT
		end
    end

    % --------------------------------------------------------------------
    function obj=check_sig_zones(obj)
    %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT Map ZONES IN CPs

        obj.continuFlag=0;

        %GET NUMBER OF SIGNIFICANT Map ZONES IN ANCHOR PATCH
        CC=bwconncomp(obj.Imap(obj.vert,obj.hori));
        numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of Map regions
        obj.N=find(numels>nMapbuffer); %number of significant Map regions

        %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT Map ZONES
        if length(obj.N)>obj.nMap
            continueFlag=1;
        end
    end

    function obj=check_sig_zones_CP(obj)
        obj.continuFlag=0;

        %GET NUMBER OF SIGNIFICANT Map ZONES IN NON-ANCHOR PATCH
        CC=bwconncomp(obj.Imap(obj.vertCP,obj.horiCP));
        numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of Map regions
        obj.N=find(numels>obj.nMapbuffer); %number of significant Map regions

        %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT Map ZONES
        if length(obj.N)>obj.nMap
            continueFlag=1;
        end
    end

    function obj=check_branching(obj)
    %DISCARD PATCHES WITH BRANCHING CENTRAL Map ZONES
        obj.continuFlag=0;
        [~,~,indOneChg]=runLengthEncoderVec(obj.Object,2);
        if any(cellfun(@(x) length(x)>1,indOneChg))
            obj.continueFlag=1;
        end
    end

    function obj=check_width(obj)
    %DISCARD PATCHES THAT DO NOT MEET WIDTH REQUIREMENTS FOR CENTRAL OBJECT
        obj.continuFlag=0;
        widths=sum(obj.Object,2);
        if obj.bTwoWay==1
            %COUNT NUMBER OF ROWS THAT MEET WIDTH REQ
            count=sum(widths>=(obj.minMaxPixMap(1)-obj.zoneBufferRC(2)) & widths <= (obj.minMaxPixMap(2)+obj.zoneBufferRC(2)));
        elseif obj.bTwoWay==0
            %COUNT NUMBER OF ROWS THAT MEET WIDTH REQ
            count=sum(widths>=(obj.minMaxPixMap(1)-obj.zoneBufferRC(2)));
        end

        if count<obj.PszXY(2)-obj.zoneBufferRC(1)
            continueFlag=1;
        end
    end

    function obj=check_neighboring(obj)
    %DISCARD PATCHES THAT HAVE A SINFICIANT ZONE HORIZONTALLY TOO CLOSE TO THE CENTRAL ZONE
        obj.continueFlag=0;

        if length(obj.N)<=1
            return
        end

        NC=obj.N(obj.co);
        obj.N(obj.co)=[];

        %GET FURTHEST EDGES OF CENTRAL Map ZONES
        [~,cc]=ind2sub(obj.PszRC,NC);
        cc=minmax(cc);

        %GET FURTHEST EDGES OF OTHER Map ZONES
        c = cellInd2sub(obj.PszRC,CC.PixelIdxList(obj.N));
        c = vertcat(c{:});

        if any(c < (cc(1)-obj.minZoneSep) & c >(cc(2))+obj.minZoneSep)
            continueFlag=1;
        end
    end
    % =================================================================
function P = batch_pack(obj,i,k,P,Ptmp)
    % COUNT NUMBER OF SAMPLES
    P.maxSmp(i,k)=size(obj.IctrRC,1);
    if k==1
        P.figInd(i)=imgNum(i);
    end

    % SORT STRUCT BASED UPON ANCHOR EYE
    if strcmp(LorRorB,'B')
        mL=(i-1).*nSmpPerImg*2+(1:nSmpPerImg)*2-1; %odd positions
        mR=(i-1).*nSmpPerImg*2+(1:nSmpPerImg)*2;
    elseif strcmp(LorRorB,'R') || strcmp(LorRorB,'L')
        mL=(i-1).*nSmpPerImg*2+(1:nSmpPerImg);
        mR=(i-1).*nSmpPerImg*2+(1:nSmpPerImg);
    end
    if LandR{k}=='L'
        %non-occluded half from left anchor
        P.LctrCrp(mL,:)         = Ptmp.LctrCrp;
        P.LitpRCchkAll(mL,:)    = Ptmp.LitpRCchk;
        P.LitpRCchkDspAll(mL,:) = Ptmp.LitpRCchkDsp;
        P.LitpRCall(mL,:)       = Ptmp.LitpRC;
        P.LitpRCdspAll(mL,:)    = Ptmp.LitpRCdsp;
        P.LphtCrpAll(:,:,mL)    = Ptmp.LphtCrp;
        P.LphtCrpZerAll(:,:,mL) = Ptmp.LphtCrpZer;
        P.LrngCrpAll(:,:,mL)    = Ptmp.LrngCrp;
        P.LxyzCrpAll(:,:,:,mL)  = Ptmp.LxyzCrp;
        P.LmapCrpAll(:,:,mL)    = Ptmp.LmapCrp;
        P.LppXmCrpAll(:,:,mL)   = Ptmp.LppXmCrp;
        P.LppYmCrpAll(:,:,mL)   = Ptmp.LppYmCrp;
        %occluded half from left anchor
        P.RctrCrp(mL,:)         = Ptmp.RctrCrp;
        P.RitpRCchkAll(mL,:)    = Ptmp.RitpRCchk;
        P.RitpRCchkDspAll(mL,:) = Ptmp.RitpRCchkDsp;
        P.RitpRCall(mL,:)       = Ptmp.RitpRC;
        P.RitpRCdspAll(mL,:)    = Ptmp.RitpRCdsp;
        P.RphtCrpAll(:,:,mL)    = Ptmp.RphtCrp;
        P.RphtCrpZerAll(:,:,mL) = Ptmp.RphtCrpZer;
        P.RrngCrpAll(:,:,mL)    = Ptmp.RrngCrp;
        P.RxyzCrpAll(:,:,:,mL)  = Ptmp.RxyzCrp;
        P.RmapCrpAll(:,:,mL)    = Ptmp.RmapCrp;
        P.RppXmCrpAll(:,:,mL)   = Ptmp.RppXmCrp;
        P.RppYmCrpAll(:,:,mL)   = Ptmp.RppYmCrp;
        %center
        P.CitpRCchkDspAll(mL,:) = Ptmp.CitpRCchkDsp;
        P.CitpRCdspAll(mL,:)    = Ptmp.CitpRCdsp;
    elseif LandR{k}=='R'
        %occluded half from left anchor
        P.LctrCrp(mR,:)         = Ptmp.LctrCrp;
        P.LitpRCchkAll(mR,:)    = Ptmp.LitpRCchk;
        P.LitpRCchkDspAll(mR,:) = Ptmp.LitpRCchkDsp;
        P.LitpRCall(mR,:)       = Ptmp.LitpRC;
        P.LitpRCdspAll(mR,:)    = Ptmp.LitpRCdsp;
        P.LphtCrpAll(:,:,mR)    = Ptmp.LphtCrp;
        P.LphtCrpZerAll(:,:,mR) = Ptmp.LphtCrpZer;
        P.LrngCrpAll(:,:,mR)    = Ptmp.LrngCrp;
        P.LxyzCrpAll(:,:,:,mR)  = Ptmp.LxyzCrp;
        P.LmapCrpAll(:,:,mR)    = Ptmp.LmapCrp;
        P.LppXmCrpAll(:,:,mR)   = Ptmp.LppXmCrp;
        P.LppYmCrpAll(:,:,mR)   = Ptmp.LppYmCrp;

        %non-occluded half from left anchor
        P.RctrCrp(mR,:)         = Ptmp.RctrCrp;
        P.RitpRCchkAll(mR,:)    = Ptmp.RitpRCchk;
        P.RitpRCchkDspAll(mR,:) = Ptmp.RitpRCchkDsp;
        P.RitpRCall(mR,:)       = Ptmp.RitpRC;
        P.RitpRCdspAll(mR,:)    = Ptmp.RitpRCdsp;
        P.RphtCrpAll(:,:,mR)    = Ptmp.RphtCrp;
        P.RphtCrpZerAll(:,:,mR) = Ptmp.RphtCrpZer;
        P.RrngCrpAll(:,:,mR)    = Ptmp.RrngCrp;
        P.RxyzCrpAll(:,:,:,mR)  = Ptmp.RxyzCrp;
        P.RmapCrpAll(:,:,mR)    = Ptmp.RmapCrp;
        P.RppXmCrpAll(:,:,mR)   = Ptmp.RppXmCrp;
        P.RppYmCrpAll(:,:,mR)   = Ptmp.RppYmCrp;
        %center
        P.CitpRCchkDspAll(mR,:) = Ptmp.CitpRCchkDsp;
        P.CitpRCdspAll(mR,:)    = Ptmp.CitpRCdsp;
    end
    function P=batch_finalize(obj,P)
        %% FIND INVALID INDECES (ANY SUBSCRIPT/INDEX EQUAL TO 1)
        validAll=squeeze( any(P.LitpRCchkDspAll,2)  &  any(P.RitpRCchkDspAll,2) );

        %ASSESS NUMBER OF SAMPLES ACCORDING TO nSmp IF SET
        if exist('nSmp','var')==1 && ~isempty(nSmp)
            smpExtra=sum(validAll)-nSmp;
            if smpExtra<0
                %WARN IF INSUFFICIENT SAMPLES
                disp(['Warning: with paramaters provided, you are short ' num2str(abs(smpExtra)) ' samples!']);
                disp('    Increase sample buffer or sampling provide more leaniest sampleing conditions');
            else
                %REMOVE EXTRA SAMPLES RANDOMLY
                ind=find(validAll);
                rmInd=datasample(ind,smpExtra,'Replace',false);
                validAll(rmInd)=0;
            end
        end

        %REMOVE INVALID PATCHES AND INDECES
        P.imgNumAll             = P.imgNumAll(validAll);
        P.LorRall               = P.LorRall(validAll);
        P.LctrCrp               = P.LctrCrp(validAll,:);
        P.RctrCrp               = P.RctrCrp(validAll,:);
        P.LitpRCchkDspAll       = P.LitpRCchkDspAll(validAll,:);
        P.RitpRCchkDspAll       = P.RitpRCchkDspAll(validAll,:);
        P.CitpRCchkDspAll       = P.CitpRCchkDspAll(validAll,:);
        P.LitpRCdspAll          = P.LitpRCdspAll(validAll,:);
        P.RitpRCdspAll          = P.RitpRCdspAll(validAll,:);
        P.CitpRCdspAll          = P.CitpRCdspAll(validAll,:);
        P.LitpRCchkAll          = P.LitpRCchkAll(validAll,:);
        P.RitpRCchkAll          = P.RitpRCchkAll(validAll,:);
        P.LitpRCall             = P.LitpRCall(validAll,:);
        P.RitpRCall             = P.RitpRCall(validAll,:);
        P.LphtCrpAll            = P.LphtCrpAll(:,:,validAll);
        P.LphtCrpZerAll         = P.LphtCrpZerAll(:,:,validAll);
        P.RphtCrpAll            = P.RphtCrpAll(:,:,validAll);
        P.RphtCrpZerAll         = P.RphtCrpZerAll(:,:,validAll);
        P.LrngCrpAll            = P.LrngCrpAll(:,:,validAll);
        P.RrngCrpAll            = P.RrngCrpAll(:,:,validAll);
        P.LmapCrpAll            = P.LmapCrpAll(:,:,validAll);
        P.RmapCrpAll            = P.RmapCrpAll(:,:,validAll);
        P.LxyzCrpAll            = P.LxyzCrpAll(:,:,:,validAll);
        P.RxyzCrpAll            = P.RxyzCrpAll(:,:,:,validAll);
        P.RppXmCrpAll           = P.RppXmCrpAll(:,:,validAll);
        P.RppYmCrpAll           = P.RppYmCrpAll(:,:,validAll);
        P.LppXmCrpAll           = P.LppXmCrpAll(:,:,validAll);
        P.LppYmCrpAll           = P.LppYmCrpAll(:,:,validAll);

        % XXX convert to patches_Map class
    end

    % =================================================================
    function obj=plot()
    % HANDLE ALL PLOTTING
        if obj.bPlotCtrORedg==1
            obj.plot_center_or_edge(9);
        end

        obj.titl={'Blue - daVinci region',...
            ['White - pixels satisfying minMaxPixMap=' num2str(minMaxPixMap) ],...
            ['Red - pixels satisfying zoneTestType=' ctrORedg ' and not too close to edges'],...
            ['Yellow - pixels at center of sampled non-overlapping patches']
            };

        num=10;
        if obj.plotFlag=0
            return
        elseif obj.plotFlag==1
            img=obj.Ipht.^.5;
            obj.plot_fun_anchor(num,img);
        elseif obj.plotFlag==2;
            img=[obj.Lpht,obj.Rpht].^.5;
            obj.plot_fun_anchor(num,img);
        elseif obj.plotFlag==3;
            obj.plot_=fun_nanchor(num);
        end
        if ~isempty(PctrRC)
            obj.plot_valid()
            obj.plot_sampled()
        end
        if plotFlag>0
            obj.plot_sampled_squares(11)
        end
    end

    %% PLOTING
    function obj=plot_fun_anchor(obj,num)
    %PLOT VALID POINTS (BLUE & WHITE)
        obj.figVet=figure(num);
        hold on

        imagesc()
        [U,V]=find(Imap==1);
        idx=ismember([obj.ImapCtrRC(:,1),obj.ImapCtrRC(:,2)],[U,V],'rows');
        scatter(V,U,'b.');
        scatter(ImapCtrRC(idx,2),ImapCtrRC(idx,1),'w.');

        formatFigure('','',obj.titl);
        formatImage();
        set(gcf,'position',[5  801  1815  537])
        hold off

    end

    function obj=plot_fun_nanchor(obj,num)
    %PLOT Map REGIONS (BLUE)
        obj.figVet=figure(num);
        hold on

        [U,V]=find(obj.Imap==1);
        idx=ismember([obj.ImapCtrRC(:,1),obj.ImapCtrRC(:,2)],[U,V],'rows');
        scatter(V+obj.IszRC(2),U,'b.')
        scatter(obj.ImapCtrRC(idx,2)+obj.IszRC(2),obj.ImapCtrRC(idx,1),'w.');

        hold off
    end

    function obj=plot_valid(obj,num)
    %PLOT VALID POINTS (RED)
        obj.figVet=figure(num);
        hold on

        ind=obj.PctrRC(:,2)<=obj.IszRC(2) & obj.PctrRC(:,2)>0 & obj.PctrRC(:,1)<=obj.IszRC(1) & PctrRC(:,1)>0;
        if obj.plotFlag==3
            scatter(obj.PctrRC(ind,2)+obj.IszRC(2),obj.PctrRC(ind,1),'r.');
        else
            scatter(obj.PctrRC(ind,2),obj.PctrRC(ind,1),'r.');
        end
        hold off
    end

    function obj=plot_sampled(obj,num)
    %PLOT SAMPLED POINTS (YELLOW)
        obj.figVet=figure(num);
        hold on

        if obj.plotFlag==3 && any(~isempty(obj.IctrRC))
            scatter(obj.IctrRC(:,2)+obj.IszRC(2),obj.IctrRC(:,1),'y','filled');
        elseif any(~isempty(obj.IctrRC))
            scatter(obj.IctrRC(:,2),obj.IctrRC(:,1),'y','filled');
        end
        hold off
    end

    function obj=plot_center_or_edge(obj,num)
        obj.figCtrOrEdg=figure(num);
        hold on

        imagesc(obj.Imap.^.5); axis image; colormap gray; hold on
        set(gca,'xtick',[]); set(gca,'ytick',[]);
        scatter(obj.ImapCtrRC(:,2),obj.ImapCtrRC(:,1),'r.');
    end
% -----------------------------------------------------------------
    function obj=plot_sampled_squares(obj,num)
        % XXX
        if strcmp(LorR,'L')
            figH3=30;
            figH4=40;
        elseif strcmp(LorR,'R')
            figH3=31;
            figH4=41;
        end

        if obj.j==1
            %PLOT EXAMPLE PATCHES
            fig3=figure(figH3);
            set(gcf,'position',[425 315  1030  414])

            %GET PATCHES BEFORE ADDITION OF DISPARITY

            LphtCrpChkOld = cropImageCtrInterp(Lpht,LitpRC(j,:),PszXY,interpType);
            RphtCrpChkOld = cropImageCtrInterp(Rpht,RitpRCchk(j,:),PszXY,interpType);

            Imin=min(min([LphtCrpChkOld, RphtCrpChkOld, P.LphtCrp(:,:,j), P.RphtCrp(:,:,j)].^.5));
            Imax=max(max([LphtCrpChkOld, RphtCrpChkOld, P.LphtCrp(:,:,j), P.RphtCrp(:,:,j)].^.5));

            %DISPLAY L EXAMPLE PATCH BEFORE SHIFTING
            subplot(2,1,1);
            imagesc([LphtCrpChkOld RphtCrpChkOld LphtCrpChkOld].^.5)
            caxis([Imin Imax]);
            colormap gray;
            axis image
            set(gca,'xtick',[]); set(gca,'ytick',[]); set(gca,'YDir','reverse');

            title('L R L: virtual eyes fixated on background')

            %DISPLAY L EXAMPLE PATCH AFTER SHIFTING
            subplot(2,1,2);
            imagesc([P.LphtCrp(:,:,j) P.RphtCrp(:,:,j) P.LphtCrp(:,:,j)].^.5)
            caxis([Imin Imax]);
            colormap gray;
            axis image
            set(gca,'xtick',[]); set(gca,'ytick',[]); set(gca,'YDir','reverse');set(gca,'YDir','reverse');
        if strcmp(fgndORbgnd,'fgnd')
                title(['L R L: virtual eyes fixated on foreground plus added disparity (\delta=' num2str(dspArcMin) ')'])
        elseif strcmp(fgndORbgnd,'bgnd')
                title(['L R L: virtual eyes fixated on background plus added disparity (\delta=' num2str(dspArcMin) ')'])
        end

        end

        %PLOT ALL PATCHES FOR SPECIFIED IMAGE
        fig4=figure(figH4);
        set(gcf,'position',[ 5  801  1815 537]);

        %PLOT PHOTOS
        if j == 1
            imagesc([Lpht,Rpht].^.5); axis image; colormap gray; hold on
            set(gca,'xtick',[]); set(gca,'ytick',[]);
            set(gca,'YDir','reverse');set(gca,'YDir','reverse');
        else
            hold on
        end

        % SCATTER IMAGE CENTERS LEFT
        scatter(LitpRC(j,2)      ,LitpRC(j,1)      ,'y','filled'); hold on %LEFT
        if strcmp(fgndORbgnd,'fgnd')
            scatter(LitpRCchkDsp(2),LitpRCchkDsp(1),'g','filled');
        elseif strcmp(fgndORbgnd,'bgnd')
            scatter(LitpRCdsp(2),LitpRCdsp(1),'g','filled');
        end

        %SCATTER IMAGE CENTERS RIGHT
        scatter(RitpRC(j,2)      +size(Lpht,2),RitpRC(j,1)      ,'y','filled'); %LEFT
        if strcmp(fgndORbgnd,'fgnd')
            scatter(RitpRCchkDsp(2)+size(Lpht,2),RitpRCchkDsp(1),'g','filled'); %RIGHT
        elseif strcmp(fgndORbgnd,'bgnd')
            scatter(RitpRCdsp(2)+size(Lpht,2),RitpRCdsp(1),'g','filled'); %RIGHT
        end

        %PLOT BORDERS LEFT
        plotSquare( fliplr(LitpRC(j,:)),      PszXY,'y',2,'-') %PATCH BORDERS LEFT EYE, BEFORE SHIFTING
        if strcmp(fgndORbgnd,'fgnd')
            plotSquare( fliplr(LitpRCchkDsp),PszXY,'g',2,'-') %PATCH BORDERS LEFT EYE, AFTER SHIFTING
        elseif strcmp(fgndORbgnd,'bgnd')
            plotSquare( fliplr(LitpRCdsp),PszXY,'g',2,'-') %PATCH BORDERS LEFT EYE, AFTER SHIFTING
        end


        %PLOT BORDERS RIGHT
        plotSquare( [ RitpRC(j,2)+size(Lpht,2),        RitpRC(j,1)],      PszXY,'y',2,'-') %PATCH BORDERS RIGHT EYE, BEFORE SHIFTING
        if strcmp(fgndORbgnd,'fgnd')
            plotSquare( [ RitpRCchkDsp(2)+size(Lpht,2),RitpRCchkDsp(1)],PszXY,'g',2,'-') %PATCH BORDERS RIGHT EYE, AFTER SHIFTING
        elseif strcmp(fgndORbgnd,'bgnd')
            plotSquare( [ RitpRCdsp(2)+size(Lpht,2),RitpRCdsp(1)],PszXY,'g',2,'-') %PATCH BORDERS RIGHT EYE, AFTER SHIFTING
        end

        if strcmp(fgndORbgnd,'fgnd')
            title({'Sampled Patches','Yellow - virtual eyes fixated on background',['Green - virtual eyes fixated on foreground plus added disparity (\delta=' num2str(dspArcMin) ')']});
        elseif strcmp(fgndORbgnd,'bgnd')
            title({'Sampled Patches','Yellow - virtual eyes fixated on background',['Green - virtual eyes fixated on background plus added disparity (\delta=' num2str(dspArcMin) ')']});
        end
    end
    % --------------------------------------------------------------------

    % SAVE FIGURES XXX
    function obj=save_figures(obj)
        if obj.bSaveFig
            saveas(obj.fig1,['tmp1.png']);
            saveas(obj.fig2,['tmp2.png']);
            saveas(obj.fig3,['tmp3.png']);
        end
    end

    % XXX
    function obj=extra(obj)
        if obj.bExtra == 1
            P = patches2FixedContrastIntensityImages([],P,DC,RMS,W,bByImage,monoORbino); %CREATE FIXED CONTRAST INTENSITY IMAGES FOR PSYCHOPYSHCIS EXPERIMNETS
            P = halfOcclusionPatchCPsBatch(P,PszXY,dspArcMin,stmXYdeg,fgndORbgnd,b2ctr,bPreDispDsp,hostName,rootDTBdir,0); %CREATE CP MAPS AND OPTIONALLY DISPARITY & DEPTH MAPS
            P = daVinciDivideZones2(P,0); %LABEL ZONES (BINOFG ETC)
            %P = patches2stats(P);         %COMPUTE STATS FOR INDIVIDUAL ZONES
            P = daVinciPatchWidths(P);    %COMPUTE PATCH WIDTHS
        end
    end

end
end
