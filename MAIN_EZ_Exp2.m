% --------------------------------------------------------------------
% README
% %    INSTRUCTIONS:
%        You do NOT need to add anything to your run path.
%        See "NOTE"s throughout code
%        See MAIN and functions in this directory for additional details
%        Functions in the 'functions' directory are dependencies, but are not be required to understand.
%
%    RUNNING
%        1. Download the database and place in this directory (or a symlink to it)
%        2. Change the USER SPECIFIED variables in the main script MAIN
%        3. Simply run the main script with the full path specified
%           eg:
%                /home/dambam/Code/Dave/collab/MAIN
%
% --------------------------------------------------------------------
% THIS SCRIPT USES 2 MAIN FUNCTIONS
%   daVinciZoneSample  - sample and vet potential patch centers
%   daVinciZonePatches - create patch centers from vetted patch centers
% WHICH ARE PROCESSED USING 2 BATCH PROCESSING SCRIPTS
%   daVinciZoneSamplePatchBatch      - create patches at a given disparity and daVinci zone width
%   daVinciZoneSamplePatchBatchBatch - create all patches for all specified disparities and widths
% --------------------------------------------------------------------
%% USER SPECIFIED VARIABLES
%  SEE daVinciZoneSamplePatchBatchBatch for details on variables

%MAKE SURE TO SET THESE
rootDTBdir='/Users/zeynobi/Documents/Repos/halfocccollab';
saveDTB='/Users/zeynobi/Documents/Repos/BinocularRivalry/code';

bSave           = 1;

bPlot           = 0;
plotImgNum      = 8;

imgNum          = [1:7,9:35,37:98];
LorRorB         = 'B';

fgndORbgnd      = 'fgnd';
dspArcMinAll    = -5;
PszXY           = 1.5*[130 52];
rndSd           = 1;

ctrORedg        = 'edg';
minMaxPixDVNall = [15 35];
zoneTestType    = 'slice';

nSmp            = [];
nSmpPerImg      = 20;

overlapPix      = 10;

zoneBufferRCall(:,2) = minMaxPixDVNall(:,1)-5;
zoneBufferRCall(:,1) = zeros(size(minMaxPixDVNall,1),1);
zoneMinDensity       = .2;

%%%%%%%%%%%%%%%%
% CREATE PATCHES

% HANDLE DIRECTORY CHANGING AND DATABASE DIRECTORIES
mfname=mfilename('fullpath');
[oldPath,oldDir]=handlePath(mfname,rootDTBdir);

% RUN
try
  P = daVinciZoneSamplePatchBatchBatch(  ...
      'rootDTBdir', rootDTBdir,  ...
      'saveDTB', saveDTB,  ...
      'bSave', bSave,  ...
      'imgNum', imgNum,  ...
      'bPlot', bPlot,  ...
      'plotImgNum', plotImgNum,  ...
      'LorRorB', LorRorB,  ...
      'fgndORbgnd', fgndORbgnd,  ...
      'dspArcMinAll', dspArcMinAll,  ...
      'PszXY', PszXY,  ...
      'rndSd', rndSd,  ...
      'nSmpPerImg', nSmpPerImg,  ...
      'ctrORedg', ctrORedg,  ...
      'minMaxPixDVNall', minMaxPixDVNall,  ...
      'zoneTestType', zoneTestType,  ...
      'zoneBufferRCall', zoneBufferRCall,  ...
      'zoneMinDensity', zoneMinDensity)
catch ME
  if ~isempty(oldDir) && isempty(oldpath)
    cd(oldDir)
    path(oldPath)
  end
  rethrow(ME)
end

% REMOVE RESTORE OLD PATH AND DIRECTORY
    if ~isempty(oldDir) && isempty(oldPath)
      cd(oldDir);
      path(oldPath);
    end