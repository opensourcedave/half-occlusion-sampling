function [Pall,figAll1,figAll3,figAll4] = daVinciZoneSamplePatchBatchBatch(H)
% function [P] = daVinciZoneSamplePatchBatchBatch(H)
%
% example call:
%    P = daVinciSamplePatchBatchBatch();
%    P = daVinciSamplePatchBatchBatch(H); %**SEE daVinciZoneParamParse FOR DETAILS ON PARAMETERS
%
% Handle batch processing of batch patch creation
%    daVinciZoneSamplePatchBatch      - handle patch grabbing for given disparity and dvn width
%    daVinciZoneSamplePatchBatchBatch - Loops over all specified disparities and widths
% --------------------------------------------------------------------
% INPUT
%
% H - options struct: SEE 'daVinciZoneParamParse' FOR DETAILS ON FIELDS OF H
% --------------------------------------------------------------------
% OUTPUT
%
% Pall - cell of P structs
%        see 'daVinciSamplePatchBatch' for details on fields of P
% figAll1
% figAll2
% figAll3
% --------------------------------------------------------------------

% HANDLE INPUT
H=daVinciZoneParamParse(H,1);

%INIT CELL IF NOT SAVING
Pall=cell(size(H.dspArcMinAll,1),size(H.minMaxPixDVNall,1));

%LOOP OVER DISPARITY AND RANGES
bWarn=1;
for d = 1:size(H.dspArcMinAll,1)
for m = 1:size(H.minMaxPixDVNall,1)

    %VARIABLES SPECIFIC TO EACH P
    H.dspArcMin   =H.dspArcMinAll(d,:);
    H.minMaxPixDVN=H.minMaxPixDVNall(m,:);
    H.zoneBufferRC=H.zoneBufferRCall(m,:);

    %MAIN
    [P,figAll1,figAll2,figAll3] =daVinciZoneSamplePatchBatch(H,bWarn);
    bWarn=0;

    if H.bSave
        %SAVE PATCHES
        P.fname=daVinciPatchFnameHash(H);
        save(P.fname,'-struct','P');

        %SAVE FIGURES
        if ~isempty(figAll1) && ~isempty(figAll2) && ~isempty(figAll3) && H.bSaveFig
            hash=fliplr(strtok(fliplr(P.fname),'_'));
            save([ H.saveDTB filesep 'daVinciZoneFigures1_' hash],'figAll1');
            save([ H.saveDTB filesep 'daVinciZoneFigures2_' hash],'figAll2');
            save([ H.saveDTB filesep 'daVinciZoneFigures4_' hash],'figAll3');
        end
    end
    Pall{d,m}=P;
end
end
if numel(Pall)==1
    Pall=Pall{1};
end
