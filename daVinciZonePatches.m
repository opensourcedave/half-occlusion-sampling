function [P,indPerm,fig3,fig4] = daVinciZonePatches(imgNum,I,LorR,IctrRC,PszXY,dspArcMin,fgndORbgnd,nSmpPerImg,plotFlag,rootDTBdir)
%function [P] = daVinciZonePatches(imgNum,IctrRC,dspArcMin,fgndORbgnd,nSmpPerImg,PszXY,LorR,plotFlag,Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,Ldvn,Rdvn)
%
%  Make half occluded patches from preselected image centers
%
%  example call;
%       IctrRC=daVinciZoneSample(1,[130 52],'L',edg,18,'slice',[0 13],.2,1,1);
%       P=daVinciZonePatches(1,[130 52],'L',IctrRC,-3.5,'fgnd',20,1)
%
%
%  INPUTS
%  imgNum      - range of images to sue from    [numImages x 1]
%  PszXY       - desired horizontal by vertical patch size [1 x 2]
%  LorR        - what to use as an anchor
%                'L' -> only use left eyes as anchor
%                'R' -> only use right eye as anchor
%  IctrRC      - vedted patch centers [maxSmp x 2]
%  dspArcMin   - how much disparity in arcMinutes to add to stereo-patches
%  fgndORbgnd  - Whether to have cyclopian focus on the foreground or background
%  nSmpPerImg  - number of samples per image desired. Determined from nSmp if unspecified.
%  plotFlag - whether to plot and/or which part of the image to plot
%                0 -> do not plog
%                1 -> plot for only L or R anchor only (whatever is being used)
%                2 -> plot for Left anchor
%                3 -> plot for RIght anchor
%                2 & 3 are meant to be used in a loop
%  Lpht     - left luminance image            [PszXY(2) x PszXY(1) ]
%  Rpht     - right luminance image           [PszXY(2) x PszXY(1) ]
%  Lrng     - left range image                [PszXY(2) x PszXY(1) ]
%  Rrng     - right range image               [PszXY(2) x PszXY(1) ]
%  Lxyz     - left cartesian range image      [PszXY(2) x PszXY(1) x 3]
%  Rxyz     - right cartesian range image     [PszXY(2) x PszXY(1) x 3]
%  LppXm:   - x-position ( meters ) of pixels in '(p)rojection (p)lane' for left  eye coordinate system
%  LppYm:   - y-position ( meters ) of pixels in '(p)rojection (p)lane' for left  eye coordinate system
%  RppXm:   - x-position ( meters ) of pixels in '(p)rojection (p)lane' for right eye coordinate system
%  RppYm:   - y-position ( meters ) of pixels in '(p)rojection (p)lane' for right eye coordinate system
%  Ldvn     - left DaVinci image              [PszXY(2) x PszXY(1) ]
%  Rdvn     - right DaVinci image             [PszXY(2) x PszXY(1) ]
%
%  rootDTBdir      - root directory of database, exlcuding the 'LRSI' directory
%
%
%  OUTPUTS
%  P               - struct containing the following:
%          LitpRCchkDsp- Left image patch center after adding disparity.
%                            Also the 'check' version of LitpRCall, meaning it uses the opposite eye to
%                            find the corresponding point, and thus is centered on the occluding
%                            surface.
%                            [nSmp x 2]
%
%          RitpRCchkDsp- Right image patch center after adding disparity.
%                            Also the 'check' version of RitpRCall, meaning it uses the opposite eye to
%                            find the corresponding point, and thus is centered on the occluding
%                            surface.
%          LitpRCchk- Left image patch center before adding disparity
%          RitpRCchk- Right image patch center before adding diparity
%          LitpRCall       - Left image patch center of occluded region defined by daVinciZoneCenter
%                            if using left anchor, or found using LRSIcorrespondingPointVec if using
%                            right anchor
%          RitpRCall       - Right image patch center of occluded region defined by daVinciZoneCenter
%                            if using right anchor, or found using LRSIcorrespondingPointVec if using
%                            left anchor
%          LitpRCdsp- Right image patch center after adding disparity.
%          RitpRCdsp- Left image patch center after adding disparity.
%          LphtCrp- cropped luminance images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LphtCrpZer - cropped luminance images centered on LitpRCchk (no disparity added)
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LrngCrp- cropped range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LxyzCrp- cropped cartesian range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x 3 x nSmp]
%          LdvnCrp- cropped DaVinci images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LppXmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          LppYmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RphtCrp- cropped luminance images centered on LitpRCchkDspA
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RrngCrp- cropped range images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RxyzCrp- cropped cartesian range images centered on LitpRCc
%                            [PszXY(2) x PszXY(1) x 3 x nSmp]
%          RdvnCrp- cropped DaVinci images centered on LitpRCchkDsp
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RppXmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%          RppYmCrp-
%                            [PszXY(2) x PszXY(1) x nSmp]
%
% --------------------------------------------------------------------
unpackOpts(I);

%% INPUT HANDLING
if exist('plotFlag','var')~=1    || isempty(plotFlag);   plotFlag=0; end
if exist('fgndORbgnd','var')~=1  || isempty(fgndORbgnd); fgndORbgnd='fgnd'; end

if ~exist('Lpht','var')  || ~exist('Rpht','var')  || ...
   ~exist('Lrng','var')  || ~exist('Rrng','var')  || ...
   ~exist('Lxyz','var')  || ~exist('Rxyz','var')  || ...
   ~exist('LppXm','var') || ~exist('LppYm','var') || ...
   ~exist('RppXm','var') || ~exist('RppYm','var') || ...
   ~exist('Ldvn','var')  || ~exist('Rdvn','var')
    [Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(imgNum,1,0,'PHT','img',rootDTBdir,'XYZ2');
    [Ldvn,Rdvn] = loadLRSIimageDVN(imgNum,'local',0,rootDTBdir,'DVN2');
end

% --------------------------------------------------------------------
%% INIT
fig3=[];
fig4=[];
IszRC      = [1080 1920]; %DATABASE IMAGE DIMENSIONS
PszRC      = [PszXY(2) PszXY(1)];        %size of patch, rows columns
IppZm      = 3;
IPDm       = .065;                       %distance between eyes in meters
interpType = 'linear';

%PREALLOCATION
P.LitpRCchkDsp = zeros(nSmpPerImg,2);
P.RitpRCchkDsp = zeros(nSmpPerImg,2);
P.CitpRCchkDsp = zeros(nSmpPerImg,2);
P.LitpRCchk    = zeros(nSmpPerImg,2);
P.RitpRCchk    = zeros(nSmpPerImg,2);
P.LitpRC       = zeros(nSmpPerImg,2);
P.RitpRC       = zeros(nSmpPerImg,2);
P.LitpRCdsp    = zeros(nSmpPerImg,2);
P.RitpRCdsp    = zeros(nSmpPerImg,2);
P.CitpRCdsp    = zeros(nSmpPerImg,2);
P.LctrCrp      = zeros(nSmpPerImg,2);
P.RctrCrp      = zeros(nSmpPerImg,2);

P.LphtCrp    = zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.LphtCrpZer = zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.LrngCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.LxyzCrp= zeros(PszXY(2),PszXY(1),3,nSmpPerImg);
P.LdvnCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.LppXmCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.LppYmCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);

P.RphtCrp    = zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.RphtCrpZer = zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.RrngCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.RxyzCrp= zeros(PszXY(2),PszXY(1),3,nSmpPerImg);
P.RdvnCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.RppXmCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);
P.RppYmCrp= zeros(PszXY(2),PszXY(1),nSmpPerImg);

%% GET CORRESPONDING POINT OF ANCHOR EYE PATCH CENTER
[LitpRC,RitpRC,CitpRCdsp,LitpRCchk,RitpRCchk] = LRSIcorrespondingPointVec([],LorR,IctrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,0,'wall');

%HANDLE RANDOM SAMPLING OF POINTS -
%  LIMITED BY MAX, BUT NOT BY A MINIMUM
n=size(LitpRC,1);
if n>=nSmpPerImg
    limit=nSmpPerImg;
elseif n<nSmpPerImg
    limit=n;
end
indPerm      = randperm(n,limit)';
LitpRC       = LitpRC(indPerm,:);
LitpRCchk    = LitpRCchk(indPerm,:);
RitpRC       = RitpRC(indPerm,:);
RitpRCchk    = RitpRCchk(indPerm,:);
CitpRCdsp    = CitpRCdsp(indPerm,:);

% --------------------------------------------------------------------
%% SHIFT PATCH CENTERS TO ELIMATE CROSSED DISPARITY

%LOOP OVER SAMPLES
for j = 1:limit
    %IF OUT OF VALID POINTS, CONTINUE
    if isempty(LitpRCchk(j,:)) || isempty(RitpRCchk(j,:)); continue; end

    % --------------------------------------------------------------------
    %% ADD DISAPRITY

    [LitpRCchkDsp, RitpRCchkDsp]    = LRSIcorrespondingPointAddDisparityExact(LitpRCchk(j,:),RitpRCchk(j,:),dspArcMin,LppXm,LppYm,RppXm,RppYm,IppZm,IPDm);
    [LitpRCdsp, RitpRCdsp]          = LRSIcorrespondingPointAddDisparityExact(LitpRC(j,:),   RitpRC(j,:),   dspArcMin,LppXm,LppYm,RppXm,RppYm,IppZm,IPDm);

    % -------------------------------------------------------------------------------
    %% SKIP OVER INVALID CENTERS

    %SKIP OVER ANY NANS OR ZEROS
    if any(isnan(LitpRCchkDsp)) || any(LitpRCchkDsp==0) || any(isnan(RitpRCchkDsp)) || any(RitpRCchkDsp==0)
        continue
    end
    %DO NOT INCLUDE PATCHES OUTSIDE OF RANGE
    if (any(LitpRCchkDsp+floor(PszRC./2) > IszRC) || any(RitpRCchkDsp+floor(PszRC./2) > IszRC))
        continue
    end

    % --------------------------------------------------------------------
    %% CROP LRSI IMAGES

    if strcmp(fgndORbgnd,'fgnd')
        LctrCrp   =LitpRCchkDsp;
        RctrCrp   =RitpRCchkDsp;
        LctrCrpZer=LitpRCchk(j,:);
        RctrCrpZer=RitpRCchk(j,:);
    elseif strcmp(fgndORbgnd,'bgnd')
        LctrCrp   =LitpRCdsp;
        RctrCrp   =RitpRCdsp;
        LctrCrpZer=LitpRC(j,:);
        RctrCrpZer=RitpRC(j,:);
    end
    %try
        if ~exist('W','var') || ~isempty(W)
            P.LphtCrp(:,:,j)    = cropImageCtrInterp(Lpht, LctrCrp,   PszXY,interpType);
            P.RphtCrp(:,:,j)    = cropImageCtrInterp(Rpht, RctrCrp,   PszXY,interpType);
            P.LphtCrpZer(:,:,j) = cropImageCtrInterp(Lpht, LctrCrpZer,PszXY,interpType);
            P.RphtCrpZer(:,:,j) = cropImageCtrInterp(Rpht, RctrCrpZer,PszXY,interpType);
        else
            P.LphtCrp(:,:,j)    = cropImageCtrInterp(Lpht, LctrCrp,   PszXY,interpType).*W;
            P.RphtCrp(:,:,j)    = cropImageCtrInterp(Rpht, RctrCrp,   PszXY,interpType).*W;
            P.LphtCrpZer(:,:,j) = cropImageCtrInterp(Lpht, LctrCrpZer,PszXY,interpType).*W;
            P.RphtCrpZer(:,:,j) = cropImageCtrInterp(Rpht, RctrCrpZer,PszXY,interpType).*W;
        end

        % CROP RANGE IMAGES
        P.LrngCrp(:,:,j)   = cropImageCtrInterp(Lrng, LctrCrp,PszXY,interpType);
        P.RrngCrp(:,:,j)   = cropImageCtrInterp(Rrng, RctrCrp,PszXY,interpType);
        P.LxyzCrp(:,:,:,j) = cropImageCtrInterp(Lxyz, LctrCrp,PszXY,interpType);
        P.RxyzCrp(:,:,:,j) = cropImageCtrInterp(Rxyz, RctrCrp,PszXY,interpType);

        %MAKE DVN IMAGES COMPLETELY BINARY AND REMOVE NANs
        %Ldvn=round(Ldvn);
        %Rdvn=round(Rdvn);
        %Ldvn(isnan(Ldvn))=1;
        %Rdvn(isnan(Rdvn))=1;

        % CROP DAVINCI IMAGES
        P.LdvnCrp(:,:,j) = cropImageCtrInterp(Ldvn, LctrCrp,PszXY,interpType);
        P.RdvnCrp(:,:,j) = cropImageCtrInterp(Rdvn, RctrCrp,PszXY,interpType);

        % CROP PROJECTION PLANE IMAGES
        P.LppXmCrp(:,:,j) = cropImageCtrInterp(LppXm, LctrCrp,PszXY,interpType);
        P.RppXmCrp(:,:,j) = cropImageCtrInterp(RppXm, RctrCrp,PszXY,interpType);
        P.LppYmCrp(:,:,j) = cropImageCtrInterp(LppYm, LctrCrp,PszXY,interpType);
        P.RppYmCrp(:,:,j) = cropImageCtrInterp(RppYm, RctrCrp,PszXY,interpType);

        %SAVE CENTERS
        P.LitpRCchkDsp(j,:) = LitpRCchkDsp;
        P.RitpRCchkDsp(j,:) = RitpRCchkDsp;
        P.LitpRCdsp(j,:)    = LitpRCdsp;
        P.RitpRCdsp(j,:)    = RitpRCdsp;
        P.CitpRCdsp(j,:)    = CitpRCdsp(j,:);
        P.LitpRCchk(j,:)    = LitpRCchk(j,:);
        P.RitpRCchk(j,:)    = RitpRCchk(j,:);
        P.LitpRC(j,:)       = LitpRC(j,:);
        P.RitpRC(j,:)       = RitpRC(j,:);
        P.RctrCrp(j,:)      = RctrCrp;
        P.LctrCrp(j,:)      = LctrCrp;

    %catch
        % FOR RARE INTERPOLATION ERROR
        %continue
    %end

    % --------------------------------------------------------------------
    %% PLOTTING

    if plotFlag==1 || plotFlag==2 || plotFlag==3
         if strcmp(LorR,'L')
             figH3=30;
             figH4=40;
         elseif strcmp(LorR,'R')
             figH3=31;
             figH4=41;
         end

         if j==1
             %PLOT EXAMPLE PATCHES
             fig3=figure(figH3);
             set(gcf,'position',[425 315  1030  414])

             %GET PATCHES BEFORE ADDITION OF DISPARITY

             LphtCrpChkOld = cropImageCtrInterp(Lpht,LitpRC(j,:),PszXY,interpType);
             RphtCrpChkOld = cropImageCtrInterp(Rpht,RitpRCchk(j,:),PszXY,interpType);

             Imin=min(min([LphtCrpChkOld, RphtCrpChkOld, P.LphtCrp(:,:,j), P.RphtCrp(:,:,j)].^.5));
             Imax=max(max([LphtCrpChkOld, RphtCrpChkOld, P.LphtCrp(:,:,j), P.RphtCrp(:,:,j)].^.5));

             %DISPLAY L EXAMPLE PATCH BEFORE SHIFTING
             subplot(2,1,1);
             imagesc([LphtCrpChkOld RphtCrpChkOld LphtCrpChkOld].^.5)
             caxis([Imin Imax]);
             colormap gray;
             axis image
             set(gca,'xtick',[]); set(gca,'ytick',[]); set(gca,'YDir','reverse');

             title('L R L: virtual eyes fixated on background')

             %DISPLAY L EXAMPLE PATCH AFTER SHIFTING
             subplot(2,1,2);
             imagesc([P.LphtCrp(:,:,j) P.RphtCrp(:,:,j) P.LphtCrp(:,:,j)].^.5)
             caxis([Imin Imax]);
             colormap gray;
             axis image
             set(gca,'xtick',[]); set(gca,'ytick',[]); set(gca,'YDir','reverse');set(gca,'YDir','reverse');
            if strcmp(fgndORbgnd,'fgnd')
                 title(['L R L: virtual eyes fixated on foreground plus added disparity (\delta=' num2str(dspArcMin) ')'])
            elseif strcmp(fgndORbgnd,'bgnd')
                 title(['L R L: virtual eyes fixated on background plus added disparity (\delta=' num2str(dspArcMin) ')'])
            end

         end

        %PLOT ALL PATCHES FOR SPECIFIED IMAGE
        fig4=figure(figH4);
        set(gcf,'position',[ 5  801  1815 537]);

        %PLOT PHOTOS
        if j == 1
            imagesc([Lpht,Rpht].^.5); axis image; colormap gray; hold on
            set(gca,'xtick',[]); set(gca,'ytick',[]);
            set(gca,'YDir','reverse');set(gca,'YDir','reverse');
        else
            hold on
        end

        % SCATTER IMAGE CENTERS LEFT
        scatter(LitpRC(j,2)      ,LitpRC(j,1)      ,'y','filled'); hold on %LEFT
        if strcmp(fgndORbgnd,'fgnd')
            scatter(LitpRCchkDsp(2),LitpRCchkDsp(1),'g','filled');
        elseif strcmp(fgndORbgnd,'bgnd')
            scatter(LitpRCdsp(2),LitpRCdsp(1),'g','filled');
        end

        %SCATTER IMAGE CENTERS RIGHT
        scatter(RitpRC(j,2)      +size(Lpht,2),RitpRC(j,1)      ,'y','filled'); %LEFT
        if strcmp(fgndORbgnd,'fgnd')
            scatter(RitpRCchkDsp(2)+size(Lpht,2),RitpRCchkDsp(1),'g','filled'); %RIGHT
        elseif strcmp(fgndORbgnd,'bgnd')
            scatter(RitpRCdsp(2)+size(Lpht,2),RitpRCdsp(1),'g','filled'); %RIGHT
        end

        %PLOT BORDERS LEFT
        plotSquare( fliplr(LitpRC(j,:)),      PszXY,'y',2,'-') %PATCH BORDERS LEFT EYE, BEFORE SHIFTING
        if strcmp(fgndORbgnd,'fgnd')
            plotSquare( fliplr(LitpRCchkDsp),PszXY,'g',2,'-') %PATCH BORDERS LEFT EYE, AFTER SHIFTING
        elseif strcmp(fgndORbgnd,'bgnd')
            plotSquare( fliplr(LitpRCdsp),PszXY,'g',2,'-') %PATCH BORDERS LEFT EYE, AFTER SHIFTING
        end


        %PLOT BORDERS RIGHT
        plotSquare( [ RitpRC(j,2)+size(Lpht,2),        RitpRC(j,1)],      PszXY,'y',2,'-') %PATCH BORDERS RIGHT EYE, BEFORE SHIFTING
        if strcmp(fgndORbgnd,'fgnd')
            plotSquare( [ RitpRCchkDsp(2)+size(Lpht,2),RitpRCchkDsp(1)],PszXY,'g',2,'-') %PATCH BORDERS RIGHT EYE, AFTER SHIFTING
        elseif strcmp(fgndORbgnd,'bgnd')
            plotSquare( [ RitpRCdsp(2)+size(Lpht,2),RitpRCdsp(1)],PszXY,'g',2,'-') %PATCH BORDERS RIGHT EYE, AFTER SHIFTING
        end

        if strcmp(fgndORbgnd,'fgnd')
            title({'Sampled Patches','Yellow - virtual eyes fixated on background',['Green - virtual eyes fixated on foreground plus added disparity (\delta=' num2str(dspArcMin) ')']});
        elseif strcmp(fgndORbgnd,'bgnd')
            title({'Sampled Patches','Yellow - virtual eyes fixated on background',['Green - virtual eyes fixated on background plus added disparity (\delta=' num2str(dspArcMin) ')']});
        end
    end
    % --------------------------------------------------------------------
end
