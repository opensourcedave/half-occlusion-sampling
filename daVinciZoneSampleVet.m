function [PctrRC,PctrInd] = daVinciZoneSampleVet(IdvnCtrInd,PszXY,zoneTestType,zoneBufferRC,zoneMinDensity,minMaxPixDVN,bTwoWay,Irng,Idvn, Odvn,LorR,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,nDVNbuffer,nDVN,minZoneSep,ML,MR)
%function [PctrRC,PctrInd] = daVinciZoneSampleVet(IdvnCtrInd,LorR,PszXY,zoneTestType,zoneBufferRC,zoneMinDensity,minMaxPixDVN,Irng,Idvn,OdvnLorR,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm)
%
%   exampleCall:
%       [Lpht,Rpht,Lrng,Rrng] = loadLRSIimage(imgNum,1,0,'PHT','img',rootDTBdir);
%       [Ldvn,Rdvn]           = loadLRSIimageDVN(8,'server',0);
%       [~,IdvnCtrInd]=daVinciZoneCenter(Ldvn,18,1)
%       daVinciZoneSampleVet(IdvnCtrInd,[130 52],'slice',[0 13],.2,18,Lrng,Ldvn)
%
% Vets DaVinci patch center using one of two methods
%   1. 'density'
%      valid points based upon some density of DVN pixels in a patch
%   2. 'slice'
%       DaVinci regions extends through vertical of specified patch size

%   INPUTS
%   PszXY    - patch size [2 x 1]
%   zoneTestType - whether to use 'slice' or 'density' vetting for patch center
%              'slice' - use slice method, where DaVinci region center width must be present
%               within every row of the patch
%              'superSlice' - slice method, but allows for non-significant DVN zones, and allows
%                             a specified number of sigificant DVN zones if they are seperated
%                             horizontally by a specified distance.
%                             See nDVNbuffer, nDVN, minZoneSep.
%              'density' - DaVinci region must have a specified density within patch region
%   zoneBufferRC(2) - buffer for 'slice' method: width can be minMaxPixDVN-zoneBufferRC(2) in length
%              to count as an extending region in patch area
%   zoneBufferRC(1) - buffer for 'slice' method: if there are X number of rows in a patch, there
%              must be X-zoneBufferRC(1) rows with width of minMaxPixDVN-zoneBufferRC(2) to be
%              classified as a valid patch
%   zoneMinDensity - density of DaVinci pixels to non-DaVinci pixels in patch region to be
%              classified as a valid patch
%   minMaxPixDVN- minimum width of DaVinci region for patch center
%   Irng     - range image
%   Idvn     - DaVinci image
%   nDVNbuffer   - size of DVN region in patch to be considered significant (superSlice method only)
%   nDVN         - number of DVN regions allowed in each corresponding patch (superSlice method only)
%   minZoneSep   - required horizontal distance between central and additional significant DVN zones (superSlice method only)
%
%   OUTPUTS
%   PctrRC  - valid patch center subscripts
%   PctrInd - valid patch center index
% --------------------------------------------------------------------

if strcmp(zoneTestType,'superSlice') && (~exist('nDVNbuffer','var') || isempty(nDVNbuffer))
    error('davinciZoneSampleVet: superSlice method requires nDVNbuffer to be specified')
end
if strcmp(zoneTestType,'superSlice') && (~exist('nDVN','var')       || isempty(nDVN))
    error('davinciZoneSampleVet: superSlice method requires nDVN to be specified')
end
if strcmp(zoneTestType,'superSlice') && (~exist('minZoneSep','var') || isempty(minZoneSep))
    error('davinciZoneSampleVet: superSlice method requires minZoneSep to be specified')
end

if isempty(IdvnCtrInd)
    PctrRC=[];
    PctrInd=[];
    return
end

if ~isvar('ML')
    ML=[];
end
if ~isvar('MR')
    MR=[];
end

if mod(PszXY(1),2)==0
    PszXY(1)=PszXY(1)+1;
end
if mod(PszXY(2),2)==0
    PszXY(2)=PszXY(2)+1;
end
PszRC=fliplr(PszXY);
blank=zeros(PszRC);
PctrRC=ceil(PszRC/2);
PctrInd=sub2ind(PszRC,PctrRC(1),PctrRC(2));
IszRC=size(Irng);
[IdvnCtrRC(:,1),IdvnCtrRC(:,2)]=ind2sub(IszRC,IdvnCtrInd);

% DaVinci CONDITIONING VARIABLES - SEE "REMOVE INVALID SAMPLING POINTS" SECTION
DVNcpKernRadHW  = fliplr(floor(PszXY./2));                 %HEIGHT (1) AND WIDTH (2) OF REGION TO CHECK FOR DVN
DVNkernRadHW    = fliplr(floor(PszXY./2));                 %HEIGHT (1) AND WIDTH (2) OF REGION TO CHECK FOR DVN
DVNkernArea     = (DVNkernRadHW(1)*2+1)*(DVNkernRadHW(2)*2+1); %DENSITY ONLY

%GET CORRESPONDING POINTS FOR EACH POTENTIAL POINT
[IdvnCtrRC(:,1),IdvnCtrRC(:,2)]=ind2sub(IszRC,IdvnCtrInd);
[LitpRC,RitpRC,~,LitpRCchk,RitpRCchk] = LRSIcorrespondingPointVec([],LorR,IdvnCtrRC,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,0,[],ML,MR);

if strcmp(LorR,'L')
    Ixyz=Lxyz;
    BitpRC=RitpRC;
    adj=1;
elseif strcmp(LorR,'R')
    Ixyz=Rxyz;
    BitpRC=LitpRC;
    adj=-1;
end

Odvn=logical(Odvn);

% -------------------------------------------------------------------------------
valInd=[];
if strcmp(zoneTestType,'density')
    ratio=zeros(length(IdvnCtrInd),1);
    for j = 1:length(IdvnCtrInd)

        %GET ALL INDECES OF POTENTIAL PATCH
        vert=cell2mat(arrayfun(@(x) x-DVNkernRadHW(1):x+DVNkernRadHW(1),IdvnCtrRC(j,1),'UniformOutput',false));
        hori=cell2mat(arrayfun(@(x) x-DVNkernRadHW(2):x+DVNkernRadHW(2),IdvnCtrRC(j,2),'UniformOutput',false));
        vert=reshape(vert,numel(vert),1);
        hori=reshape(hori,numel(hori),1);

        %DO NOT INCLUDE PATCHES OUTSIDE OF RANGE
        if any(vert<=0) || any(hori<=0) || max(vert)>=IszRC(1) || max(hori)>=IszRC(2)
            continue
        end

        %DO NOT INCLUDE PATCHES WITH NANS
        if any(isnan(Ixyz(vert,hori)))
            continue
        end

        %CALCULATE DENSITY
        ratio(j)=(sum(sum(Idvn(vert,hori)))/DVNkernArea);

        %VALID IF DENSITY REQUIREMENT MET
        if ratio(j)>zoneMinDensity
            valInd=[valInd; j];
        else
            continue
        end


    end
elseif strcmp(zoneTestType,'slice') || strcmp(zoneTestType,'superSlice')
    MIN=inf;
    for j =1:length(IdvnCtrInd)

        %GET ALL INDECES OF POTENTIAL PATCH
        vert=round(cell2mat(arrayfun(@(x) x-DVNkernRadHW(1):x+DVNkernRadHW(1),IdvnCtrRC(j,1),'UniformOutput',false)));
        hori=round(cell2mat(arrayfun(@(x) x-DVNkernRadHW(2):x+DVNkernRadHW(2),IdvnCtrRC(j,2),'UniformOutput',false)));
        vert=reshape(vert,numel(vert),1);
        hori=reshape(hori,numel(hori),1);

        %DO NOT INCLUDE PATCHES OUTSIDE OF RANGE
        if any(vert<=0) || any(hori<=0) || max(vert)>=IszRC(1) || max(hori)>=IszRC(2)
            continue
        end

        %GET ALL INDECES OF NON-ANCHOR CORRESPONDING PATCH
        vertCP=round(cell2mat(arrayfun(@(x) x-DVNcpKernRadHW(1):x+DVNcpKernRadHW(1),BitpRC(j,1),'UniformOutput',false)));
        horiCP=round(cell2mat(arrayfun(@(x) x-DVNcpKernRadHW(2):x+DVNcpKernRadHW(2),BitpRC(j,2),'UniformOutput',false)));
        vertCP=reshape(vertCP,numel(vertCP),1);
        horiCP=reshape(horiCP,numel(horiCP),1);

        %REMOVE INVALID SUBSCRIPTS
        vertCP(vertCP<0)=[];
        horiCP(horiCP<0)=[];
        vertCP(vertCP>IszRC(1))=[];
        horiCP(horiCP>IszRC(2))=[];

        %DO NOT INCLUDE PATCHES WITH INVALID _CORRESPONDING_ PATCHES
        if isempty(vertCP) || isempty(horiCP)
            continue
        end
        try
            CPpatchDVN=Odvn(vertCP,horiCP);
        catch
            continue
        end

        %DO NOT INCLUDE PATCHES WITH NANS IN RANGE
        if any(isnan(Ixyz(vert,hori)))
            continue
        end

        if strcmp(zoneTestType,'superSlice')
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT DVN ZONES IN CPs
            % see nDVNbuffer & nDVN
            % NOTE this method takes much longer

            %GET NUMBER OF SIGNIFICANT DVN ZONES IN NON-ANCHOR PATCH
            CC=bwconncomp(Idvn(vertCP,horiCP));
            numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of DVN regions
            N=find(numels>nDVNbuffer); %number of significant DVN regions

            %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT DVN ZONES
            if length(N) > nDVNbuffer
                continue
            end

            %GET NUMBER OF SIGNIFICANT DVN ZONES IN ANCHOR PATCH
            CC=bwconncomp(Idvn(vert,hori));
            numels=cellfun(@(x) numel(x), CC.PixelIdxList); %total number of DVN regions
            N=find(numels>nDVNbuffer); %number of significant DVN regions

            %DO NOT INCLUDE PATCHES WITH MORE THAN SPECIFIED SIGNIFICANT DVN ZONES
            if length(N)>nDVN
                continue
            end

            %FIND MOST CENTRAL DVN ZONE
            co=[];
            for i = 1:length(N)
                if ismember(PctrInd,CC.PixelIdxList{N(i)})
                    co=i;
                    break
                end
            end

            %IF MOST CENTRAL ZONE IS INSIGNIFICANT, DISCARD
            if isempty(co)
                continue
            end

            %QUERY ONLY THE CENTRAL OBJECT
            O=CC.PixelIdxList{N(co)};
            Object=blank;
            try
                Object(O)=1;
            catch ME
                size(Object)
                size(Idvn(vertCP,horiCP))
                size(Idvn(vert,hori))
                max(O)
                rethrow(ME)
            end

            %DISCARD PATCHES WITH BRANCHING CENTRAL DVN ZONES
            [~,~,indOneChg]=runLengthEncoderVec(Object,2);
            if any(cellfun(@(x) length(x)>1,indOneChg))
                continue
            end

            %DISCARD PATCHES THAT DO NOT MEET WIDTH REQUIREMENTS FOR CENTRAL OBJECT
            widths=sum(Object,2);
            if bTwoWay==1
                count=sum(widths>=(minMaxPixDVN(1)-zoneBufferRC(2)) & widths <= (minMaxPixDVN(2)+zoneBufferRC(2))); %COUNT NUMBER OF ROWS THAT MEET WIDTH REQ
            elseif bTwoWay==0
                count=sum(widths>=(minMaxPixDVN(1)-zoneBufferRC(2))); %COUNT NUMBER OF ROWS THAT MEET WIDTH REQ
            end
            if count<PszXY(2)-zoneBufferRC(1)
                continue
            end

            %DISCARD PATCHES THAT HAVE A SINFICIANT ZONE HORIZONTALLY TOO CLOSE TO THE CENTRAL ZONE
            if length(N)>1
                NC=N(co);
                N(co)=[];

                %GET FURTHEST EDGES OF CENTRAL DVN ZONES
                [~,cc]=ind2sub(PszRC,NC);
                cc=minmax(cc);

                %GET FURTHEST EDGES OF OTHER DVN ZONES
                c = cellInd2sub(PszRC,CC.PixelIdxList(N));
                c = vertcat(c{:});

                if any(c < (cc(1)-minZoneSep) & c >(cc(2))+minZoneSep)
                    continue
                end
            end

            valInd=[valInd; j];

        elseif strcmp(zoneTestType,'slice')

            %COUNT NUMBER OF ROWS THAT ARE VALID
            count=0;
            exitflag=0;
            lastChng=[];
            for i = 1


                %GET WIDTH
                [indChng,runVal,runCnt]=runLengthEncoder(Idvn(vert(v),hori));
                width=runCnt(runVal==1);

                %DON'T INCLUDE MULTIPLE HALF OCCLUSION ZONES
                ind1=runVal==1;
                ind0=runVal==0;
                if sum(ind1) > 1 || sum(ind0) > 2
                    exitflag=1;
                    break
                end

                %INSURE DVN ZONE CONTINUITY
                ind=find(runVal==1);
                curChng=[indChng(ind):indChng(ind+1)];
                if ~isempty(lastChng) && ~any(ismember(curChng,lastChng))
                    exitflag=1;
                    break
                end
                lastChng=curChng;

                %CHECK IF MEETS WIDTH REQUIREMENT
                if bTwoWay==0 && width>=minMaxPixDVN-zoneBufferRC(2)
                    count=count+1;
                elseif bTwoWay==1 && width>=minMaxPixDVN-zoneBufferRC(2) && width<=minMaxPixDVN+zoneBufferRC(2)
                    count=count+1;
                end
            end
            if exitflag==1
                continue
            end

            % IF COUNT IS LARGE ENOUGH, CONSIDER AS VALID
            if count<PszXY(2)-zoneBufferRC(1)
                continue
            end
        end

        valInd=[valInd; j];


   end

end
%USE VALIND AS INDEX FOR VALID IdvnCtrInd
PctrInd=IdvnCtrInd(valInd);
PctrRC=IdvnCtrRC(valInd,:);
