function P = halfOcclusionPatchCPsBatch_ZB(P,PszXY,dspArcMin,stmXYdeg,fgndORbgnd,b2ctr,bPreDispDsp,hostName,rootDTBdir,bPlot)
% function P = halfOcclusionPatchCPsBatch(P,PszXY,dspArcMin,stmXYdeg,fgndORbgnd,rootDTBdir,bPreDispDsp,hostName)
%   get CPs or display disparity maps for every patch in P struct
%
% P           - struct containing patches etc from daVinciPatchSampleBatch code
% PszXY       - patch size resolution
% dspArcMin   - how many disparity in arcMin is being added to fixation
% stmXYdeg    - size of patch being displayed in degrees (req only if bPreDispDsp is 1)
% b2ctr      - whether bring fixation to the projection plane by shifting CPs by a fixed amount
% fgndORbgnd  - whether fixation is on binocular foreground, or monocular background
% rootDTBdir  - root directory of LRSI database
% bPreDispDsp - whether you are computing depth/disparity maps (1) or CPs (0)
% hostname    - computer on which you will be displaying patch stimuli

%GET LOCAL HOSTNAME IF ONE NOT SPECIFIED
if ~exist('hostName','var') || isempty(hostName)
    D.cmpInfo = psyComputerInfo();
    hostName = D.cmpInfo.localHostName;
end
if strcmp('hostName','ignore')
    D.cmpInfo = [];
    hostName = [];
end
if ~exist('bPreDispDsp','var') || isempty(bPreDispDsp)
    bPreDispDsp=0;
end
if ~exist('bPlot','var') || isempty(bPlot)
    bPlot=0;
end
if ~exist('b2ctr','var') || isempty(b2ctr)
    b2ctr=0;
end

%GET KEY CORRESPONDING POINTS
P = halfOcclusionKeyPatchCPs(P,b2ctr,fgndORbgnd);

%PROJETION PLANE FOR SPECIFIED DISPLAY
if bPreDispDsp==1
    [pCppXm,pCppYm,pCppZm] = psyProjPlaneAnchorEye('C',1,1,PszXY,stmXYdeg,hostName);
end

%INIT CPs OR DISPARITY MAPS DEPENDING ON bPreDispDsp
P.AitpRC            = zeros(PszXY(1)*PszXY(2)*3/2,2,length(P.LorRall));
P.BitpRC            = zeros(PszXY(1)*PszXY(2)*3/2,2,length(P.LorRall));
P.ABitpRC           = zeros(PszXY(1)*PszXY(2)*3/2,2,length(P.LorRall));
if bPreDispDsp==1
    P.depthImgDisp      = zeros(PszXY(2),PszXY(1),length(P.LorRall));
    P.dspArcMinImgDisp  = zeros(PszXY(2),PszXY(1),length(P.LorRall));
    P.depthImgDispB     = zeros(PszXY(2),PszXY(1),length(P.LorRall));
    P.dspArcMinImgDispB = zeros(PszXY(2),PszXY(1),length(P.LorRall));
end

%LOOP OVER ALL PATCH CENTERS
for i = 1:length(P.LorRall)

    %LOAD SCENE PARAMS IF NOT YET LOADED FOR CURRENT IMAGE NUMBER
    if i==1 || (i > 1 && P.imgNumAll(i) ~= P.imgNumAll(i-1))
        [Lpht,Rpht,Lrng,Rrng,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm] = loadLRSIimage(P.imgNumAll(i),0,0,'PHT','img',rootDTBdir);
    end

    %SELECT THE ANCHOR CORRESPONDING POINT
    if     strcmp(P.LorRall(i),'L')
        ItpRC=round(P.LitpRCall(i,:));
    elseif strcmp(P.LorRall(i),'R')
        ItpRC=round(P.RitpRCall(i,:));
    end

    %GET CPs
    [AitpRC(:,:,i), BitpRC(:,:,i), ABitpRC(:,:,i)] = halfOcclusionPatchCPs(dspArcMin,ItpRC,P.LorRall(i),fgndORbgnd,PszXY,stmXYdeg,Lxyz,Rxyz,LppXm,LppYm,RppXm,RppYm,[],b2ctr,bPlot,0);

    %GET DEPTH & DISPARITY MAPS
    if bPreDispDsp==1
        [P.depthImgDisp(:,:,i),P.dspArcMinImgDisp(:,:,i),P.depthImgDispB(:,:,i),P.dspArcMinImgDispB(:,:,i)]=disparityMapDisplay(AitpRC,ABitpRC,BitpRC,stmXYdeg,PszXY,pCppXm,pCppYm,pCppZm);
    end

    %PLOT KEY CORRESPONDING POINTS
    if bPlot==1
        figure(1343)
        %AitpRC
        subplot(1,3,1)
        hold on
        scatter(P.fAitpRC(i,2),P.fAitpRC(i,1),'r','filled')
        xlabel(P.LorRall(i))
        hold off

        %ABitpRC
        subplot(1,3,2)
        hold on
        scatter(P.bAitpRC(i,2),P.bAitpRC(i,1),'r','filled')
        xlabel(P.LorRall(i))
        hold off

        if strcmp(P.LorRall(i),'L')
            notLorR='R';
        elseif strcmp(P.LorRall(i),'R')
            notLorR='L';
        end

        %BitpRC
        subplot(1,3,3)
        hold on
        scatter(P.fbBitpRC(i,2),P.fbBitpRC(i,1),'r','filled')
        xlabel(notLorR)
        hold off

        drawnow
        waitforbuttonpress
    end

end
